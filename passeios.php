<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informações Turísticas IHC</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<script src="js/lightbox/js/jquery-1.7.2.min.js"></script>
<script src="js/lightbox/js/lightbox.js"></script>
<link href="js/lightbox/css/lightbox.css" rel="stylesheet" />
</head>

<body class="turismobg">
<div class="bgThickBox">
	<div class="tituloThickBox">Programas Turísticos</div>
</div>
<div class="blocoThickBox"> 

<div>
<p align="center">

	<img src="img/logo-confianca.jpg" alt="Logo Confiança Turismo" />

</p>
<p align="center">
	<strong>PROGRAMAS TURISTICOS</strong>
</p>
<p align="center">
	<strong>PRÉ E PÓS EVENTO</strong>
</p>
<p align="center">
	<strong><em>EXCLUSIVO AOS PARTICIPANTES DO EVENTO IHC</em></strong>
</p>
	<ul>
	<li><strong>Tour Chapada dos Guimarães –</strong> Duração Aprox.08 hs: Inclui veículo equipado com ar condicionado, serviço de água mineral a bordo, passeio
	pelos principais pontos turísticos do parque nacional de Chapada dos Guimarães incluindo a Cachoeira Véu de Noivas, o Portão do Inferno, parada na
	Salgadeira (possibilidade para banhos de cachoeira), o Mirante, visita à Igreja de Santana (último monumento barroco da região), 01 almoço, além de
	acompanhamento integral de guia de turismo. <br /> <br />
	
	Valor - Por pessoa ....................R$ 150,00.
	</li>
	</ul>
<ul>
	<li>
		<strong>Tour Pantanal: </strong>
		Saída 07h30<strong> - </strong>Duração aprox. 10 hs: Inclui veículo equipado com ar condicionado, serviço de água mineral a bordo, 65Km de safári
		fotográfico pela rodovia Transpantaneira, 01 Day-Use no Pantanal Mato Grosso Hotel, 01 almoço, 01 passeio de barco (safári fluvial) pelo rio Pixaim com
		aprox. 01 hora de duração, além de acompanhamento integral de guia de turismo. <br /> <br />
		Valor - Por pessoa ...................R$ 170,00.
	</li>
</ul>
<ul>
	<li>
		<strong>Tour Jaciara (esportes radicais) </strong>
		Duração aprox. 10 hs<strong>: </strong>Inclui: Traslado em veículo devidamente equipado com ar condicionado, serviço de água mineral a bordo, passeio
		de Rafting -<strong> </strong>Descidas em botes infláveis por corredeiras de rios com aventura e emoção. São utilizados equipamentos de segurança
		(remo, colete, salva-vidas com duplo preenchimento e capacete ). Inclui ainda 01 Day-Use balneário Cachoeira da Fumaça, e 01 almoço, além de
		acompanhamento integral de guia de turismo. <br /> <br />
		Valor - Por pessoa......................R$ 170,00.
	</li>
</ul>
<ul>
	<li>
		<strong>Tour Nobres - </strong>
		Duração aprox. 10 hs: Inclui: Traslado em veículo devidamente equipado com ar condicionado, serviço de água mineral a bordo, 01 flutuação de superfície
		no aquário encantado, 01 flutuação de superfície descendo o Rio Salobra com aprox. 01 hora de duração, 01 almoço, mergulho no Rio Estivado,
		acompanhamento integral de guias e monitores, e equipamento para mergulho em superfície. <br /> <br />
		Valor - Por pessoa ...................R$ 170,00.
	</li>
</ul>

<ul>
	<li>	
		Pacotes de 01 dia tipo "Full Day". Para opções com hospedagem, favor entrar em contato com José Eduardo: joseeduardo@confiancaturismo.com.br 
	</li>
	<li>
		Valores para grupos fechados <strong>com mínimo de 10 pessoas confirmadas</strong>. <br /><br />
	</li>
	<li>
		Crianças com até 03 anos (acompanhada dos pais) – Free <br />
	</li>
	<li>
		Pagamento em até 03x sem Juros – Cartões: <strong>VISA</strong> ou <strong>MASTERCARD.</strong>
	</li>
	<li class="bold">

		Reservas somente por:</br>
		Turismo Confiança</br>
		José Eduardo Sanches - Departamento Receptivo</br>
		joseeduardo@confiancaturismo.com.br</br>
		(65) 3314-2717
	</li>
		
	
</ul>
</div>

</div>
</body>
</html>