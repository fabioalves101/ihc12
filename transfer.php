<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Transfer IHC</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="bgThickBox">
	<div class="tituloThickBox">Transfer</div>
</div>
<div class="blocoThickBox"> 
<p>
<strong>Para os hóspedes do Holiday Inn Express Cuiabá.</strong>
</p>
<p>
O Holiday Inn Express Cuiabá (local e hotel oficial do IHC´12) oferece aos seus hóspedes o serviço de traslado Aeroporto –   Hotel grátis mediante pré agendamento de 24h da data de chegada entre 8h e 18h do horário local.
Para o retorno Hotel – Aeroporto será cobrado uma taxa de R$ 15,00 mediante pré-agendamento de 24h também no período de 8h às 18 no horário local.
Esse serviço está disponível somente entre segunda e sexta-feira.
</p>
<p>
	<strong>Para os hóspedes de qualquer outro Hotel.</strong>
</p>
<p>
Sugerimos aos participantes do IHC´12 a contratação do transfer via Agência Oficial do
evento ‘Confiança Turismo’.
</p>
<p>
Tarifa especial IHC´12: R$ 50,00.
</p>
<p>
Trajeto: Aeroporto – Hotel – Aeroporto.
</p>
<p class="bold">
Reservas somente no e-mail: 
<a href="mailto:joseeduardo@confiancaturismo.com.br">joseeduardo@confiancaturismo.com.br</a> <br />
José Eduardo Sanches - Departamento Receptivo <br />
(65) 3314-2717.
</p>
</div>

</body>
</html>