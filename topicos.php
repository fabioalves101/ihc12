   <div id="topicos">
        <div class="tituloBranco">Tópicos</div>
        <div class="textoBranco">
    Os tópicos de interesse incluem, mas não estão limitados a:

			<div class="blocoConteudo">
           		 <span style="width: 45%; float: left; padding:5px 40px 5px 0px;">
                 		<ul class="ulTopicos">
							<li> Acessibilidade <br /></li>
							<li> Análise de tarefas e de usuários <br /></li>
							<li> Aplicações em saúde, educação, transporte, esporte e meio ambiente <br /></li>
							<li> Computação afetiva e aspectos emocionais na interação <br /></li>
							<li> Educação em IHC <br /></li>
							<li> Ergonomia em IHC <br /></li>
							<li> Estudos sobre novos dispositivos de interação <br /></li>
							<li> Fatores legais relacionados ao design e ao uso de sistemas computacionais <br /></li>
							<li> Impacto da interação na sociedade <br /></li> 
							<li> Integração de técnicas de IHC e de Engenharia de Software <br /></li>
							<li> Interação com dados póstumos ou post-mortem <br /></li>
							<li> Interação e cidadania <br /></li>
							<li> Interação e disseminação de informações <br /></li>
							<li> Interação e entretenimento <br /></li>
							<li> Interação e o envelhecimento <br /></li>
							<li> Interação flexível e sensível a contexto <br /></li>
							<li> Interação homem-robô <br /></li>
							<li> Interação social: comunidades virtuais, comunidades online <br /></li>
							
						</ul>

					</span>
                     <span style="width: 45%; float: left; padding:5px;">
						<ul class="ulTopicos">
							
							<li> Interfaces de voz e multimodais <br /></li>
							<li> Interfaces inteligentes, adaptáveis e adaptativas <br /></li>
							<li> Interfaces naturais (NUI) <br /></li>
							<li> Interfaces para aplicações web <br /></li>
							<li> Interfaces para dispositivos móveis e sistemas ubíquos <br /></li>
							<li> Internacionalização de sistemas computacionais <br /></li>
							<li> Letramento digital <br /></li>
							<li> Maturidade na avaliação da qualidade da interação <br /></li>
							<li> Métodos e ferramentas de projeto de IHC <br /></li>
							<li> Métodos, formalismos, ferramentas e ambientes de design <br /></li>
							<li> Modelos teóricos em IHC <br /></li>
							<li> Privacidade e segurança do usuário <br /></li>
							<li> Programação por usuários finais <br /></li>
							<li> Técnicas de avaliação e design de interfaces com usuário <br /></li>
							<li> Tecnologia Verde na IHC e Design Sustentável <br /></li>
							<li> Uso terapêutico da interação <br /></li>
							<li> Visualização e interação de grandes volumes de informações (Visual Analytics) <br /></li>						
                       
						</ul>

                     </span>
                    <div class="clear"></div>

			</div><!-- FIM BLOCO CONTEUDO -->

	
    

			
			
			
			
			
		</div><!-- FIM TEXTO BRANCO: TOPICOS -->
       </div> <!-- FIM: TOPICOS -->