<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inscrição IHC</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<style>
body { background:none; }
</style>
</head>

<body>
<div class="bgThickBox">
	<div class="tituloThickBox">Inscrição</div>
</div>
<div class="blocoThickBox" style="margin-bottom: 55px"> 
<p>
	As inscrições podem ser realizadas no endereço especificado abaixo:</br> 
	<a href="https://centraldesistemas.sbc.org.br/ecos/ihc2012" target="_blank">https://centraldesistemas.sbc.org.br/ecos/ihc2012</a></br>
	</br>
	Fiquem atentos aos prazos de descontos e valores especiais para as inscrições antecipadas.
	
	<h3>O IHC’12 é composto por duas partes:</h3>
	Uma parte é destinada as sessões técnicas, com apresentação dos artigos completos, resumidos e industriais; palestrantes internacionais, pôsteres e demonstrações. A taxa básica de inscrição inclui a participação nestas ações bem como inclui o material do evento, coffee breaks e coquetel.
	</br>A outra parte é composta de tutoriais e workshops, os quais têm taxas pagas individualmente, conforme a intenção de cada participante. Ao se inscrever, favor observar a programação do evento. 
	</br>Vale ressaltar que as taxas básicas de inscrição não incluem alimentação, hotel, acomodações, nem a participação no jantar do evento, que será realizado na quarta-feira, dia 07 de novembro, em local a ser anunciado e atrações especialmente programadas para você. O valor do jantar, com bebida inclusa, será de R$70,00 por pessoa.
	
	<h4>Aguardamos você no IHC´12!</h4>

</p>
<div class="blocoThickBox" style="margin-bottom: 55px"> 
<h2>Valores das Inscrições:</h2>
<p>
	De forma inovadora, a Comissão Organizadora do IHC 2012, criou uma categoria especial para estudantes de graduação adotando um modesto valor de inscrição
	com o intuito de disseminar os estudos e pesquisas da área de IHC para estudantes deste nível, bem como incentivar a sua participação.
</p>
<table border="1" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td width="144" valign="top">
			</td>
			<td width="144">
				<p align="center">
					<strong>Até 31/08/2012</strong>
				</p>
			</td>
			<td width="144">
				<p align="center">
					<strong>Entre 01/09/2012 e 30/09/2012</strong>
				</p>
			</td>
			<td width="144">
				<p align="center">
					<strong>A partir de 01/10/2012</strong>
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Estudante de Gradução membro SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 50,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 70,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					<strike>
					R$ 100,00
					</strike>
				</p>				
				<p align="center">
					R$ 80,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Estudante de Gradução não Membro SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 100,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 140,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 200,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Estudante de Pós-Gradução Membro SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 175,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 200,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 240,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Estudante de Pós-Gradução não Membro SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 235,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 260,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 300,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Profissional Membro SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 350,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 400,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 470,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Não Membro SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 420,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 470,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 530,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Tutorial (3h)</strong>
				</p>
				<p>
					<strong>Membro SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 55,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 75,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 100,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Não Membro SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 75,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 95,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 130,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Tutorial (6h) membro SBC </strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 85,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 105,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 125,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Não Membro SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 110,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 140,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 170,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Workshops IHC Membro da SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 80,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 100,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					<strike>R$ 120,00</strike><br />
					R$ 100,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="144" valign="bottom">
				<p>
					<strong>Não Membro da SBC</strong>
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 110,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
					R$ 130,00
				</p>
			</td>
			<td width="144" valign="bottom">
				<p align="center">
				<strike>R$ 150,00</strike> <br />
					R$ 130,00
				</p>
			</td>
		</tr>
	</tbody>
</table>


</div>

</body>
</html>