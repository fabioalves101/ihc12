         	<div id="apresentacao">
                <div class="tituloAzul">Apresentação</div>
                <div class="textoAzul">
              O Simpósio Brasileiro sobre Fatores Humanos em Sistemas Computacionais, IHC, é o principal evento para troca de idéias e informações sobre estudos e pesquisas multidisciplinares que visam contribuir com a interação entre usuários e sistemas computacionais. Ele é também o ponto de encontro de pesquisadores de universidades e indústrias no Brasil do exterior, bem como de designers, programadores, educadores e outras pessoas de diferentes tradições e comunidades.

A primeira edição do IHC foi em 1998. Em 2011, em conjunto com o CLIHC (Conferencia Latino Americana de IHC), o evento foi realizado em Porto de Galinhas, PE, e contou com a presença de aproximadamente 200 pessoas.

Este ano, pela primeira vez na história, o evento acontece no Centro-Oeste do Brasil, na cidade de Cuiabá, MT, apoiado e organizado pela UFMT, mostrando assim o desenvolvimento contínuo e expressivo das pesquisas nessa região do país, trazendo à área de IHC como um todo uma maior abrangência no território Brasileiro.

<div>
<div class="tituloAzulTema">Tema</div>
         O tema da conferência, "A NATUREZA <font size="1"><SUP>DA</SUP>/<SUB>NA</SUB></font> INTERAÇÃO", pretende evocar a tendência das interações mais naturais e intuitivas, a forte presença da computação social e coletiva, e trazer a naturalidade ao processo de interação entre pessoas e tecnologia a qualquer momento e lugar, respeitando a Natureza e seus recursos.

Os Coordenadores convidam as diversas comunidades que atuam na área de IHC para discutir o papel dos pesquisadores e profissionais de IHC no desenvolvimento de tecnologias interativas que possam prover naturalidade, socialização, construção colaborativa para o futuro sustentável e em harmonia com os recursos naturais. O evento pretende discutir as necessidades dos usuários, expectativas, experiências e possibilidades ao usar as diferentes tecnologias de interação e comunicação para participar plenamente na vida social e civil. 



</div>
                	
                </div> <!-- FIM TEXTO AZUL -->
           </div> <!-- FIM ID APRESENTAÇÃO -->   
		   
		            	
     
                	