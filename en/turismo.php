<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informações Turísticas IHC</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<script src="js/lightbox/js/jquery-1.7.2.min.js"></script>
<script src="js/lightbox/js/lightbox.js"></script>
<link href="js/lightbox/css/lightbox.css" rel="stylesheet" />
</head>

<body class="turismobg">
<div class="bgThickBox">
	<div class="tituloThickBox">Touristic Informations</div>
</div>
<div class="blocoThickBox"> 
<div style="text-align: center">
<object width="600" height="450">

<param name="movie" value="../swf/ConhecaMT.swf" />

<embed src="../swf/ConhecaMT.swf" width="600" height="450">

</embed>

</object>



</div>

<p>
	Cuiabá, capital city of Mato Grosso, with tropical climate and 530.308 inhabitants
	(IBGE, 2010), is located in the southern part of the state. It is nationally wide known for
	its natural beauties, delicious typical dishes and welcoming people.
</p>
<p>
	Most of time, thermometers reach high temperatures in the city: 104o F or more. To
chill out, there is nothing better than a <em>happy hour</em> with regional fruit ice cream or draft
beer, in any of the bars or restaurants in the city.
</p>
<p>
	A meeting point for all tastes is the Popular Square. Surrounded by bars, 
	cafés, ice cream shops, pizza restaurants and snack bars, it offers many 
	food options, gathering beautiful people and good music. It is undoubted 
	great fun from <em>happy hour</em> time to the end of the night.
</p>
<p>
	People from Cuiabá know how to keep and revivify their history and traditions. 
	It is a good idea to visit some museums, art galleries and other touristic 
	venues in the city.
</p>
<p>
	Day and night, there are lots of things to do in Cuiabá!
</p>
<p>
	<u>Important tips for tourists:</u>
	:
</p>
<p>
	1. Cuiabá is in a different time zone from Brasília. It is one hour earlier.
</p>
<p>
	2. Bring light clothes and sunblock in your luggage.
</p>
<p>
	3.	If you go to Chapada, Jaciara, Nobres or Pantanal: bring caps, hats, 
	bathing suits and insect repellent.
</p>
<p>
	4. Tourist Help Center in the Airport: (65)3692-6204
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/cuiaba1-aquario.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/cuiaba1-aquario.jpg" alt="Aquário Municipal" />
</a>
</div>



<div class="imggal">
<a href="img/fotos/cuiaba2-avcpa.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/cuiaba2-avcpa.jpg" alt="Avenida do CPA" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/cuiaba3-bonifacia.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/cuiaba3-bonifacia.jpg" alt="Parque Mãe Bonifácia" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/cuiaba4-viola.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/cuiaba4-viola.jpg" alt="Viola de Cocho" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/cuiaba5-rio.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/cuiaba5-rio.jpg" alt="Rio Cuiabá" />
</a>
</div>
</div>
<br clear="left" />

<p>
	<strong><u>Must-see places:</u></strong>
</p>
<ul>
	<li>
		<strong>SESC ARSENAL</strong>
	</li>
</ul>
<p>
	<br/>
	It was created in 1818, by D. João IV, to work as a military building for making and 
	mending guns. It is now restored and works as cultural Center, with a bar/restaurant, 
	cinema rooms, library and space for artistic events. Among the permanent activities at 
	SESC, there is Bulixo, a big fair with typical dishes and handcraft. 
	Bulixo happens every Thursday from 6:00 p.m. to 10:00 p.m.</p>
<p>
	For sure, all the events at Sesc Arsenal are worth attending to.
</p>
<p>
	Open from Tuesday to Sunday, from 09:00 a.m. to 10:00 p.m.
</p>
<p>
	The bar is open from 7:00 p.m. 
</p>
<p>
	Phone: (65) 3611-0550
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/sesc2.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/sesc2.jpg" alt="Fachada Sesc Arsenal" />
</a>
</div>
<div class="imggal">
<a href="img/fotos/sesc1.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/sesc1.jpg" alt="Interior Sesc Arsenal" />
</a>
</div>
</div>
<br clear="left" />

<ul>
	<li>
		<strong>CASA DO ARTESÃO (CRAFTSMAN’S HOUSE)</strong>
	</li>
</ul>
<p>
	<br/>
	Placed in the Street 13 de Junho, in the Porto neighborhood, Casa do 
	Artesão provides you with a good sample of the culture from Mato Grosso. 
	It has an Handcraft Museum with a permanent exhibition of caboclo an indian 
	art, besides works by local craftsmen. The tourist can not only see this art, 
	but also buy some pieces and eat some typical food, such as rice cake and 
	cheese cake from Cuiabá.
</p>
<p>
	Open from Monday to Friday, from 8:30 a.m. to 5:00 p.m., and on Saturdays, 
	from 8:30 a.m. to 1:00 p.m.
</p>
<p>
	Snacks on Fridays, from 2:00 p.m. to 5:00 p.m. Phone: (65) 3611-0500.
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/casaartesao.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/casaartesao.jpg" width="200px" border="2" alt="Fachada Casa do Artesão" />
</a>
	</div>
	<div class="imggal">
<a href="img/fotos/art1.jpg" rel="lightbox"><img src="img/fotos/thumbs/art1.jpg" width="200px" border="2" alt="Artesanato" />
</a>

</div>
	
	<div class="imggal">
<a href="img/fotos/art3.jpg" rel="lightbox"><img src="img/fotos/thumbs/art3.jpg" width="200px" border="2" alt="Artesanato" />
</a>
	</div>
</div>

<br clear="left" />
<ul>
	<li>
		<strong>CATEDRAL METROPOLITANA (METROPOLITAN CATHEDRAL)</strong>
	</li>
</ul>
<p>
	<br/>
	 Inaugurated in 1973, the current cathedral was built over the debris of the 
	 former one, a jewel from the colonial period that was demolished by unknown 
	 reasons. Its modern architecture is much admired, and it holds annually the 
	 traditional Senhor Divino Festival, from May to June.
</p>
<ul>
	<li>
		<strong>NOSSA SENHORA DO BOM DESPACHO CHURCH</strong>
	</li>
</ul>
<p>
	<br/>
	Located on Morro do Seminário, the church began being built in 1720 by 
	Fray Ambrósio Daylé, who gave it the same architectural style of the 
	French church of Notre Dame, so that our church is also known as the 
	Cuiaban Notre Dame . It was inaugurated in 1919, but it remains unfinished. 
	Beside the church, there is the Sacred Art Museum, the old Seminário da 
	Conceição, which exhibits pieces from the old cathedral and some of 
	Dom Aquino Corrêa’s belongings.
</p>
<p>
	Open from Monday to Friday, from 2:00 p.m. to 6:00 p.m. Phone: (65)3028-6286.
</p>
<p>
	<strong></strong>
</p>
<p>
	<strong></strong>
</p>
<ul>
	<li>
		<strong>RONDON MUSEUM</strong>
	</li>
</ul>
<p>
	<br/>
	Created in 1972 by the Federal University of Mato Grosso, to promote research
	 on indians from Mato Grosso, the museum exhibits handcraft, weapons and
	  ornaments from these people.
</p>
<p>
	Open from Monday to Friday, 7:30 – 11:30 a.m. and 13:30 – 17:30 p.m., and on 
	Saturdays,  7:30 – 11:30 a.m. Free entry, at the campus of UFMT. Phone: (65)3615-8489.
	<br/>
	<br/>
</p>
<ul>
	<li>
		<strong>MORRO DA CAIXA D’ÁGUA VELHA MUSEUM</strong>
	</li>
</ul>
<p>
	<strong></strong>
</p>
<p>
	 Comandante Costa Street, no number, Downtown. Phone: (65) 3617-1274
</p>
<ul>
	<li>
		<strong>SOUTH AMERICA GEODESIC CENTER</strong>
	</li>
</ul>
<p>
	<strong></strong>
</p>
<p>
	Barão de Melgaço Street. Moreira Cabral Square.
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/geo.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/geo.jpg" alt="Fachada Sesc Arsenal" />
</a>
</div>
</div>
<br clear="left" />

<ul>
	<li>
		<strong>SÃO GONÇALO RIVERSIDE COMMUNITY</strong>
	</li>
</ul>
<p>
	<strong></strong>
</p>
<p>
	Traditional riverside community with simple but original fish 
	restaurants, offering local fish and clay pottery.
</p>
<ul>
	<li>
		<strong>GREEN AREA</strong>
	</li>
</ul>
<p>
	<strong></strong>
</p>
<p>
	There is a zoo in UFMT, and the city has other green areas, excellent 
	for relaxing and doing physical activities, such as Mãe Bonifácia Park, 
	Zé Bolo Flô Park and Massairo Okamura Park.
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/zoo.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/zoo.jpg" alt="Fachada Sesc Arsenal" />
</a>
</div>
</div>
<br clear="left" />
<p>
	<strong>
		 Stay longer in Mato Grosso and get to know its three ecosystems: 
		 Pantanal, Cerrado and the Amazon Forest. The excursion trips to 
		 Chapada dos Guimarães, Nobres, Jaciara and Pantanal are amazing!
	</strong>
</p>
<p>
	<strong></strong>
</p>
<p>
	<strong>Chapada dos Guimarães National Park</strong>, symbol of ecotourism, has 
	beautiful plateaus, green trails, caves, cliffs and beautiful 
	waterfalls! There is also Chapada dos Guimarães City, where you
	 can find downtown good handcraft and delicious restaurants.
	  Distance from Cuiabá: 60km.
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/chapada1-veu.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/chapada1-veu.jpg" alt="Véu de Noiva" />
</a>
</div>



<div class="imggal">
<a href="img/fotos/chapada3.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/chapada3.jpg" alt="Chapada dos Guimarães" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/chapada4-mirante.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/chapada4-mirante.jpg" alt="Mirante" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/chapada6-mirante.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/chapada6-mirante.jpg" alt="Mirante" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/chapada2-cachoeirinha.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/chapada2-cachoeirinha.jpg" alt="Cachoeirinha" />
</a>
</div>
</div>
<br clear="left" />
<p>
	<strong>Nobres</strong>, linda por natureza, tem como principais atrativos: 
	o Aquário Natural, 
	a Lagoa das Araras, o Rio Salobra e a Cachoeira da Serra Azul. Passeios de 
	bote, tirolesa e flutuação são as principais opções que vão te colocar em 
	contato com a vida aquática. Distância de Cuiabá: 140km.
</p>
<p>
	<strong>Jaciara</strong>
	is awaiting adventurers for radical sports: rafting and abseiling! 
	The city also offers thermal waters, waterfalls, caves and ecotrails. 
	Distance from Cuiabá: 125km.
</p>
<p>
	<strong>Pantanal</strong> is famous all around the world, due to its fauna 
	and flora. It is the greatest floodable plains in the world, comprising 
	parts of Mato Grosso, Mato Grosso do Sul, Paraguay and Bolivia. There you 
	can find many options for lodging and outdoor activities that will bring 
	you close to nature. Distance from Cuiabá: 140km.
</p>

<div class="galeria">
	<div class="imggal">
<a href="img/fotos/pantanal1-jacare.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal1-jacare.jpg" alt="Jacaré" />
</a>
</div>



<div class="imggal">
<a href="img/fotos/pantanal2.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal2.jpg" alt="Pantanal" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal3-pantaneiro.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal3-pantaneiro.jpg" alt="Pantaneiro" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal4-aves.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal4-aves.jpg" alt="Aves" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal5-tapete.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal5-tapete.jpg" alt="Tapete Natural" />
</a>
</div>
<div class="imggal">
<a href="img/fotos/pantanal6-paisagem.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal6-paisagem.jpg" alt="Paisagem" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal6-tuiuiu.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal6-tuiuiu.jpg" alt="Tuiuiu" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal6-vegetacao.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal6-vegetacao.jpg" alt="Vegetação" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal7-tuiuiu.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal7-tuiuiu.jpg" alt="Tuiuiu" />
</a>
</div>
</div>
<br clear="left" />



</div>
</body>
</html>