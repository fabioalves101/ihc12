         	<div id="apresentacao">
                <div class="tituloAzul">Presentation</div>
                <div class="textoAzul">
             The Brazilian Symposium on Human Factors in Computer Systems, 
             IHC, is the main conference for exchanging ideas and information 
             about multidisciplinary studies and researches on the interaction 
             between users and computer systems. It is also a meeting a venue 
             for university and industry researchers from Brazil and abroad, 
             as well as for designers, programmers, educators and other people 
             from different communities and traditions. The first edition of 
             IHC took place in 1998. In 2011, together with CLIHC (Latin American 
             HCI Conference), the conference was held in Porto de Galinhas, PE, 
             and was attended by nearly 200 people. This year, IHC will be held 
             for the first time in the Midwest of Brazil, in the city of Cuiabá, 
             supported and organized by UFMT (Federal University of Mato Grosso). 
             It aims to show the continuous and significant development of researches 
             in this Brazilian region, overspreading HCI studies throughout the 
             country.
<div>
<div class="tituloAzulTema">Main Theme</div>

The main theme of the conference, THE NATURE <font size="1"><SUP>OF</SUP>/<SUB>IN</SUB></font> INTERACTION, 
intends to evoke the tendency for more natural and intuitive interactions, 
the strong presence of social and collective computing, and the natural 
process of interaction between people and technology, respecting Nature 
and its resources. The coordinators invite the various communities in 
HCI to discuss the role of researchers and HCI professionals in developing 
interactive technologies that promote naturality, socialization and 
collaborative efforts for a sustainable future, in harmony with natural 
resources. The conference is intended to discuss users needs, expectations, 
experiences and possibilities for employing different interaction and 
communication technologies, aiming at full participation in social 
and civil life.
        

</div>
                	
                </div> <!-- FIM TEXTO AZUL -->
           </div> <!-- FIM ID APRESENTAÇÃO -->   
		   
		            	
     
                	