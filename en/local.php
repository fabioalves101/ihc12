﻿<div id="local">
    <div class="tituloBranco">Event Location</div>
    <div class="sanfona">
				 <div class="sanfonaTitulo"  id="SArtComp">
				 Information about the location of the event
				 </div>
				<div class="sanfonaCorpo">
        <p>
        	The Brazilian Symposium on Human Factors in Computer Systems, IHC, 
        	will be held in Cuiabá, capital city of Mato Grosso, in the hotel 
        	Holiday Inn Express. This hotel is part of IHG (InterContinental 
        	Hotels Group), worldwide famous for its lodgings. The hotel is 10km 
        	away from the International Airport of Cuiabá – Marechal Rondon, 
        	and it is considered one of the best options in the city, with 
        	adequate structure for events.
        	<br /><br /> 
        </p>
        <p><strong>Service:</strong></p>
        <p>                
        	<img src="img/holiday_logo_grande.gif" width="250" />        	
    	</p>
    	<p><br />
		    <strong>Address:</strong>
		    Avenida Miguel Sutil, 2050 - Jardim Leblon, Cuiabá - MT, 78060-000.
		    <br/>
		    <strong>Phone number:</strong>
		    (65) 3055-8500.
		</p>
		<p>
		    <strong>Exclusive e-mail address for IHC´12 participants to book their rooms:</strong>
		    <br />
		    <a href="mailto:ihc12@holidaycuiaba.com.br">ihc12@holidaycuiaba.com.br</a>
		    
		</p>
		<p>
		    <strong>Website:</strong> <br />
		    <a href="http://www.holidaycuiaba.com.br/">
		    	<strong>http://www.holidaycuiaba.com.br/</strong>
		    </a>
		</p>
		
		
		<ul>
			<li>
			- Close to a shopping center..
			</li>
			<li>
			- 20 minutes from the airport
			</li>
			<li>
			- All rooms with two Queen Size beds or one King Size bed.
			</li>
			<li>
			- Doors and windows with anti-noise systems
			</li>
			<li>
			- FREE Internet Access in the rooms
			</li>
			<li>
			- Breakfast included
			</li>
			<li>
			- Fitness room, sauna and swimming pool
			</li>
			<li>
			- Free Wi-Fi Internet connection all over the hotel
			</li>
			<li>
			- 24/7 Convenience shop
			</li>
			<li>
			- Free monitored parking
			</li>
		</ul>
		<p><br />
			<strong>
				Exclusive fees for IHC´12 participants:
			</strong>
		</p>
		<p>
			SGL rooms: R$ 180,00 + 13% taxes <br />
			DBL rooms: R$ 220,00 + 13% taxes <br />
			Third person in the room: plus R$ 50,00 + 13% taxes<br /><br /><br />
		</p>
		
	</div><!-- FIM CAIXA DESTAQUE -->
</div>
</div>