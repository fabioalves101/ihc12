<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Touristic Programs IHC</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<script src="js/lightbox/js/jquery-1.7.2.min.js"></script>
<script src="js/lightbox/js/lightbox.js"></script>
<link href="js/lightbox/css/lightbox.css" rel="stylesheet" />
</head>

<body class="turismobg">
<div class="bgThickBox">
	<div class="tituloThickBox">Touristic Programs</div>
</div>
<div class="blocoThickBox"> 

<div>
<p align="center">
	<img src="img/logo-confianca.jpg" alt="Logo ConfianÃ§a Turismo" />
</p>
<p align="center">
	<strong>TOURISTIC PROGRAMS</strong>
</p>
<p align="center">
	<strong>for before and after the event</strong>
</p>
<p align="center">
	<strong><em>Only for IHC participants</em></strong>
</p>
	<ul>
	<li><strong>Chapada dos GuimarÃ£es Trip â€“ </strong> Duration: Approximately 08 h. 
	The price includes air conditioned vehicle, mineral water, sightseeing in the 
	main landmarks of Chapada dos GuimarÃ£es national park, including the VÃ©u de 
	Noivas Waterfall, the â€œHell Gateâ€�, some time at Salgadeira (and waterfall baths), 
	the Belvedere, visit to Santana Church (last Baroque monument in the city), 
	lunch and tour guide all day long.  <br /> <br />
	
	Price - R$ 150,00 per person.
	</li>
	</ul>
<ul>
	<li>
		<strong>Pantanal Trip - </strong>
		Departure: 07h30 â€“ Duration: approximately 10 hs. The price includes air 
		conditioned vehicle, mineral water, 65Km of photo safari on the Transpantaneira 
		road, 01 Day-Use at Pantanal Mato Grosso Hotel, lunch, 1 boat ride (river safari) 
		in Pixaim river (for about 1 hour, and tour guide all day long).  <br /> <br />
		Price - R$ 170,00 per person.
	</li>
</ul>
<ul>
	<li>
		<strong>Jaciara Trip  - </strong>
		Duration: approximately 10 hs. The price includes air conditioned vehicle, 
		mineral water and rafting. Safety equipment is used, such as oars, life-vests 
		and helmet). It also includes 01 Day-Use at BalneÃ¡rio Cachoeira da FumaÃ§a, 
		luch and tour guide all day long. <br /> <br />
		Price - R$  170,00 per person.
	</li>
</ul>
<ul>
	<li>
		<strong>Nobres Trip - </strong>
		Duration: approximately 10 hs. The price includes air conditioned vehicle, 
		mineral water, 01 floating in the AquÃ¡rio Encantado, 01 surface floating 
		down Rio Salobra (for about one hour), lunch, bathing in Rio Estivado, 
		tour guides and monitors all day long and equipment for surface diving.
<br /> <br />
		Price - R$  170,00 per person.
	</li>
</ul>

<ul>
	<li>	
		1 full day trips. For lodgings, please call us. 
	</li>
	<li>
		Prices for groups with <strong>at least 10 people</strong>.
	</li>
	<li>
		Kids under 3 (with their parents) â€“ Free. <br />
	</li>
	<li>
		Payment in up to 3 monthly installments, no interest, on <strong>VISA</strong> or <strong>MASTERCARD</strong>.
	</li>
</ul>
</div>

</div>
</body>
</html>