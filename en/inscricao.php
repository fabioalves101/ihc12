<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inscrição IHC</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="bgThickBox">
	<div class="tituloThickBox">Register</div>
</div>
<div class="blocoThickBox" style="margin-bottom: 55px"> 
<p>
Registrations in the following website: <br />
<a href="https://centraldesistemas.sbc.org.br/ecos/ihc2012" target="_blank">
https://centraldesistemas.sbc.org.br/ecos/ihc2012
</a>
<br />
Pay attention to the deadlines for discounts and special prizes.

</p>


<h3>IHC’12 comprises two parts:</h3>
<p>
One of them is related two technical sessions, with presentations of full, short and industrial papers; international keynote speakers; posters; and demonstrations. <br />
The basic registration fees include these activities, registration material, coffe breaks and cocktail.<br />
The second part includes tutorials and workshops, which must be paid for separately. Please, when registering for these activities, pay attention to the conference schedule. <br/>
Note that basic registration fees do not include catering, lodging, or the conference dinner, to be held on Wednesday (November, 7th). The price of the dinner ticket, including beverage, is R$70,00 per person.
<br />
<strong>We are waiting for you in IHC’12!</strong>
</p>
<p><strong>Registration fees</strong></p>
<p>
	In an innovative way, the IHC 2012 Organizer Committee created a special low-charged category for undergraduate students, so as to spread researches and
	studies on HCI among university students and incentivate them to participate in the event.
</p>
<table border="1" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td width="121" valign="bottom">
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					<strong>Up to 08/31/2012</strong>
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					<strong>From 09/01/2012 to 09/30/2012</strong>
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					<strong>After 10/01/2012</strong>
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Undergraduate student – SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 24,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 34,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					<strike>US$ 48,00</strike>
				</p>
				<p align="center">
					US$ 38,40
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Undergraduate student – non SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 48,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 67,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 96,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Post-Graduation student – SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 84,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 96,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 115,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Post-Graduation student - non SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 112,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 124,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 143,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Professional – SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 167,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 191,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 224,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Non SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 200,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 224,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 253,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Tutorial (3h) – SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 31,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 41,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 53,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Tutorial (3h) – non SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 36,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 46,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 62,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Tutorial (6h) – SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 48,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 58,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 67,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Tutorial (6h) – non SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 53,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 67,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 81,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Workshops – SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 48,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 58,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 67,00
				</p>
			</td>
		</tr>
		<tr>
			<td width="121" valign="bottom">
				<p>
					<strong>Workshops - non SBC member</strong>
				</p>
			</td>
			<td width="168" valign="bottom">
				<p align="center">
					US$ 53,00
				</p>
			</td>
			<td width="142" valign="bottom">
				<p align="center">
					US$ 62,00
				</p>
			</td>
			<td width="136" valign="bottom">
				<p align="center">
					US$ 72,00
				</p>
			</td>
		</tr>
	</tbody>
</table>

</div>

</body>
</html>