<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lodgings IHC</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="bgThickBox">
	<div class="tituloThickBox">Hotel of the event</div>
</div>
<div class="blocoThickBox"> 
<div style="float: left; width: 395px">
<p>
The official hotel of the event is Holiday Inn Express Cuiabá, considered 
one of the best lodging options in the city. 
It has an excellent structure for events and:
</p>
</div>
<div style="float: right;">
<a href="http://www.holidaycuiaba.com.br" target="_blank">
<img src="../img/holidayinnlogo.jpg" alt="logo holidayinn" border="0" />
</a>
</div>
<div style="float:left">
<ul>
<li>Good location, close to a shopping mall and 20 minutes <br />from the airport</li>
<li>1 King size or 2 Queen size beds in all rooms</li>
<li>Free local calls for fixed phones</li>
<li>Free internet in all rooms</li>
<li>Breakfast included</li>
<li>Fitness room, sauna and swimming pool</li>
<li>Convenience store, open 24h</li>
<li>Free monitored parking</li>
</ul>

</div>
<div style="float: left; width: 100%; font-weight: bold">
<p>

Exclusive e-mail for IHC 2012 participants booking: 
<a href="mailto:ihc12@holidaycuiaba.com.br">
ihc12@holidaycuiaba.com.br
</a>
</p>
<p>
Exclusive prices for IHC 2012 participants
<br />
SGL room: R$ 180,00 + 13% taxes
<br />
DBL room: R$ 220,00 + 13% taxes
<br />
For a third person in the room, add R$ 50,00 + 13% taxes
</p>
<p>
Website: 
<a href="http://www.holidaycuiaba.com.br" target="_blank">
www.holidaycuiaba.com.br
</a>
</p>
</div>
<br />
<div style="float: left; width: 595px">
<p>
    As a second lodging option, IHC’12 Organizer Committee suggests Serras Hotel, 
    Just beside the official hotel of the conference (Holiday Inn Express Cuiabá).
</p>
</div>
<div style="float: right;">
<a href="http://www.serrashotel.com.br/" target="_blank">
<img src="img/serrashotel.jpg" alt="logo Serras Hotel" border="0" />
</a>
</div>
<div style="float:left;">
<ul>
	<li>
		96 rooms: 14 suites with bathtub and balcony, luxury double<br /> 
		rooms with balcony and single rooms 
	</li>
	<li> 
		Cable TV, free broadband Internet access, electronic safes, <br /> 
		ar conditioner and fridge 
	</li>
	<li> 
		Restaurant (24/7 room service)
	</li>
	<li>
		Fitness room
	</li>
	<li>
		Monitored parking
	</li>
	<li>
		Breakfast included
	</li>
</ul>

</div>
<div style="float:left;" class="bold">
<p>
IHC 12 participants must book their rooms via e-mail:  
<a href="mailto:reservas@serrashotel.com.br">
reservas@serrashotel.com.br
</a>
</p><p>
Exclusive fees for IHC 2012 participants
</p>
<ul>
	<li>
Luxury Single Room, 01 pax R$ 119,20
	</li>
	<li>
Luxury Double Room, 02 pax R$ 183,20
</li>
	<li>
Luxury Triple Room, 03 pax R$ 207,20
</li>
	<li>
Suite, 02 pax R$ 223,20
</li>
</ul>
<p>
Website: <a href="http://www.serrashotel.com.br/" target="_blank">http://www.serrashotel.com.br/</a>
<br /><br /><br />
</p>

</div>




</div>




</body>
</html>