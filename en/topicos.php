   <div id="topicos">
        <div class="tituloBranco">Topics of Interest</div>
        <div class="textoBranco">
    The topics of interest include, but are not limited to:

			<div class="blocoConteudo">
           		 <span style="width: 45%; float: left; padding:5px 40px 5px 0px;">
                 		<ul class="ulTopicos">
							<li> Accessibility <br /></li>
							<li> Task and user analysis  <br /></li>
							<li> Applications on health, education, transportation, sports and environment <br /></li>
							<li> Affective computing and emotional aspects in computing <br /></li>
							<li> HCI education <br /></li>
							<li> Ergonomics in HCI <br /></li>
							<li> Studies on new interaction devices <br /></li>
							<li> Legal issues related to computer systems use and design<br /></li>
							<li> Impacts of HCI on society <br /></li> 
							<li> Integrating HCI and Software Engineering techniques <br /></li>
							<li> Interaction with posthumous or post-mortem data <br /></li>
							<li> Interaction and citizenship <br /></li>
							<li> Interaction and information spread <br /></li>
							<li> Interaction and entertainment <br /></li>
							<li> Interaction and aging <br /></li>
							<li> Flexible and  context-aware interaction <br /></li>
							<li> Human-robot interaction <br /></li>
							<li> Social interaction: virtual communities, online communities<br /></li>
							
						</ul>

					</span>
                     <span style="width: 45%; float: left; padding:5px;">
						<ul class="ulTopicos">
							
							<li> Voice and Multimodal interfaces <br /></li>
							<li> Adaptive, adaptable, intelligent interfaces <br /></li>
							<li> Natural user interfaces (NUI) <br /></li>
							<li> Interfaces for web applications <br /></li>
							<li> Interfaces for mobile and ubiquitous systems <br /></li>
							<li> Internationalization of computer systems <br /></li>
							<li> Digital litteracy <br /></li>
							<li> Maturity in the evaluation of interaction quality <br /></li>
							<li> Methods and tools for HCI projects <br /></li>
							<li> Design methods, formalisms, tools and environments <br /></li>
							<li> Theoretical models in HCI <br /></li>
							<li> User’s privacy and security  <br /></li>
							<li> End user programming <br /></li>
							<li> Techniques for assessing and design interfaces with users <br /></li>
							<li> Green technology and sustainable design <br /></li>
							<li> Therapeutic uses of interaction <br /></li>
							<li> Visualization and interaction with big amounts of information (Visual Analytics) <br /></li>						
                       
						</ul>

                     </span>
                    <div class="clear"></div>

			</div><!-- FIM BLOCO CONTEUDO -->

	
    

			
			
			
			
			
		</div><!-- FIM TEXTO BRANCO: TOPICOS -->
       </div> <!-- FIM: TOPICOS -->