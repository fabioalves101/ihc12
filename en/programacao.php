     
      <div id="programacao"> <!-- PROGRAMAÇÃO ------------------------------ -->
    	<div class="tituloBranco">Schedule </div>
        	<div class="sanfona">
				 <div class="sanfonaTitulo"  id="SArtComp">
				 	Keynote Speakers
				 </div>
				<div class="sanfonaCorpo">
				<h1>Letizia Jaccheri</h1> 
                <img src="../img/Letizia.jpg" width="150" height="150" align="left" style="margin-right:15px;" />
                <strong>Web:</strong> http://letiziajaccheri.org/<br />
                
                <p>
                	<strong>"From software to art: a personal journey"</strong>
                </p>
                <br />
                Professor and leader of the Software Engineering Group at the Department of Computer and Information Science at the Norwegian University of Science and Technology.<br />
                <br />
Letizia Jaccheri has a PhD from the University of Torino and a Master in Computer Science, University of Pisa, Italy.She has been working in the field of Information Technology since 1989. Jaccheri has published more than 80 papers (h-index 16 g-index 30 calculated with Harzing’s Publish and Perish) in national and international journals and conferences and has supervised (as first or second supervisor) ten PhD students.<br />
				<br />
				<p>In the last decade,Jaccheri's research focus has been on software engineering support for art and entertainment. She serves as expert for national and international organizations, including being evaluator for the Norwegian Research Council, the Finnish Research Council, and the Italian Research Government. She is member of the National Committee which awards professorship to computer science academics since 2011. She is member of the ERCIM group Media Technology & Edutainment since 2010. She is member of IFIP TC14 on Entertainment Computing, National Representative of Norway.<br />
				<br />
				She has participated to several research projects, both nationally and internationally and to several software intensive artistic projects. For her book, "Kjærlighetog computer", Tapir Akademisk Forlag.ISBN 82-519-2034-5, April 2006 and for active participation in the Norwegian press, she get the prize of disseminator of the year at her faculty in 2006.For a detailed list of publications, supervised PhD students, and research projects see 
				<a href="http://www.letiziajaccheri.org/" target="_blank">
				http://www.letiziajaccheri.org/
				</a>
				 </p>
				 
				 <br /><br /><br />
				 
				 <h1>Sergi Jorda</h1>
				 <img src="img/sergijorda.jpg"  height="150" align="left" style="margin-right:15px;" />
				 <p>
				 	<strong>
				 	“Exploring the convergence between music performance and complex real-time interaction”
				 	</strong>
				 </p>
				 <p>Music Technology Group, Universitat Pompeu Fabra, Barcelona, Spain
				 </p>
				 
				 <p>
				 Dr. Sergi Jordà holds a degree in Fundamental Physics and a PhD in Computer Science and Digital Communication. After discovering computer programming during his undergraduate years in the early 1980s, he decided to fully devote himself to live computer music and interactive media. Throughout the 1990s he conceived and developed award winning interactive installations and multimedia performances, in collaboration with internationally renowned Catalan artists such as Marcel•lí Antunez or La Fura dels Baus. Since the late 1990s, he is a researcher in the Music Technology Group of Universitat Pompeu Fabra in Barcelona, and an Associate Professor in the same university. His main research interests are in the confluence of HCI and tangible and musical interaction, augmented communication using tabletops and brain computer interfaces. He has authored 20 articles in journals and book chapters, more than 50 peer-reviewed conference papers as well as given more than 20 invited presentations and keynote talks. He has received several international awards, including the prestigious Prix Ars Electronica Golden Nica. He is currently best known as one of the inventors of the Reactable, a tabletop musical instrument that accomplished mass popularity after being integrated in Icelandic artist Bjork’s last world tour, and he is one of the founding partners of the spin-off company Reactable Systems.
				 </p>
				 
				 <p>
				 In 1982, I was studying for a B.Sc. in Fundamental Physics, when I first ever saw a computer and soon discovered the magic of computer programming. It was such a revelation that some weeks later I had decided to give up saxophone practice and free jazz in order to become a computer music improviser! Since then, I have pursued the complexity, delicacy and futility of real-time, multidimensional performer–instrument–interaction from different and complementary perspectives. Initially I did this from a freer and purely aesthetically driven artistic/performer and freelance perspective, later trying to systematize and expand this empirical knowledge from a more scientific/academic point of view as a researcher at the Music Technology Group in Barcelona (1999–present), and more recently, also from an industrial/commercial perspective, manufacturing and selling new electronic musical instruments at Reactable Systems (2009–present). Furthermore, during the whole last decade I have also been expanding my areas of interest, studying HCI also from non-musical perspectives, both from technological (e.g. tangible and tabletop interaction), conceptual (e.g. real-time interaction) and applied (e.g. education) viewpoints. 
				 </p>
				 <p>
				 It seems that with the arrival of tablets, moultitouch or alternative videogame controllers, real-time music creation is finally acquiring a relevant status in the realm of HCI-related areas of application. 
				 </p>
				 
				 <p>
				 Yet, the truth is that since the boom of MIDI controllers in the early 1908s and even before, music and HCI have traveled a long path together, albeit sadly, a path very often quite unknown for the non-musical HCI scholars. For millennia, music instruments have indeed epitomized the potential and the richness that complex, skilled, continuous, multidimensional and real-time interaction can bring.
				 </p>
				 
				 <p>
				 In this keynote lecture, I will give an overview of my journey along the last three decades, exposing and unveiling some of the reasons that turn live music performance into an ideal test-bed for advanced human–computer interaction, opening promising and exiting fields of multi-disciplinary research and experimentation.
				 </p>
				  
				</div>
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Panels 
				 </div>
				<div class="sanfonaCorpo">
				<p>
				<strong>"IHC Panel and Industry"</strong>. <br/>
				Marco Winckler and Bruno Santana  
				</p>
				<p>
					<strong>
				"GranDIHC-BR: Great HCI Research Challenges in Brazil"</strong>. <br />
				Cecilia Baranauskas
				</p>
				</div>
				
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Technical Sessions
				 </div>
				<div class="sanfonaCorpo" id="listaArtigos">
				
					 <h1>FULL PAPERS – IHC 2012</h1>
			 
 <p>1.    Alessandro Luiz Stamatto Ferreira, Leonardo Cunha de Miranda and Erica Esteves Cunha de Miranda. Interfaces Cérebro-Computador de Sistemas Interativos: Estado da Arte e Desafios de IHC</p>

<p>2.    Aline Alves, Simone Bacellar Leal Ferreira, Viviane Veiga and Denis Silveira. Communicability in corporate information systems on the web : analyzing the interaction of deaf bilingual</p>

<p>3.    Artur Kronbauer, Celso A S Santos and Vaninha Vieira. Um Estudo Experimental de Avaliação da Experiência dos Usuários de Aplicativos Móveis a partir da Captura Automática dos Dados Contextuais e de Interação</p>

<p>4.    Bruno Santana Da Silva and Simone Diniz Junqueira Barbosa. A Conceptual Model for HCI Design Cases</p>

<p>5.    Cristiano Maciel and Vinicius Pereira. The internet generation and its representations of death: considerations for posthumous interaction projects</p>

<p>6.    Daniel A. Chagas, Elizabeth S. Furtado and Jouderian Nobre Jr.. Análise de Alternativas de Design de Mapas para TV digital Brasileira Baseada em Multicritérios</p>

<p>7.    Diego Henrique Dantas de Oliveira, Leonardo Cunha de Miranda, Erica Esteves Cunha de Miranda and Lyrene Fernandes Da Silva. Prototipação de Interfaces de Aplicativos para Dispositivos Móveis: Estado da Arte e Desafios de IHC</p>

<p>8.    Fernando Cesar Balbino and Junia Coutinho Anacleto. Redes Sociais Online Orientadas à Difusão de Inovações como Suporte à Comunicação Sustentável nas Organizações</p>

<p>9.    Francine Bergmann and Milene Silveira. “Eu vi o que você fez ...e eu sei quem você é!”: uma análise sobre privacidade no Facebook do ponto de vista dos usuários</p>

<p>10.    Gabriel Alves Vasiljevic Mendes, Leonardo Cunha de Miranda, Erica Esteves Cunha de Miranda and Lyrene Fernandes Da Silva. Prototipação de Interfaces Tangíveis de Produtos Interativos: Estado da Arte e Desafios da Plataforma Arduino</p>

<p>11.    Heiko Hornung and Cecilia Baranauskas. Timelines as mediators of lifelong learning processes</p>

<p>12.    Ingrid Monteiro and Clarisse De Souza. The representation of self in mediated interaction with computers</p>

<p>13.    Juliana Cristina Braga, Antonio Carlos Costa Campi Junior and Rafael Jeferson Pezzuto Damaceno. Estudo e Relato sobre a Utilização da Tecnologia pelos Deficientes Visuais</p>

<p>14.    Lara Piccolo and Cecilia Baranauskas. Basis and Prospects of Motivation Informing Design: Requirements for Situated Eco-feedback Technology</p>

<p>15.    Leonelo Almeida and Maria Baranauskas. Accessibility in Rich Internet Applications: People and Research</p>

<p>16.    Luciana Borges, Lucia Filgueiras, Cristiano Maciel and Vinicius Pereira. Customizing a communication device for a child with cerebral palsy using Participatory Design practices: contributions towards the PD4CAT method</p>

<p>17.    Luiz Corrêa, Flávio Coutinho, Raquel Prates and Luiz Chaimowicz. Uso do MIS para avaliar signos sonoros – Quando um problema de comunicabilidade se torna um problema de acessibilidade</p>

<p>18.    Marcelle Mota, Leonardo Faria and Clarisse De Souza. Documentation Comes to Life in Computational Thinking Acquisition with AgentSheets</p>

<p>19.    Mauro Anjo, Ednaldo Pizzolato and Sebastian Feuerstack. A Real-Time System to Recognize Static Hand Gestures of Brazilian Sign Language (Libras) alphabet using Kinect</p>

<p>20.    Natasha Malveira Costa Valentim, Káthia Marçal De Oliveira and Tayana Conte. Definindo uma Abordagem para Inspeção de Usabilidade em Modelos de Projeto por meio de Experimentação</p>

<p>21.    Roberto Calderon, Jonatas Leite de Oliveiras, Junia Anacleto and Sidney Fels. Understanding NUI-supported Nomadic Social Places in a Brazilian Health Care Facility.</p>

<p>22.    Roberto Romani and Maria Cecília Calani Baranauskas. Helping Designers in Making Choices through Games</p>

<p>23.    Rodrigo Rabello, Rodrigo Barbalho, Juliane Nunes and Christiane Von Wangenheim. INTEGRAÇÃO DE ENGENHARIA DE USABILIDADE EM UM MODELO DE CAPACIDADE/MATURIDADE DE PROCESSO DE SOFTWARE</p>

<p>24.    Rogério Xavier and Vania Neris. Decisões de design ruins e o impacto delas na interação: um estudo preliminar considerando o estado emocional de idosos</p>

<p>25.    Samuel B. Buchdid and Cecilia Baranauskas. IHC em contexto: o que as palavras relevam sobre ela</p>

<p>26.    Sarah Gomes Sakamoto, Lyrene Fernandes Da Silva and Leonardo Cunha de Miranda. Identificando Barreiras de Acessibilidade Web em Dispositivos Móveis: Resultados de um Estudo de Caso Orientado pela Engenharia de Requisitos</p>

<p>27.    Silvia Amelia Bim, Carla Faria Leitão and Clarisse Sieckenius de Souza. Can the teaching of HCI contribute to the learning of Computer Science? The case of Semiotic Engineering methods</p>

<p>28.    Simone Isabela De Rezende Xavier, Maria Lúcia Bento Villela and Raquel Oliveira Prates. Método de Avaliação de Comunicabilidade para Sistemas Colaborativos: Um Estudo de Caso</p>

<p>29.    Soraia Reis and Raquel Prates. Assessing the Semiotic Inspection Method - The Evaluators' Perspective</p>

<p>30.    Tathiane Andrade and Simone Barbosa. Design Aspects through the users’ behavior using large displays for 3D Navigation</p>
					 
<br />
<h1>SHORT PAPERS – IHC 2012</h1>
<p>1. Carlos Rosemberg Maia De Carvalho and Elizabeth Sucupira Furtado. Wikimarks: an approach proposition for generating collaborative, structured content from social networking sharing on the web. </p>
<p>2. Dyego Morais, Tancicleide Gomes and Flávia Peres. Desenvolvimento de jogos educacionais pelo usuário final: Uma abordagem alternativa ao Design Participativo. </p>
<p>3. Jônatas Leite de Oliveira and Junia Coutinho Anacleto. SoS – Um algoritmo para identificar pessoas homófilas em redes sociais com o uso da tradução cultural.</p>
<p>4. Lafayette Melo, Bruno Costa and Dayvison Almeida. Desenvolvimento de sistemas para Web com base nas metáforas em uso: estudo de caso para Redes Sociais.</p>
<p>5. Lara Piccolo and Cecilia Baranauskas. Energy, Environment, and Conscious Consumption: Making Connections through Design.</p>
<p>6. Luciano Tadeu Esteves Pansanato, André Luís Martins Bandeira, Luiz Gustavo Dos Santos and Dferson Do Prado Pereira. Projeto D4ALL: Acesso e Manipulação de Diagramas por Pessoas com Deficiência Visual.</p>
<p>7. Natália Santos, Lidia Ferreira and Raquel Prates. Caracterização das Adaptações em Métodos de Avaliação para Aplicações Colaborativas.</p>
<p>8. Silvia Amelia Bim, Milene Silveira and Raquel Prates. Ensino de IHC – Compartilhando as Experiências Docentes no Contexto Brasileiro.</p>
<p>9. Thiago S. Barcelos, Roberto Muñoz and Virginia Chalegre. Gamers as usability evaluators: a study in the domain of Virtual Worlds.</p>

<br />


<h1>INDUSTRY PAPERS – IHC 2012</h1>		
<p>
2. Erton Vieira and Alex Sandro Gomes. Design da Experiência em Processos Ágeis
</p><p>
3. Luciana Romani, Daniel Chino, Renata Gonçalves and Agma Traina. Challenges for users and designers in the design process of a satellite images handling system
</p><p>
4. Maihara Oliveira, Cristiano Maciel and Patricia Souza. Um diagnóstico do uso da modelagem da interação em métodos ágeis no mercado de software
</p><p>
5. Viviane Delvequio and Alessandra Rosa. Interação entre time de Design de Interfaces e time de Desenvolvimento: Tentativas, Fracassos e Sucessos
</p>				
				</div>
				
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Workshops 
				 </div>
				<div class="sanfonaCorpo">
				
	<h1>III WEIHC - Workshop on the Teaching of Human-Computer Interaction.</h1>
	<p>
	Silvia Bim and Clodis Boscarioli.
</p>
<p>
	<a href="http://www.inf.unioeste.br/WEIHC/">http://www.inf.unioeste.br/WEIHC/</a>
</p>
<p>Complete schedule <a href="http://www.inf.unioeste.br/WEIHC/?s=Programacao" target="_blank">here</a></p>

	<h1>IV WAIHCWS - Workshop on Aspects of Human-Computer Interaction for the Social Web.</h1>
<p>
	Adriana Santarosa Vivacqua, José Viterbo Filho and Sérgio Roberto P. da Silva.
</p>
<p>
	<a href="http://www.ufmt.br/ihc12/waihcws/">http://www.ufmt.br/ihc12/waihcws/</a>
</p>
<p>Complete schedule <a href="http://www.ufmt.br/ihc12/waihcws/trabalhosaceitos_pt.html" target="_blank">here</a></p>
<h1>Licínio Roque</h1>
<p>
Universidade de Coimbra
</p>
<p>
<a href="http://eden.dei.uc.pt/~lir" target="_blank">http://eden.dei.uc.pt/~lir</a>
</p>
<p>
<strong>Keynote Speaker:
Design Research on Pervasive Augmented Reality Games - lessons from the sociotechnical trenches
</strong>
</p>
<p>
Designing pervasive and augmented reality games as part of social computing strategies requires understanding the elements of context that will influence individual adoption, performance and social interaction in that context. Technical and social actors interfere in the wider network that constitutes a pervasive social game. In this presentation we will report on a Design Research case for a general purpose infrastructure to quickly model and experiment with diverse ARG designs, based on a proposed set of interactive building blocks. We will present our Petri Net modeling paradigm for ARGs, the basic interactive building blocks we tested and some of the lessons learned while trying to deploy pervasive games and their technical infrastructure.
</p>

				
				</div>
				
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Tutorials
				 </div>
				<div class="sanfonaCorpo">
				
	<h1>Tutorial A (6h) – Web Systems Design Using Human-Computer Interaction Techniques.</h1>

<p>
	<strong>30 attendants.</strong>
</p>
<p>
	<strong>Monday (05/11) morning and afternoon, from 8:30 a.m. to 5:00p.m.</strong>
</p>

<h2>
	Authors: Patricia Souza, Cristiano Maciel and Luciana Moraes.
</h2>

	<h1>Tutorial B (6h) – Clusterization Algorithms and Scientific Python Científico supporting User Modeling.</h1>

<p>
	<strong>Monday(05/11) morning and afternoon, from 8:30 a.m. to 5:00 p.m.</strong>
</p>

<h2>
Authors: Andrey Araujo Masiero, Leonardo Anjoletto Ferreira and Plinio Thomaz Aquino Jr.
</h2>
<h1>Tutorial C (3h) –  Designing speech based interfaces.</h1>
<p>
	<strong>Tuesday (06/11), from 2:00 p.m. to 3:30 p.m. and from 4:00 p.m. To 5:30 p.m.</strong>
</p>

<h2>
	Authors: Rodrigo Lins Rodrigues, Pablo Barros, Alexandre Magno Andrade Maciel and Edson Costa de Barros Carvalho Filho.
</h2>
<h1>Tutorial D (3h) – Systamatic Bibliographic Reviews in HCI.</h1>
<p>
	<strong>Thursday (08/11), from 8:00 p.m. to 10:30 p.m. and from 10:45 p.m. To 11:45 p.m.</strong>
</p>

<h2>
	Authors: Elizabete Munzlinger, Fabricio Batista Narcizo and José Eustáquio Rangel de Queiroz.
</h2>
				</div>
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Posters and Demonstrations
				 </div>
				<div class="sanfonaCorpo" id="listaArtigos">
<h1>Poster template</h1>
<p>
<a href="../arquivos/Modelo-poster -ihc12.pptx">
- PPTX Template
</a>
</p>
<p>
<a href="../arquivos/Modelo-poster -ihc12.odp">
- ODP Template
</a>
</p>		
<p>
	1.
	Clodis Boscarioli, Jeferson José Baquetta, João Paulo Colling e Charles Giovane De Salles. Avaliação e Design de Interação de Jogos Voltados ao Aprendizado
	de Crianças Surdas.
</p>
<p>
	2.
	Cristiano Maciel, Silvia Amelia Bim e Clodis Boscarioli. A Fantástica Fábrica de Chocolate: levando o sabor de IHC para meninas do ensino fundamental.
</p>
<p>
	
</p>
<p>
	3.
	Edie Santana, Cristiano Maciel e Kátia Alonso. Adicionando sociabilidade à interação em Ambientes virtuais de aprendizagem.
</p>
<p>
	4.
	Fábio Alves, Cristiano Maciel e Junia Anacleto. Investigando a percepção dos usuários sobre os mecanismos de gamificação da rede social Foursquare.
</p>
<p>
	5.
	Jader Saldanha e Amanda Meincke Melo. Qualidade no Uso de Objetos de Aprendizagem: apoio a inspeção de interface de usuário.
</p>
<p>
	6.
	Junio Soares Dias e Raquel Oliveira Prates. Sistema de Apoio à Aplicação do Método de Inspeção Semiótica.
</p>
<p>
	7.
	Marcio Alexandre De Queiroz e Cecília Arias Peixoto. Apoio à Geração de Protótipo de Interface Web Baseada na Metodologia The Bridge.
</p>
<p>
	8.
	Ricardo Barros e Giuliano Bertoti. Uma Ferramenta Para a Visualização de Informações no Jornalismo de Dados.
</p>
<p>
	9.
	Silvia Amelia Bim e Clodis Boscarioli. A IHC nos Cursos de Computação das Universidades Estaduais no Estado do Paraná.
</p>

<p>
	10.
	Luiz Henrique Americo Salazar, Christiane Gresse von Wangenheim, Thaísa Lacerda e Rodrigo Araújo Barbalho. 
	Customizando Heurísticas de Usabilidade para Celulares Touchscreen.
	 
</p>
<p>
	11.
	Rodrigo Andrade, Adriana Vivacqua e Ana Cristina Garcia. 
	O papel da interação centrada ao usuário para suporte à colaboração em uma Unidade de Emergência Médica.
</p>
				
				</div>
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Evaluation Competition
				 </div>
				<div class="sanfonaCorpo">
				
		<p>
	<strong>Undergraduate:</strong>
</p>
<p>
	-Análise da Percepção e Interação de Usuários sobre Privacidade e Segurança no Facebook
	<br/>
	UNIOESTE - Universidade Estadual do Oeste do Paraná (Cascavel - PR)
	<br/>
	Luiz Gustavo de Souza, Tiago Alexandre Schulz Sippert,
	<br/>
	André Specian Cardoso e Clodis Boscarioli (professor-orientador)
	<br/>
	<br/>
	-Exposição de imagem no Facebook - Um estudo sobre a privacidade de fotos pessoas na rede social
	<br/>
	UFC - Universidade Federal do Ceará (Fortaleza - CE)
	<br/>
	Mateus Pinheiro, Rodrigo Almeida, Deivith Oliveira
	<br/>
	Atila Oliveira e Arthur Tavares
</p>
<p>
	<strong>PosGraduate</strong>
	:
</p>
<p>
	-Avaliando aspectos de privacidade no Facebook pelas lentes de usabilidade, acessibilidade e fatores emocionais
	<br/>
	UFSCar - Universidade Federal de São Carlos (São Carlos - SP)
	<br/>
	Tatiana Alencar, Maira Canal, Kamila Rodrigues
	<br/>
	Rogério Xavier e Vânia Neris ( professora-orientadora )
	<br/>
	<br/>
	-Imagem e privacidade - contradições no Facebook
	<br/>
	UFMG - Universidade Federal de Minas Gerais (Belo Horizonte - MG)
	<br/>
	Ana Terto, Cláudio Alves, Janicy Rocha
	<br/>
	e Raquel Prates (professora-orientadora)
	<br/>
	<br/>
	-Inspeção Semiótica e Avaliação de Comunicabilidade: identificando falhas de comunicabilidade sobre as configurações de privacidade do Facebook
	<br/>
	PUC-RS - Pontifícia Universidade Católica do Rio Grande do Sul (Porto Alegre - RS)
	<br/>
	Juliano Varella de Carvalho, Felipe Lammel
	<br/>
	Janaína Dias da Silva, Lucélia Cynthia Chipeaux
	<br/>
	e Milene Silveira ( professora-orientadora)
</p>
				</div>
				<div class="sanfonaTitulo"  id="SDesaf">
				 	Desafios IHC12
				 </div>
				<div class="sanfonaCorpo" id="SDesafio">
				<p>
				Atualmente está em fase de submissão. Para mais informações
				<a href="#" onClick="movimento('submissoes')">
					Acesse a área de submissões do IHC12
				</a>
				</p>
				</div>
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Complete Schedule
				 </div>
				<div class="sanfonaCorpo">
				<h1>Complete Schedule</h1>
				<p>
				<a href="../img/tabela-prog.jpg" rel="lightbox">
					<img src="../img/tabela-prog.jpg" width="550px" />
				</a>
				</p>
				<p>
				<a href="../img/tabela-prog.jpg" rel="lightbox">
					Click to enlarge.	
				</a>				
				</p>
				<p>
					<a href="../arquivos/programacaoihc12.pdf" target="_blank">
					Download the complete schedule
					</a>
				</p>
				
				</div>
        	</div>
        	
        	
            </div>