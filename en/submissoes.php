    <div id="submissoes"> <!-- SUBMISSÕES -->
            <div class="tituloBranco">Submissions </div>
                
                  
                   <div class="sanfona">
				 <div class="sanfonaTitulo"  id="SArtComp">
				 	Important Informations
				 </div>   
                    <div class="sanfonaCorpo">

<u><strong>Important dates:</strong></u><br />



Registration of the groups for Evaluation Competition: 10/07/2012 <br />
Deadline for submitting workshop proposals: 10/07/2012 <br />
Deadline for submitting tutorials: 12/07/2012 <br />
Deadline for submitting Posters and Demonstrations: 16/07/2012 <br />
Deadline for submitting the paper, for short papers: 10/07/2012 <br />
Deadline for submitting the reports for Evaluation Competition: 20/07/2012 <br />
Deadline for submitting the paper, for industry papers: 22/07/2012 <br />

<!-- 
Deadline for submitting title, authors, keywords and abstract, for full papers: <strike>28/06/2012</strike> 01/07/2012 (extended)<br />
Deadline for submitting the paper, for full papers (only for papers already registered): 08/07/2012<br />
Deadline for submitting works in all other categories: 05/07/2012<br /><br />
 -->
Website for submission: 	<br />
<a href="http://www.easychair.org/conferences/?conf=ihc2012">		
http://www.easychair.org/conferences/?conf=ihc2012
</a><br />
E-mail of the conference: ihc2012@easychair.org<br /><br />
The authors must submit their papers either in English or in Portuguese.



</div><!-- FIM CAIXA DESTAQUE -->
</div>


<br />
    
    <div class="textoBranco">IHC 2012 gives the opportunity of submitting papers in the following categories:</div><br />
    
    		<div class="sanfona"><!--SANFONA - -->
  				 
                  <div class="sanfonaTitulo"  id="SArtComp">Full papers</div>
                  	<div class="sanfonaCorpo"><!--  SANFONA CORPO 1 -->
                  	  <p>
                  	  The category of full papers aims at the presentation of 
                  	  results from high quality academic researches. Submissions 
                  	  must contain an original contribution, and must not have 
                  	  already been published in another forum, or be subject to 
                  	  review for other conferences or publications. Contributions 
                  	  should include unpublished research results, case studies or 
                  	  experiences that provide new evidence about the research or 
                  	  application regarding HCI. 
                  	  <!-- 
                  	  Papers accepted in this category 
                  	  will be published in the proceedings of the event (it is intended 
                  	  to publish them too in ACM digital library, as in previous events).
                  	   -->
                  	  </p>
                      <h1>Submissions:</h1>
                      Submissions must be anonymous and the paper must have up to <strong>10 pages</strong>, 
                      in the <strong>ACM SIGCHI</strong> format
                      <a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>
                      . The authors must submit their papers 
                      electronically in PDF, in the 
                      <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a> system. 
                      
                      <h1>Presentation:</h1>
						Selected papers will be presented during technical sessions within 20 
						of expository presentation, followed by 10 minutes for discussion.
                      <h1>Coordinators: </h1>
                      Júnia Anacleto and Vânia Neris (UFSCar)
                      <h1>Important dates:</h1>
                        <strike>28/06/2012</strike> 01/07/2012 (extended) – deadline for submission for title, authors, keywords and abstract<br />
                        08/07/2012 – deadline for paper submission <br/>
                        04/08/2012 – results notification<br />
                      	15/08/2012 – final version submission
</div><!-- FIM SANFONA CORPO 1 -->
                        
                        
                          <div class="sanfonaTitulo" id="SArtRes">Short papers</div>
                  	<div class="sanfonaCorpo"><!--  SANFONA CORPO 2 -->
                  	  <p>The purpose of the short papers category is to allow researchers to 
                  	  submit a concise report of ongoing studies with partial, but significant 
                  	  results that help developing the HCI community. Submissions must contain 
                  	  an original contribution not published in another forum or subject to review 
                  	  for other conferences or publications. 
                  	  <!-- 
                  	  Papers accepted in this category 
                  	  will be published in the proceedings of the event (it is intended to publish 
                  	  them too in ACM digital library, as in previous events). --></p>
                      <h1>Submissions:</h1>
                      Submissions must be anonymous and the paper must have up to <strong>4 pages</strong>, in 
                      the <strong>ACM SIGCHI</strong> format 
                      <a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>
                      . The authors must submit their papers electronically 
                      in PDF, in the <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a> system.
                   <h1>Presentation:</h1>
                    Selected papers will be presented during technical sessions within 10 of 
                    expository presentation, followed by 5 minutes for discussion
                      <h1>Coordinators: </h1>
                      Raquel Prates (UFMG) and Luciana Salgado  (PUC-Rio)
                      <h1>Important dates:</h1>
                      
                      <strike>05/07/2012</strike> 10/07/2012 (extended) –  deadline for submission<br />
                      <strike>04/08/2012</strike> 10/08/2012 (extended) –  results notification<br />
                      <strike>28/08/2012</strike> 29/08/2012 (extended) –  final version submission
</div><!-- FIM SANFONA CORPO 2 -->
                        
                        
                    
                         <div class="sanfonaTitulo" id="SArtInd">Industry papers</div>
  <div class="sanfonaCorpo"><!--  SANFONA CORPO 3 -->

This category aims at case studies, tools, techniques, methods, practices and 
experiences in the industry area concerning the commercial development of 
interfaces for users of interactive systems. This is an opportunity for 
industrials to show scholars their needs, practices and cultures, so as to 
stimulate dialogues and partnerships between their areas. The main goals of 
this category are to identify challenges faced by professionals when developing 
interactive systems, stimulate researches applied to such challenges and discuss 
possible technology transfers by means of partnerships.<br />
 
Submissions must relate relevant experiences of HCI in industries, comparing 
existing principles, models, techniques or HCI solutions to new ones. <br />
 <!-- 
Submissions will be reviewed by members of the program committee and selected 
papers will be published in the proceedings of the event, as well as in the 
ACM digital library.  --><br/>

It is important to note that the approval of a submitted paper naturally implies 
the participation in the symposium.
<h1>Submissions:</h1>
Submissions must be anonymous and the paper must have up to <strong>4 pages</strong>, in the <strong>ACM SIGHCHI</strong> format
<a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>
. The authors must 
submit their texts electronically in PDF, in the 
<a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a> system.<br />
<br />


Industry papers are expected to contain:<br />

    <h2>Description of the problem: area, context, sources, managing issues, 
    social issues, technical issues, importance and consequences;</h2> 

    <h2>Report on experience in industry: challenges to solve the problem and explored solutions; and</h2>

	<h2>Critical analysis of the reported experience: What worked effectively? What didn’t work? 
	What must be improved? What challenges were faced? </h2>
    
<br />


<h1>Presentation:</h1>
 Papers will be presented in technical sessions, with 10 minutes for the 
 oral presentation and 5 minutes for discussion.



<h1>General criteria:</h1>


All contributions must be based in real cases. It is strongly recommended that 
professionals write about their experiences, relating socio-technical contexts, 
faced challenges, doubts, successes, failures and lessons learned. Presented 
experiences must be compatible with the topics of interest of the symposium, 
being relevant both for scholars and industrials attending to the event.
<br />

Papers do not need to be restricted to the Information Technology area. 
The event will accept technical or academic papers that can be applied 
to industries, universities, research centers, enterprises etc.
<br />

Papers must not have any commercial connotation or explicitly advertise 
for a specific product or institution. Any paper that does not follow 
this instruction will be rejected by the program committee.
<br />


<h1>Coordinators: </h1>
Marco Winckler (ICS-IRIT) and Bruno Santana (Petrobrás) 

<h1>Important dates:</h1>

	<strike>05/07/2012</strike> 22/07/2012 (extended) – deadline for submission<br />
    <strike>04/08/2012</strike> 19/08/2012 (extended) – notification of results<br />
    <strike>28/08/2012</strike> 29/08/2012 (extended) – deadline for submitting the final version.<br />

                        </div><!-- FIM SANFONA CORPO 3 -->
                  
                                             
                        
                        
                          <div class="sanfonaTitulo"  id="SArtW">Workshops</div>
                  			<div class="sanfonaCorpo"><!--  SANFONA CORPO 4 -->
                        	
                            Workshops offer a valuable opportunity for small communities or 
                            groups with common interests to meet for a discussion on specific 
                            topics of interest, trends and emerging themes from research or 
                            practical aspects of HCI. A workshop usually gathers from 10 to 20 
                            participants and lasts a whole day, a morning or an afternoon. It 
                            is interesting that the organizers of each workshop plan a publishing 
                            with the results from the workshop discussions, as a book or a 
                            special issue in a journal.


<h1>Submissions:</h1>


<strong>Every submission must contain the following items:</strong><br />



   	- Cover with the title of the workshop, contact information, duration (a whole day, 
   	a morning, an afternoon), target public and a brief biography of the organizers;<br />

    - Two-page extended abstract in the ACM SIGCHI format 
    <a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>
    , to be included in the proceedings. 
    This abstract must include the focus of the workshop, justifying its relevance and 
    mentioning some research questions or practices to be discussed in the conference, 
    based on the state of the art of the theme proposed; and<br />
	
	- <strong>250-word</strong> call for participation, to be published in the website of IHC 2012 and in 
	e-mail lists used of the HCI community. This material must be submitted electroniclly 
	in PDF, in the <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a> system.

    


Selection: Each workshop proposal Will be evaluated by a committee of specialists in the area.<br />


<h1>Coordinators: </h1> Milene Silveira (PUC-RS) and Leonardo Cunha de Miranda (UFRN)


<h1>Important dates:</h1>
                  			  
                  			  
                  			 <h2>For workshop organizers:</h2>
                  			  
                  			  
<p class="pEspacoSub">  
	<strike>05/07/2012</strike> 10/07/2012 (extended) – deadline for submitting workshop proposals<br />
    <strike>04/08/2012</strike> 20/07/2012 (extended) – notification of workshop approval
</p>
                  			  
                  			
                  			  <h2>For workshop participants:</h2>
                  			  
                  			  
                  			<p class="pEspacoSub">  
                              15/08/2012 – deadline for submission <br />
                  			  25/08/2012 – notification of results<br />
                  			  29/08/2012 – submission of abstract final version for the proceedings
                            </p>
                            
                            <h1>Workshops selecionados IHC'12:</h1>
  
  <p class="pEspacoSub"> 
  	<strong>III WEIHC – Workshop sobre o Ensino de Interação Humano-Computador </strong> <br />
	Silvia Bim, Clodis Boscarioli <br />
	<a href="http://www.inf.unioeste.br/WEIHC/" target="_blank">
	http://www.inf.unioeste.br/WEIHC/</a> <br /><br />

	<strong>
		IV WAIHCWS - Workshop sobre Aspectos da Interação Humano-Computador para a Web Social
	</strong><br />	
	Adriana Santarosa Vivacqua, José Viterbo Filho, Sérgio Roberto P. da Silva <br />
	<a href="http://www.ufmt.br/ihc12/waihcws/" target="_blank">
	http://www.ufmt.br/ihc12/waihcws/</a> 
  </p>
  <br />
   			  </div>
               			<!-- FIM SANFONA CORPO 4 -->
                        
                        
                        
<div class="sanfonaTitulo" id="SArtT">Tutorials</div>
                  			<div class="sanfonaCorpo"><!--  SANFONA CORPO 5 -->
                        	Tutorials are short events that aim to present practically and 
                        	didactically a general view on a research topic or technology 
                        	that might be interesting for the HCI community. Thus, the 
                        	attendant will be able to learn about a new topic on this area 
                        	and also get information for his researches and practices. <br/>
                        	
The tutorials must have as target public graduation and post graduation students and professionals 
on HCI. The tutorials may be introductory or advanced and they must be related to the topics 
of interest of the symposium. Interdisciplinary proposals are welcome. The tutorials must be 
3 or 6 hours long and presented in Portuguese.

<h1>Submissions:</h1>
 The tutorial proposals must have up to <strong>5 pages</strong>, in the format of book chapters published by SBC
<a href="http://www.sbc.org.br/index.php?subject=60&content=downloads" target="_blank">
http://www.sbc.org.br/index.php?subject=60&content=downloads</a>, including the following:

<h2> An abstract;<br /></h2>
    <h2>The duration of the tutorial (3 or 6 hours);<br /></h2>
   <h2> Target public, justifying and specifying any pre-requirements;<br /></h2>
  <h2>  Extended index, explaining what will be covered in each topic;<br /></h2>

  <h2>  An indication of why the course is interesting and should call the attention of the target public;<br /></h2>

   <h2>A brief biography of the authors;<br /></h2>

   <h2> Which author will present the tutorial;<br /></h2>
 <h2>Necessary computing or audiovisual resources.<br /></h2>

<br />

Each selected tutorial must have as a final version a text published as a book chapter: from 20 to 
30 pages for 3-hour tutorials, or from 30 to 40 pages, for 6-hour tutorials. Authors must allow 
publishing the abstract and the presentation of tutorials on Internet, in the IHC 2012 website.<br />

One lecturer for each tutorial will be given free enrollment in the event. 
Other types of support are being evaluated by the Organizer Committee. 
Material for tutorials must be submitted electronically in PDF, in the 
<a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a> system.<br />

<br />

<h1>Coordinators: </h1>
Simone Barbosa (PUC-Rio) and Plínio Aquino Jr (FEI)<br /><br />


<h1>Important dates:</h1>

    <strike>05/07/2012</strike> 12/07/2012 (extended) – deadline for submission <br />
    <strike>28/07/2012</strike> 26/07/2012 (extended) – notification of results <br />
    <strike>28/08/2012</strike> 29/08/2012 (extended) – submission of final version for the proceedings<br />

                        </div><!-- FIM SANFONA CORPO 5 -->
                    
                        
               <div class="sanfonaTitulo" id="SArtPD">Posters and Demonstrations</div>
                  			<div class="sanfonaCorpo"><!--  SANFONA CORPO 6 -->
                        	The category of posters and demonstrations is intended to promote an environment 
                        	of visibility and discussions with companies, universities and developers, 
                        	presenting works in initial or intermediate stages (without final results), 
                        	as well as interactive computer systems that may profit from contributions 
                        	from IHC 2012 participants.<br />

							It is expected to create an environment in which authors can present and receive feedback 
							from experienced members of the HCI community. This opportunity also aims to give 
							visibility to ongoing academic researches and product prototypes among scholars 
							and industries, enhancing the integration and cooperation between these areas.<br />


Presentation: There will be a specific time for collective and interactive exhibition; 
at least one of the authors must be present during the presentation. The instructions 
and information about resources available at the event will be sent to the authors 
of the selected papers.<br />

<br />

<h1>Submission:</h1>
The text of the presentation must be up to 2 pages long and follow the <strong>ACM SIGCHI</strong> format 
<a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>
, including:<br />
	
    - Research problem<br />

	- Theoretical foundations<br />

    - Methodololgy and current stage of the work<br />

    - Related works<br />

    - Expected results<br />

<br />


The texts must be submitted electronically in PDF, in the <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a> system.<br />


<h1>Coordinators:</h1>

 Amanda Melo (UNIPAMPA) and Marcelo Pimenta (UFRGS)<br />



<h1>Important dates:</h1>
    
    <strike>05/07/2012</strike> <strike>10/07/2012</strike> 16/07/2012 (extended) – deadline for submission<br />

    <strike>04/08/2012</strike> 10/08/2012 (extended) – notification of results<br />

    <strike>28/08/2012</strike> 29/08/2012 (extended) – submission of final version for the proceedings<br />

                        </div><!-- FIM SANFONA CORPO 6 -->    
                        
                        
                        
                         <div class="sanfonaTitulo" id="SArtCA">Evaluation competition</div>
                  			<div class="sanfonaCorpo"><!--  SANFONA CORPO 7 -->
This category aims to motivate students and HCI professors to participate 
in IHC 2012, contributing to students’ formation, production of didactic 
material and the growth of the community. The evaluation competition is 
mainly practical: participants will be asked to evaluate a computer system, 
applying their theoretical knowledge about methodologies of HCI evaluation. 
The first edition of the evaluation competition took place in IHC 2006, 
involving groups of graduation and post graduation students from many 
universities. The positive results led us to the fifth edition of the 
competition, in HCI 2012. <br />
<a target="_blank" href="http://www.ufmt.br/ihc12/Chamada-Competicao-Avaliacao-IHC2012.pdf">
Details about the theme of the competition 
will be available here.                 
</a>       	
                        	
							

<h1>Submissions</h1> 
Submissions must be anonymous and have up to <strong>12 pages</strong>, in the <strong>SBC papers format</strong>. 
The authors must submit their texts electronically in PDF, in the <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a> system.


<h1>Coordinators</h1>
Silvia Amélia Bim (UNICENTRO) and Carla Faria Leitão (PUC-Rio)

<h1>Important dates:</h1>

	<strike>05/07/2012</strike> 10/07/2012 (extended) – registration of the groups<br />

    <strike>19/07/2012</strike> 20/07/2012 (extended) – deadline for submitting the reports<br />

    <strike>10/08/2012</strike> 15/08/2012 (extended) – notification of the finalists for presentation in IHC 2012<br />

    28/08/2012 – submission of the final report
    
                          </div><!-- FIM SANFONA CORPO 7 -->      
                        
                        
                        <div class="sanfonaTitulo" id="SArtCMD">IHC'12 Challenge</div>
                  			<div class="sanfonaCorpo"> 
<h1>
	GranDIHC-BR: Possibilities for great research challenges on Human-Computer Interaction in Brazil
</h1>
<p>

	In 2006, the Brazilian Computing Society (SBC) defined the Five Greatest Research Challenges on 
	Computing in Brazil for the next decade. Challenge number 4, “Participative and universal access 
	to knowledge for all citizens”, corroborates the importance of HCI for the social, scientific, 
	technological and economic development of the nation. 
</p>
<p>
	Among the nine major fields of Computer Science delimited by ACM, HCI is the area that must deal 
	with universal and transversal issues, as well as consider social, cultural, economic, political 
	and geographic aspects involved in interaction. The Brazilian context, therefore, is complex: it 
	is the fifth biggest country in the world, there are many different cultures within its territory 
	and it is traditionally characterized by social inequality. Although in the last years fast and 
	deep changes have enhanced socioeconomic development indicators, hindrances to this evolution are 
	becoming more evident (e.g. education, science and technology, health, infrastructure, security 
	etc.). Scientifc and technological development is a key point to overcome these limitaions, but 
	such benefits will only come true  in a truly responsible way.
</p>
<p>
	The GranDIHC-BR will be held in the XI Brazilian Symposium on Human Factors in Computer Systems 
	(IHC 2012), in order to foment research problems on HCI that will be important for science and 
	our nation in the next decade, as an extension to SBC challenge 4.
</p>
<p>
	Researchers and professionals from industries are invited to fill in the <a href="http://goo.gl/4yo5Y" target="_blank">online form</a> 
	presenting their proposals and challenges. These proposals will 
	be analyzed by specialists and serve as basis for discussing the greatest research 
	challenges on HCI in Brazil. Some proposals will be invited for presentation in 
	the event
</p>
<p>
	Characteristics of a Great Research Challenge, as proposed by SBC:
</p>
<div>

	<p>
		1. A Great Research Challenge must address meaningful advancements for the area and science.
	</p>
	<p>
		2. The research must go beyond works and results that may arise from conventional 
		individual projects.
	</p>
	<p>
		3. Its progress must be incrementally evaluated, so that any necessary changes can 
		be made throughout the process.
	</p>
	<p>
		4. Its success must be evaluated in a clear and objective manner.
	</p>
	<p>
		5. It is possibly interdisciplinary in its essence and in its possibilities of solution.
	</p>
	<p>
		6. It must be realistic and feasible within a reasonable deadline (e.g. 10 years) 
		and it must  challenge current models and promote the evolution of traditions in the area.
	</p>
	<p>
		7. It must arise from a consensus among the scientific community to serve as a 
		long term scenario for researchers, irrespective of sponsoring politics or 
		conjectural variations.
	</p>

<h1>
	Form for submitting proposals: <a href="http://goo.gl/4yo5Y" target="_blank">Access the form</a>
</h1>
<p>Aditional information: 
<a href="mailto:rpereira@ic.unicamp.br">
rpereira@ic.unicamp.br
</a>
</p>
<h1>
	Important dates
</h1>
<p>
	Deadline for submitting proposals: <strike>30/09/2012</strike>  05/10/2012
</p>
<p>
	Feedback to the authors: 
	15/10/2012
</p>
<p>
	Presentation in the conference:
	 06/11/2012, in the afternoon.
</p>

	<h1>Coordination:</h1>
	
<p>
	Cecília Baranauskas (UNICAMP)
</p>
<p>
	Clarisse de Souza (PUC-Rio)
</p>
<h1>Comitê de Avaliação:</h1>
<p>
	Alessandro Rubim de Assis (Nuance)
</p>
<p>
	Elizabeth Furtado (UNIFOR)
</p>
<p>
	Jair Leite (UFRN)
</p>
<p>
	Juliana Sales (Microsoft Research)
</p>
<p>
	Junia C. Anacleto (UFSCar)
</p>
<p>
	Laura Sanchez Garcia (UFPR)
</p>
<p>
	Lúcia Filgueiras (USP)
</p>
<p>
	Marcelo Pimenta (UFRGS)
</p>
<p>
	Rodrigo Bonacin (CTI)
</p>
<p>
	Sergio Roberto da Silva (UEM)
</p>
<h1>Support:</h1>

<p>
	Roberto Pereira (UNICAMP)
</p>

</div>                         
                        
                        
                        
           <!--<div class="sanfonaTitulo" id="SArtCMD">Consórcio de Mestrado e Doutorado</div>
                  			<div class="sanfonaCorpo">  SANFONA CORPO 8 
                        	O Consórcio de Mestrado e Doutorado do IHC2012 é uma trilha componente do IHC2012, dedicada à apresentação e à discussão de trabalhos de mestrado e doutorado em andamento nas áreas de IHC e afins.<br />


Para os trabalhos submetidos nessa trilha, os autores devem submeter um arquivo de 3 páginas no formato ACM SIGCHI contendo as seguintes informações:<br />
<br />


   <h2> Título<br /></h2>

    <h2>Nome do aluno<br /></h2>

    <h2>Nome do orientador<br /></h2>

    <h2>Nível (mestrado ou doutorado)<br /></h2>

   <h2> Programa de pós-graduação<br /></h2>

    <h2>E-mail de contato do aluno<br /></h2>

    <h2>Ano de ingresso no programa<br /></h2>

    <h2>Mês/Ano previsto para conclusão<br /></h2>

    <h2>Data da aprovação da proposta de tese ou dissertação (qualificação)<br /></h2>

    <h2>Resumo<br /></h2>

    <h2>Palavras-chave<br /></h2>

    <h2>Caracterização do problema<br /></h2>

    <h2>Fundamentação teórica<br /></h2>

    <h2>Metodologia e estado atual do trabalho<br /></h2>

    <h2>Trabalhos relacionados<br /></h2>

    <h2>Resultados esperados<br /></h2>
<br />

    
Os textos devem ser submetidos eletronicamente através do sistema EasyChair, e em formato PDF.
<br />

<h1>Apresentação </h1>

Os autores dos trabalhos terão um momento de apresentação na forma de exposição oral de 10 minutos.  Um grupo de pesquisadores terá 5 minutos  para conversar com os autores. (obs aos chairs – esse formato de apresentação deve ser definido - reservamos 1 hora no programa para esta trilha)<br />


<h1>Coordenadores </h1>
Clarisse de Souza e Walter Cybis


<h1>Datas Importantes<br /></h1>

    05/07/2012– prazo de submissão<br />

    04/08/2012 – notificação dos resultados<br />

    15/08/2012 – envio da versão final

                        </div> FIM SANFONA CORPO 8 -->                
                        
                        
                        
                        
                           
                        
	  </div><!-- FIM SANFONA -->
    
    </div><!-- FIM SUBMISSÕES -->
    </div>