     
      <div id="programacao"> <!-- PROGRAMAÇÃO ------------------------------ -->
    	<div class="tituloBranco">Programação </div>
        	<div class="sanfona">
				 <div class="sanfonaTitulo"  id="SArtComp">
				 	Keynote Speakers
				 </div>
				<div class="sanfonaCorpo">
				<h1>Letizia Jaccheri</h1> 
                <img src="img/Letizia.jpg" width="150" height="150" align="left" style="margin-right:15px;" />
                <strong>Web:</strong> http://letiziajaccheri.org/<br />
                
                <p>
                	<strong>"From software to art: a personal journey"</strong>
                </p>
                <br />
                Professora e líder do Grupo de Engenharia de Software no Departamento de Computação e Ciência da Informação na Universidade Norueguesa de Ciência e Tecnologia. <br />
                <br />
				Letizia Jaccheri é doutora pela Universidade de Torino e mestre em Ciência da Computação, na Universidade de Pisa, na Itália. Tem trabalhado na área de Tecnologia da informação desde 1989. Jaccheri publicou mais de 80 artigos (h-index 16 g-index 30 calculado com Harzing’s Publish and Perish) em periódicos nacionais e internacionais e em conferências, além de ter orientado/co-orientado dez doutorandos.<br />
				<br />
				<p>Na última  década, o foco de pesquisa de Jaccheri tem sido o suporte da Engenharia de  Software à arte e ao entretenimento. Ela é consultora em organizações nacionais  e internacionais, além de avaliadora do Conselho Norueguês de Pesquisa, do  Conselho Finlandês de Pesquisa e do Conselho Nacional de Pesquisa da Itália.  Ela é membro do Conselho Nacional que confere cátedra a cientistas da  computação, desde 2011; do grupo ERICM de Tecnologia de Mídia e Entretenimento  Educacional, desde 2010; e do IFIP TC14 de Computação e Entretenimento, como  representante nacional da Noruega.<br />
				<br />
				Participou de diversos projetos de pesquisa, tanto nacional quanto internacionalmente, e de inúmeros projetos artísticos de software . Por seu livro, "Kjærlighetog computer", Tapir Akademisk Forlag.ISBN 82-519-2034-5, Abril de 2006, e por sua participação ativa na imprensa norueguesa, ganhou o prêmio de divulgadora do ano em sua faculdade, em 2006. Para uma lista detalhada de suas publicações, orientações de doutorado e projetos de pesquisa, veja em http://www.letiziajaccheri.com.            </p>
				
				<br /><br/><br />
				<h1>Sergi Jorda</h1>
				<img src="img/sergijorda.jpg"  height="150" align="left" style="margin-right:15px;" />
				<p>
					<strong>"Explorando a convergência entre performance musical e interações complexas em tempo real"</strong>
				</p>
				<p>
				Music Technology Group, Universitat Pompeu Fabra, Barcelona, Spain
				</p>
				
				<p>
				Dr. Sergi Jordà é formado em Física Fundamental e Doutor em Ciência da Computação e Comunicação Digital. 
				Depois de conhecer programação de computadores durante a graduação, no início dos anos 80, ele decidiu 
				dedicar-se completamente à música computacional e às mídias interativas. Ao longo dos anos 90, concebeu 
				e desenvolveu instalações interativas e performances multimídia premiadas, em colaboração com artistas 
				catalães renomados, como Marcel Alí Antunez ou La Fura dels Baus. Desde o final dos anos 90, Jordà é 
				pesquisador do Grupo de Tecnologia Musical da  Universidade Pompeu Fabra, em Barcelona, e professor 
				associado da mesma universidade. Seus principais interesses de pesquisa são a confluência da IHC e 
				interação tangível e musical, comunicação aumentada usando tampos de mesa e interfaces cérebro-computador. 
				É autor de 20 artigos em periódicos e capítulos de livro, mais de 50 artigos em conferências de revisão 
				por pares, além de ter feito mais de 20 comunicações e palestras magnas. Recebeu vários prêmios internacionais, 
				incluindo o prestigioso Prix Ars Electronica Golden Nica. É melhor conhecido atualmente por ser um dos 
				inventores do Reactable, um instrumento musical de tampo de mesa, que ganhou expressiva popularidade 
				depois de ser integrado à turnê mundial da artista islandesa Bjork. Além disso, é um dos membros fundadores 
				da empresa Reactable Systems.
				</p>
				<p>
				“Em 1982, eu estava estudando em meu bacharelado em Física Fundamental, quando vi pela primeira vez um 
				computador e logo descobri a magia da programação. Tamanha foi a revelação que, algumas semanas depois, 
				eu decidi abandonar o saxofone e o jazz para me tornar um improvisador de música computacional! Desde 
				então, tenho buscado a complexidade, delicadeza e futilidade da interação multidimensional e em tempo 
				real entre instrumento e performer, sob perspectivas diferentes e complementares. Eu comecei com uma 
				perspectiva mais livre e movida apenas por questões estéticas de um artista/performer; depois, tentei 
				sistematizar e expandir esse conhecimento empírico de um ponto de vista mais acadêmico/científico, como 
				pesquisador do Grupo de Tecnologia Musical de Barcelona (de 1999 até hoje); mais recentemente, também 
				de uma perspectiva industrial/comercial, manufaturando e vendendo novos instrumentos musicais eletrônicos 
				na Reactable Systems (de 2009 até hoje). Além disso, ao longo de toda a última década, tenho expandido 
				minhas áreas de interesse, estudando IHC também de perspectivas não musicais, tanto do ponto de vista 
				tecnológico (por exemplo, interação tangível e de topo de mesa), conceitual (como a interação em tempo 
				real) e aplicao (como a educação)
				</p>
				<p>
				Parece que, com a chegada dos tablets e de controles de videogame multitoque e alternativos, a criação 
				musical em tempo real está finalmente ganhando um status relevante no domínio das áreas de aplicação 
				relacionadas à IHC.
				</p>
				<p>
				A verdade é que, desde o sucesso dos controles MIDI, no início da década de 80, ou mesmo antes, a música 
				e a IHC vêm trilhando um longo caminho juntas, ainda que, infelizmente, com pouco conhecimento de 
				acadêmicos de IHC que  não sejam músicos. Por milênios, os instrumentos musicais têm materializado 
				o potencial e a riqueza de interações complexas, habilidosas, contínuas, multidimensionais e em 
				tempo real.
				</p>
				<p>
				Nesta palestra, darei uma visão geral de minha jornada ao longo das três últimas décadas, expondo e 
				revelando algumas das razões que fazem da performance musical ao vivo um laboratório ideal de 
				experimentos na interação avançada entre homem e computador, abrindo campos promissores de pesquisa 
				e experimentação multidisciplinar.”
				</p>
				  
				  
				  
				</div>
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Painéis 
				 </div>
				<div class="sanfonaCorpo">
				<h1>"Painel IHC e a Indústria"</h1>
				<strong>Marco Winckler e Bruno Santana</strong>
				<p>
				Este painel tem por objetivo reunir profissionais experientes para 
				discutir sobre o mercado de trabalho relacionado com a área de 
				Interação Humano-Computador (IHC) no Brasil. Os participantes 
				falarão sobre sua formação e experiência de mercado, destacando 
				oportunidades e desafios para aqueles que pretendem atuar na 
				área de IHC.
				</p>
				<p>
				<strong>Participantes:</strong>				
				</p>
				<p>
				<strong>Paulo Melo</strong> é psicólogo, mestre em psicologia cognitiva e doutor em design. 
				Profissionalmente, Paulo atuou como Designer de Interação e Especialista 
				em Usabilidade no CESAR Recife e CSIRO (Austrália). Nos últimos anos, ele 
				tem atuado na área de Experiência do Usuário (UX), onde trabalhou pelo CESAR Sul, 
				Philips e In/situm. Ao longo de sua experiência profissional, Paulo participou 
				de projetos ligados a jogos para aparelhos celulares, acessibilidade, aplicações 
				médicas, TV digital, sistemas de informação e design de produtos. Há quase 2 anos 
				Paulo é gerente de negócios no CESAR São Paulo. Além disso, Paulo tem dado aula 
				sobre design, usabilidade e inovação na Faculdade Anhanguera, Universidade 
				Positivo e CESAR.Edu.
				<br /><br />
				</p>
				
				<p>				
				<strong>Carlos Rosemberg</strong> é especialista em experiência do usuário com mais de 14 anos 
				de prática profissional na concepção, projeto e avaliação de produtos e serviços 
				interativos de diversos portes e plataformas. Atualmente atua como engenheiro de 
				requisitos e designer de interação no Instituto Atlântico (empresa de P&D em 
				Tecnologia da Informação e Comunicação, com sede em Fortaleza) e como pesquisador 
				no LUQS (Laboratório de estudos do Usuário e da Qualidade de Uso de Sistemas - 
				UNIFOR). É mestrando em Informática Aplicada (UNIFOR), possui Pós-Graduação em 
				Design Digital (FIC), Bacharelado em Administração de Empresas (UFC), habilitação 
				técnica em Informática Industrial (IFCE) e é professor convidado no Curso de 
				Especialização em Engenharia de Software na UECE. Também é Local Chapter Leader 
				do IXDA (Interaction Design Association) e um dos organizadores do WUD (Dia Mundial 
				da Usabilidade) em Fortaleza, desde 2008.
				<br /><br />
				</p>
				
				<p>
				<strong>Lara Piccolo</strong> trabalha há 8 anos como pesquisadora na área de IHC na Fundação CPqD, em 
				Campinas, SP. É engenheira de computação, mestre e doutoranda em IHC pelo Instituto 
				de Computação da UNICAMP. Atua na concepção de serviços e design de interfaces 
				interativas para Web, TV Interativa e dispositivos móveis. Tem trabalhado em projetos 
				de Pesquisa & Desenvolvimento de soluções inclusivas e consultoria em usabilidade e 
				acessibilidade para diversos setores, tais como setor bancário e de energia elétrica. 
				Seu papel tem sido levar a perspectiva do usuário aos processos de desenvolvimento 
				tecnológico.
				<br /><br />
				</p>
				<p>
					<strong>Renato Correa Vieira</strong>
					 é mestre em Ciência da Computação pela Universidade Federal de Santa Catarina (UFSC), 
					 tem mais de 10 anos de experiência em gestão de projetos, atualmente é Gerente de 
					 ERP da Petrobras Distribuidora, responsável por projetos de Gestão de Conteúdo e 
					 Supply Chain (logística). Palestrante nos temas de gestão de projetos, cadeia de 
					 suprimentos, inovação e empreendedorismo, seu background inclui projetos de 
					 desenvolvimento de aplicativos em plataformas proprietárias e de código aberto 
					 para diversos públicos de interesse de uma empresa de porte do Sistema Petrobrás.
					 <br /><br />
				</p>
				
				<h1>
				"GranDIHC-BR: Grandes Desafios de Pesquisa em IHC no Brasil"</h1>
				<strong>Cecilia Baranauskas</strong>
				
				</div>
				
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Sessões Técnicas
				 </div>
				<div class="sanfonaCorpo" id="listaArtigos">
				
					 <h1>ARTIGOS COMPLETOS – IHC 2012</h1>
			 
			 <p>1.    Alessandro Luiz Stamatto Ferreira, Leonardo Cunha de Miranda and Erica Esteves Cunha de Miranda. Interfaces Cérebro-Computador de Sistemas Interativos: Estado da Arte e Desafios de IHC</p>

<p>2.    Aline Alves, Simone Bacellar Leal Ferreira, Viviane Veiga and Denis Silveira. Communicability in corporate information systems on the web : analyzing the interaction of deaf bilingual</p>

<p>3.    Artur Kronbauer, Celso A S Santos and Vaninha Vieira. Um Estudo Experimental de Avaliação da Experiência dos Usuários de Aplicativos Móveis a partir da Captura Automática dos Dados Contextuais e de Interação</p>

<p>4.    Bruno Santana Da Silva and Simone Diniz Junqueira Barbosa. A Conceptual Model for HCI Design Cases</p>

<p>5.    Cristiano Maciel and Vinicius Pereira. The internet generation and its representations of death: considerations for posthumous interaction projects</p>

<p>6.    Daniel A. Chagas, Elizabeth S. Furtado and Jouderian Nobre Jr.. Análise de Alternativas de Design de Mapas para TV digital Brasileira Baseada em Multicritérios</p>

<p>7.    Diego Henrique Dantas de Oliveira, Leonardo Cunha de Miranda, Erica Esteves Cunha de Miranda and Lyrene Fernandes Da Silva. Prototipação de Interfaces de Aplicativos para Dispositivos Móveis: Estado da Arte e Desafios de IHC</p>

<p>8.    Fernando Cesar Balbino and Junia Coutinho Anacleto. Redes Sociais Online Orientadas à Difusão de Inovações como Suporte à Comunicação Sustentável nas Organizações</p>

<p>9.    Francine Bergmann and Milene Silveira. “Eu vi o que você fez ...e eu sei quem você é!”: uma análise sobre privacidade no Facebook do ponto de vista dos usuários</p>

<p>10.    Gabriel Alves Vasiljevic Mendes, Leonardo Cunha de Miranda, Erica Esteves Cunha de Miranda and Lyrene Fernandes Da Silva. Prototipação de Interfaces Tangíveis de Produtos Interativos: Estado da Arte e Desafios da Plataforma Arduino</p>

<p>11.    Heiko Hornung and Cecilia Baranauskas. Timelines as mediators of lifelong learning processes</p>

<p>12.    Ingrid Monteiro and Clarisse De Souza. The representation of self in mediated interaction with computers</p>

<p>13.    Juliana Cristina Braga, Antonio Carlos Costa Campi Junior and Rafael Jeferson Pezzuto Damaceno. Estudo e Relato sobre a Utilização da Tecnologia pelos Deficientes Visuais</p>

<p>14.    Lara Piccolo and Cecilia Baranauskas. Basis and Prospects of Motivation Informing Design: Requirements for Situated Eco-feedback Technology</p>

<p>15.    Leonelo Almeida and Maria Baranauskas. Accessibility in Rich Internet Applications: People and Research</p>

<p>16.    Luciana Borges, Lucia Filgueiras, Cristiano Maciel and Vinicius Pereira. Customizing a communication device for a child with cerebral palsy using Participatory Design practices: contributions towards the PD4CAT method</p>

<p>17.    Luiz Corrêa, Flávio Coutinho, Raquel Prates and Luiz Chaimowicz. Uso do MIS para avaliar signos sonoros – Quando um problema de comunicabilidade se torna um problema de acessibilidade</p>

<p>18.    Marcelle Mota, Leonardo Faria and Clarisse De Souza. Documentation Comes to Life in Computational Thinking Acquisition with AgentSheets</p>

<p>19.    Mauro Anjo, Ednaldo Pizzolato and Sebastian Feuerstack. A Real-Time System to Recognize Static Hand Gestures of Brazilian Sign Language (Libras) alphabet using Kinect</p>

<p>20.    Natasha Malveira Costa Valentim, Káthia Marçal De Oliveira and Tayana Conte. Definindo uma Abordagem para Inspeção de Usabilidade em Modelos de Projeto por meio de Experimentação</p>

<p>21.    Roberto Calderon, Jonatas Leite de Oliveiras, Junia Anacleto and Sidney Fels. Understanding NUI-supported Nomadic Social Places in a Brazilian Health Care Facility.</p>

<p>22.    Roberto Romani and Maria Cecília Calani Baranauskas. Helping Designers in Making Choices through Games</p>

<p>23.    Rodrigo Rabello, Rodrigo Barbalho, Juliane Nunes and Christiane Von Wangenheim. INTEGRAÇÃO DE ENGENHARIA DE USABILIDADE EM UM MODELO DE CAPACIDADE/MATURIDADE DE PROCESSO DE SOFTWARE</p>

<p>24.    Rogério Xavier and Vania Neris. Decisões de design ruins e o impacto delas na interação: um estudo preliminar considerando o estado emocional de idosos</p>

<p>25.    Samuel B. Buchdid and Cecilia Baranauskas. IHC em contexto: o que as palavras relevam sobre ela</p>

<p>26.    Sarah Gomes Sakamoto, Lyrene Fernandes Da Silva and Leonardo Cunha de Miranda. Identificando Barreiras de Acessibilidade Web em Dispositivos Móveis: Resultados de um Estudo de Caso Orientado pela Engenharia de Requisitos</p>

<p>27.    Silvia Amelia Bim, Carla Faria Leitão and Clarisse Sieckenius de Souza. Can the teaching of HCI contribute to the learning of Computer Science? The case of Semiotic Engineering methods</p>

<p>28.    Simone Isabela De Rezende Xavier, Maria Lúcia Bento Villela and Raquel Oliveira Prates. Método de Avaliação de Comunicabilidade para Sistemas Colaborativos: Um Estudo de Caso</p>

<p>29.    Soraia Reis and Raquel Prates. Assessing the Semiotic Inspection Method - The Evaluators' Perspective</p>

<p>30.    Tathiane Andrade and Simone Barbosa. Design Aspects through the users’ behavior using large displays for 3D Navigation</p>
</br>
<h1>ARTIGOS RESUMIDOS – IHC 2012</h1>
<p>1. Carlos Rosemberg Maia De Carvalho and Elizabeth Sucupira Furtado. Wikimarks: an approach proposition for generating collaborative, structured content from social networking sharing on the web. </p>
<p>2. Dyego Morais, Tancicleide Gomes and Flávia Peres. Desenvolvimento de jogos educacionais pelo usuário final: Uma abordagem alternativa ao Design Participativo. </p>
<p>3. Jônatas Leite de Oliveira and Junia Coutinho Anacleto. SoS – Um algoritmo para identificar pessoas homófilas em redes sociais com o uso da tradução cultural.</p>
<p>4. Lafayette Melo, Bruno Costa and Dayvison Almeida. Desenvolvimento de sistemas para Web com base nas metáforas em uso: estudo de caso para Redes Sociais.</p>
<p>5. Lara Piccolo and Cecilia Baranauskas. Energy, Environment, and Conscious Consumption: Making Connections through Design.</p>
<p>6. Luciano Tadeu Esteves Pansanato, André Luís Martins Bandeira, Luiz Gustavo Dos Santos and Dferson Do Prado Pereira. Projeto D4ALL: Acesso e Manipulação de Diagramas por Pessoas com Deficiência Visual.</p>
<p>7. Natália Santos, Lidia Ferreira and Raquel Prates. Caracterização das Adaptações em Métodos de Avaliação para Aplicações Colaborativas.</p>
<p>8. Silvia Amelia Bim, Milene Silveira and Raquel Prates. Ensino de IHC – Compartilhando as Experiências Docentes no Contexto Brasileiro.</p>
<p>9. Thiago S. Barcelos, Roberto Muñoz and Virginia Chalegre. Gamers as usability evaluators: a study in the domain of Virtual Worlds.</p>

<br />

<h1>ARTIGOS INDUSTRIAIS – IHC 2012</h1>		
<p>
2. Erton Vieira and Alex Sandro Gomes. Design da Experiência em Processos Ágeis
</p><p>
3. Luciana Romani, Daniel Chino, Renata Gonçalves and Agma Traina. Challenges for users and designers in the design process of a satellite images handling system
</p><p>
4. Maihara Oliveira, Cristiano Maciel and Patricia Souza. Um diagnóstico do uso da modelagem da interação em métodos ágeis no mercado de software
</p><p>
5. Viviane Delvequio and Alessandra Rosa. Interação entre time de Design de Interfaces e time de Desenvolvimento: Tentativas, Fracassos e Sucessos
</p>				
				</div>
				
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Workshops 
				 </div>
				<div class="sanfonaCorpo">
				
	<h1>III WEIHC - Workshop sobre o Ensino de Interação Humano-Computador.</h1>
	<p>
	Silvia Bim, Clodis Boscarioli.
</p>
<p>
	<a href="http://www.inf.unioeste.br/WEIHC/">http://www.inf.unioeste.br/WEIHC/</a>
</p>
<p>
O III WEIHC – Workshop sobre Ensino de IHC visa continuar as discussões em torno do ensino de IHC no Brasil, iniciadas no<a href="http://www.irit.fr/recherches/ICS/events/conferences/weihc/weihc2010/index.html">WEIHC 2010</a> em Belo Horizonte (MG) e no	<a href="http://www.irit.fr/recherches/ICS/events/conferences/weihc/index.html">WEIHC 2011</a> ocorrido em Porto de Galinhas(PE).
</p>

<p>Programação completa 
<a href="http://www.inf.unioeste.br/WEIHC/?s=Programacao" target="_blank">aqui</a></p>

	<h1>IV WAIHCWS - Workshop sobre Aspectos da Interação Humano-Computador para a Web Social.</h1>
<p>
	Adriana Santarosa Vivacqua, José Viterbo Filho e Sérgio Roberto P. da Silva.
</p>
<p>
	<a href="http://www.ufmt.br/ihc12/waihcws/">http://www.ufmt.br/ihc12/waihcws/</a>
</p>
<p>Programação completa 
<a href="http://www.ufmt.br/ihc12/waihcws/trabalhosaceitos_pt.html" target="_blank">aqui</a></p>

<h1>Lições das Trincheiras do Design Research em Pervasive Augmented Reality Games</h1>
<p>Licínio Roque</p>
<p><strong>Universidade de Coimbra</strong></p>
<p>
<a href="http://eden.dei.uc.pt/~lir" target="_blank">http://eden.dei.uc.pt/~lir</a>
</p>
<p>
O design de pervasive and augmented reality games enquanto componente de estratégias de 
computação social necessita um entendimento dos elementos do contexto que infuenciam a 
adopção, performance e interação social dos individuos nesse contexto. Actores técnicos 
e sociais interferem na rede alargada que constitui um jogo social ubíquo. Nesta apresentação 
iremos reportar um caso de Design Research para uma infraestrutura de âmbito genérico para 
a rápida modelação e experimentação com diversos conceitos de ARG, baseada numa proposta 
de componentes interactivos. Apresentaremos o nosso paradigma de modelação de ARGs baseado 
em Petri Nets, o conjunto de building blocks interactivos por nós ensaiados e algumas das 
lições aprendidas durante o processo de implementação de jogos ubíquos e infraestrutura 
técnica de suporte.
</p>
				</div>
				
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Tutoriais
				 </div>
				<div class="sanfonaCorpo">
				
	<h1>Tutorial A (6h) – Projetando Sistemas Web com o uso de Técnicas de Interação Humano-Computador.</h1>

<p>
	<strong>30 Vagas.</strong>
</p>
<p>
	<strong>Segunda-feira (05/11) o dia todo, das 8h30m às 17h.</strong>
</p>
<p>
	Este tutorial é considerado introdutório, tendo, como público-alvo, acadêmicos de graduação e pós-graduação e representantes da indústria de software
	interessados na Engenharia para Sistemas Web, nos aspectos da Interação Humano-Computador e na relação entre as duas áreas. O tutorial pretende oferecer
	subsídios aos participantes para o aprofundamento de estudos visando a aplicabilidade dos métodos da Engenharia Web, em especial, o método proposto por
	Lowe and Pressman (2009) de forma integrada às práticas de IHC no desenvolvimento de WebApps. A dinâmica do tutorial é baseada na exposição oral de
	conceitos, discussão de cases, realização de atividade prática e participação intensa do público, visando à troca de experiências e ideias. Está
	estruturado em cinco etapas especificadas a seguir: 1) Métodos da Engenharia Web - exposição oral sobre os métodos da engenharia web, características e
	uso. Em especial, métodos ágeis, muito utilizados no projeto Web, serão contextualizados. 2) Pirâmide de Projeto para Aplicações Web - apresentação da
	pirâmide proposta por Lowe and Pressman (2009), com explicação sobre as camadas (ações de projeto) de uma WebApp, as tarefas de projeto que devem ser
	realizadas e a relação entre tais tarefas. 3) Atividades da Interação Humano-Computador em Projetos de WebApps - apresentação das recomendações para
	integração do projeto de WebApp com a área de IHC. Será tratado cada nível da pirâmide, relacionando as atividades de IHC e apresentando exemplos. 4)
	Atividade Prática - Uso de técnicas para incorporação de IHC às ações de projeto de uma WebApp, a partir de um caso prático simples, em equipe. 5)
	Finalização e Avaliação - discussão dos cases desenvolvidos e encerramento com as considerações finais dos instrutores e do público presente.
</p>
<h2>
	Autores: Patricia Souza, Cristiano Maciel and Luciana Moraes.
</h2>

	<h1>Tutorial B (6h) – Algoritmos de Clusterização e Python Científico apoiando Modelagem de Usuário.</h1>

<p>
	<strong>Segunda-feira (05/11) o dia todo, das 8h30m às 17h.</strong>
</p>
<p>
	Python é uma linguagem de programação interativa, interpretada, orientada à objeto e multiplataforma que possui diversas aplicações no mercado de trabalho
	e cientificas. Existem diversas biblioteca disponíveis para Python que auxiliam o desenvolvimento das diversas aplicações, inclusive em Interação
	Humano-Computador, através dos algoritmos de aprendizado de máquina. Através desses algoritmos, em especial os algoritmos de clusterização de dados, serão
	apresentados para auxiliar na construção de modelos de usuários do tipo Personas. As Personas podem auxiliar o projetista de interface durante o projeto
	desta, ou posteriormente, na melhoria de interfaces que já estão em utilização. O momento que as Personas irão auxiliar o projetista dependerá de como as
	informações serão capturadas. Essas informações podem ser capturadas através de surveys ou log de aplicação, ambas podem ser utilizadas em momentos pré e
	pós projeto, contudo a segunda necessita de uma versão funcional do sistema para coletar as informações do usuário. Ao longo deste tutorial serão
	demonstrados como executar essas capturas de informações e ainda como realizar o tratamento desses dados para aplicar os algoritmos para clusterização, ou
	seja, o agrupamento dos perfis coletados e como se pode automatizar esse processo para que sejam utilizados em interfaces adaptativas. Por fim, serão
	demonstradas as técnicas para obter as Personas com base em métodos de medida de dispersão utilizadas em estatística dos dados e ainda as ferramentas
	disponíveis no Python para demonstração dos resultados de maneira gráfica para uma comparação analítica dos mesmos. O conteúdo do tutorial considera
	Python, Bibliotecas Cientificas, Bibliotecas para criação de Gráficos, técnica de Personas, Coleta de Informações, exemplos e aplicações. O público alvo
	deste tutorial é pesquisadores e estudantes de Interface Humano-Computador, com conhecimento em lógica de programação (importante para o treinamento em
	programação científica Python). Os benefícios diretos para os participantes são o aprendizado de técnicas que auxiliam no processo de modelagem de usuário
	e como utilizar esses modelos para interface adaptativa com Python, ganhando produtividade e flexibilidade no desenvolvimento de pesquisas científicas e
	mercadológicas.
</p>
<h2>
	Autores: Andrey Araujo Masiero, Leonardo Anjoletto Ferreira and Plinio Thomaz Aquino Jr.
</h2>
<h1>Tutorial C (3h) –  Desenvolvendo Soluções com Interface baseada em Voz.</h1>

<p>
	<strong>Sexta-feira (09/11), das 8:00 às 11:15h.</strong>
</p>
<p>
	O homem sempre buscou meios de comunicação que facilitassem a sua interação com a máquina e a interface com o usuário é parte fundamental dessa
	comunicação. Ao longo dos anos, vários meios de interação natural vêm sendo desenvolvidos e ido ao encontro da nova geração de interface humano-computador,
	que é de fácil aprendizado e de alta acessibilidade. Telas sensíveis ao toque, luvas (datagloves), sistemas de câmeras que captam gestos, ou que controlam
	o movimento dos olhos promovem uma interface multimodal que estimulam múltiplos canais do sentido humano. As interfaces baseadas em voz tem ganhado grande
	destaque nesta área devido à melhoria na performance dos sistemas de processamento automático de fala, à convergência de dispositivos e à massiva produção
	de conteúdos multimídia, que passaram a requerer modos mais rápidos e eficientes de interação com os usuários. Apesar desta evolução, o processo de
	desenvolvimento de aplicações com interface de voz, sofre os desgastes causados pela falta de um ambiente de desenvolvimento que torne a tarefa dos
	desenvolvedores mais produtiva. Este tutorial tem como objetivo demonstrar a utilização de um ambiente produtivo intitulado FIVE (Framework for an
	Integrated Voice Environment). Esse ambiente permite a diminuição da curva de aprendizagem, a independência de plataforma, a extensibilidade e a integração
	entre diversos motores de fala. O público alvo deste tutorial são os projetistas de aplicações baseadas em interfaces de voz, que desejam construir
	soluções para a realização de reconhecimento de fala, síntese de voz e verificação de locutor. O tutorial será composto de três fases: (1) Introdução ao
	processamento de voz, (2) Utilização do ambiente FIVE para construção de motores de fala e (3) Desenvolvimento de aplicações com interface de voz.
</p>
<h2>
	Autores: Rodrigo Lins Rodrigues, Pablo Barros, Alexandre Magno Andrade Maciel and Edson Costa de Barros Carvalho Filho.
</h2>
<h1>Tutorial D (3h) – Sistematização de revisões bibliográficas em pesquisas da área de IHC.</h1>
<p>
	<strong>Quinta-feira (08/11), das 8:00 às 11:15h.</strong>
</p>
<p>
	O minicurso destina-se à comunidade de pesquisadores, estudantes, educadores e profissionais atuantes em universidades e na indústria, que desejem conhecer
	ou se aprofundar em uma metodologia de Revisão Sistemática (RS) aplicável a estudos que envolvam levantamentos bibliográficos, especialmente sobre o estado
	da arte em IHC. Os participantes receberão contribuições significativas para o aprimoramento de suas metodologias de pesquisa, elevando a qualidade de seus
	levantamentos bibliográficos, em virtude das vantagens advindas da RS, dentre as quais se destacam: (i) Revisão do estado da arte de temas relativos à área
	de IHC; (ii) Auxílio à identificação de problemas atuais não esgotados em IHC; (iii) Subsídios para a identificação de soluções para problemas encontrados
	no foco da pesquisa de interesse; (iv) Produção de conteúdo relevante a publicações científicas, relatórios, trabalhos de conclusão de curso, dissertações
	e teses; (v) Rápida atualização do estado da arte sobre o tema/área da pesquisa; e (vi) Rápida incorporação/nivelamento de novos membros nos grupos de
	pesquisa das instituições, mediante o acesso a relatórios de atividades de RS de outros pesquisadores. Na primeira etapa do minicurso, apresenta-se a
	metodologia de pesquisa fundamentada em RS, sua origem, objetivos, importância, etapas e procedimentos, assim como a documentação gerada a partir do
	processo de RS. Na segunda etapa, planeja-se e formaliza-se a pesquisa, via Protocolo de Estudo. Na terceira etapa, demonstra-se a execução do Protocolo de
	Estudo e discutem-se procedimentos referentes à condução e/ou execução da pesquisa. Na quarta etapa, sumarizam-se os dados coletados e apresentam-se
	indicadores quantitativos e qualitativos de interesse para a extração e sumarização de dados, assim como de formas de organização e apresentação. Para
	todas as etapas, serão discutidos cuidados a serem tomados durante a execução de cada etapa, visando obter os melhores resultados.
</p>
<h2>
	Autores: Elizabete Munzlinger, Fabricio Batista Narcizo and José Eustáquio Rangel de Queiroz.
</h2>
				</div>
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Pôsteres e Demonstrações
				 </div>
				<div class="sanfonaCorpo" id="listaArtigos">
<h1>Modelo de Pôster</h1>
<p>
<a href="arquivos/Modelo-poster -ihc12.pptx">
- Download do modelo em PPTX
</a>
</p>
<p>
<a href="arquivos/Modelo-poster -ihc12.odp">
- Download do modelo em ODP
</a>
</p>
<h1>Observações</h1>
<p>
Observação: Cabe aos apresentadores providenciarem os pôsteres em material adequado 
(lona, pvc ou similar). Os pôsteres deverão conter acabamento com bastão e cordão para 
serem pendurados nos tripés disponibilizados pela organização.
</p>

<h1>Pôsteres Selecionados</h1>
<p>
	1.
	Clodis Boscarioli, Jeferson José Baquetta, João Paulo Colling e Charles Giovane De Salles. Avaliação e Design de Interação de Jogos Voltados ao Aprendizado
	de Crianças Surdas.
</p>
<p>
	2.
	Cristiano Maciel, Silvia Amelia Bim e Clodis Boscarioli. A Fantástica Fábrica de Chocolate: levando o sabor de IHC para meninas do ensino fundamental.
</p>
<p>
	3.
	Edie Santana, Cristiano Maciel e Kátia Alonso. Adicionando sociabilidade à interação em Ambientes virtuais de aprendizagem.
</p>
<p>
	4.
	Fábio Alves, Cristiano Maciel e Junia Anacleto. Investigando a percepção dos usuários sobre os mecanismos de gamificação da rede social Foursquare.
</p>
<p>
	5.
	Jader Saldanha e Amanda Meincke Melo. Qualidade no Uso de Objetos de Aprendizagem: apoio a inspeção de interface de usuário.
</p>
<p>
	6.
	Junio Soares Dias e Raquel Oliveira Prates. Sistema de Apoio à Aplicação do Método de Inspeção Semiótica.
</p>
<p>
	7.
	Marcio Alexandre De Queiroz e Cecília Arias Peixoto. Apoio à Geração de Protótipo de Interface Web Baseada na Metodologia The Bridge.
</p>
<p>
	8.
	Ricardo Barros e Giuliano Bertoti. Uma Ferramenta Para a Visualização de Informações no Jornalismo de Dados.
</p>
<p>
	9.
	Silvia Amelia Bim e Clodis Boscarioli. A IHC nos Cursos de Computação das Universidades Estaduais no Estado do Paraná.
</p>
<p>
	10.
	Luiz Henrique Americo Salazar, Christiane Gresse von Wangenheim, Thaísa Lacerda e Rodrigo Araújo Barbalho. 
	Customizando Heurísticas de Usabilidade para Celulares Touchscreen.
	 
</p>
<p>
	11.
	Rodrigo Andrade, Adriana Vivacqua e Ana Cristina Garcia. 
	O papel da interação centrada ao usuário para suporte à colaboração em uma Unidade de Emergência Médica.
</p>
				
				</div>
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Competição de Avaliação
				 </div>
				<div class="sanfonaCorpo">
				
				<p>
    <strong>Graduação:</strong>
</p>
<p>
    -Análise da Percepção e Interação de Usuários sobre Privacidade e Segurança no Facebook
    <br/>
    UNIOESTE - Universidade Estadual do Oeste do Paraná (Cascavel - PR)
    <br/>
    Luiz Gustavo de Souza, Tiago Alexandre Schulz Sippert,
    <br/>
    André Specian Cardoso e Clodis Boscarioli (professor-orientador)
    <br/>
    <br/>
    -Exposição de imagem no Facebook - Um estudo sobre a privacidade de fotos pessoas na rede social
    <br/>
    UFC - Universidade Federal do Ceará (Fortaleza - CE)
    <br/>
    Mateus Pinheiro, Rodrigo Almeida, Deivith Oliveira
    <br/>
    Atila Oliveira e Arthur Tavares
</p>
<p>
    <strong>Pós-Graduação:</strong>
</p>
<p>
    -Avaliando aspectos de privacidade no Facebook pelas lentes de usabilidade, acessibilidade e fatores emocionais
    <br/>
    UFSCar - Universidade Federal de São Carlos (São Carlos - SP)
    <br/>
    Tatiana Alencar, Maira Canal, Kamila Rodrigues
    <br/>
    Rogério Xavier e Vânia Neris ( professora-orientadora )
    <br/>
    <br/>
    -Imagem e privacidade - contradições no Facebook
    <br/>
    UFMG - Universidade Federal de Minas Gerais (Belo Horizonte - MG)
    <br/>
    Ana Terto, Cláudio Alves, Janicy Rocha
    <br/>
    e Raquel Prates (professora-orientadora)
    <br/>
    <br/>
    -Inspeção Semiótica e Avaliação de Comunicabilidade: identificando falhas de comunicabilidade sobre as configurações de privacidade do Facebook
    <br/>
    PUC-RS - Pontifícia Universidade Católica do Rio Grande do Sul (Porto Alegre - RS)
    <br/>
    Juliano Varella de Carvalho, Felipe Lammel
    <br/>
    Janaína Dias da Silva, Lucélia Cynthia Chipeaux
    <br/>
    e Milene Silveira ( professora-orientadora)
</p>
				</div>
				<div class="sanfonaTitulo"  id="SDesaf">
				 	Desafios IHC12
				 </div>
				<div class="sanfonaCorpo" id="SDesafio">
				<p>
				Atualmente está em fase de submissão. Para mais informações
				<a href="#" onClick="movimento('submissoes')">
					Acesse a área de submissões do IHC12
				</a>
				</p>
				</div>
				
				
				<div class="sanfonaTitulo"  id="SArtComp">
				 	Programação Completa
				 </div>
				<div class="sanfonaCorpo">
				<h1>Tabela Completa da Programação</h1>
				<p>
				<a href="img/tabela-prog.jpg" class="thickbox">
					<img src="img/tabela-prog.jpg" width="550px" />
				</a>
				</p>
				<p>
				<a href="img/tabela-prog.jpg" class="thickbox">
					Clique para ampliar.	
				</a>				
				</p>
				<p>
					<a href="arquivos/programacaoihc12.pdf" target="_blank">
					Download da Programação Completa
					</a>
				</p>
				
				</div>
        	</div>
        	
        	
            </div>
            
            