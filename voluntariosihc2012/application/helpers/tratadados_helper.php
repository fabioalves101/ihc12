<?php

	function verficaVazio($valor)
	{
		if(!empty($valor))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function verificaString($valor)
	{	
		if(verficaVazio($valor))
		{	
			if(is_string($valor))
			{
				return true;				
			}
			else
			{
				return false;
			}
		}
	}

	function verificaNumerico($valor)
	{
		if(verficaVazio($valor))
		{
			if(is_numeric($valor))
			{
				return true;
			}
			else
			{
				return false;

			}
		}
	}

	function verificaData($valor)
	{
		if(verficaVazio($valor))
		{
			
			$data = explode("/","$valor"); // fatia a string $dat em pedados, usando / como refer�ncia
			
			if($data=="00/00/0000"){return false;}
			$d = $data[0];
			$m = $data[1];
			$y = $data[2];
	
			if( (!empty($d)) and (!empty($m)) and (!empty($y)))
			{
				if (checkdate($m,$d,$y))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	function preparaString($valorString)
	{
		$retornoString=$valorString;
		$retornoString=str_replace("'", "\'",$retornoString);
		return $retornoString;
	}


	function retornaData($valorData)
	{
		if(!empty($valorData))
		{
			$dataRetorno=substr($valorData,8,2).'/'.substr($valorData,5,2).'/'.substr($valorData,0,4);
			return $dataRetorno;
		}
	}

	function retiraTagHTML($textoComTag)
	{
		return strip_tags($textoComTag, '<(.*?)>');
	}

	function preparaValor($valorNumerico)
	{
		$retornoValor=str_replace(",","",$valorNumerico);
		$retornoValor=str_replace(".","",$retornoValor);
		return $retornoValor;
	}

	function insereValorMonetario($valorX)
	{
		$retornaValor=str_replace(".","",$valorX);
		$retornaValor=str_replace(",",".",$retornaValor);
		return $retornaValor;
	}

	function retornaValorMonetario($valorX)
	{
		$retornaValor=number_format($valorX,2,',','.');
		return $retornaValor;
	}

	function insereData($valorData)
	{
		$dataRetorno=substr($valorData,6,4).'/'.substr($valorData,3,2).'/'.substr($valorData,0,2);
		return $dataRetorno;
	}

	function verificar_email($email)
	{
		$mail_correto = 0;
		//verifico umas coisas
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@"))
		{
			if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," ")))
			{
				//vejo se tem caracter .
				if (substr_count($email,".")>= 1)
				{
					//obtenho a terminao do dominio
					$term_dom = substr(strrchr ($email, '.'),1);
					//verifico que a terminao do dominio seja correcta
					if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) )
					{
						//verifico que o de antes do dominio seja correcto
						$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
						$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
						if ($caracter_ult != "@" && $caracter_ult != ".")
						{
							$mail_correto = 1;
							return true;
						}
					}
				}
			}
		}
	}

	function verificaImagem($imagem)
	{
		if($imagem["type"] == "image/jpeg")
		{
			return true;
		}
		else if($imagem["type"] == "image/gif")
		{
			return true;
		}
		else if($imagem["type"] == "image/png")
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function obtemArea($operacao)
	{
		if($operacao=="usuarios")
		{
			return 1;
		}
		else if($operacao=="links")
		{
			return 2;
		}
		else if($operacao=="revistas")
		{
			return 3;
		}
		else if($operacao=="paginas")
		{
			return 4;
		}
	}

	function verificaArquivo($arquivo)
	{
		if($arquivo["type"]=="text/pdf" or $arquivo["type"]=="application/pdf")
		{
			return true;
		}
		else
		{
			return false;
		}
	}






	function geraImg($img, $max_x, $max_y, $imgNome)
	{
		list($width, $height, $tipo) = getimagesize($img);
		$original_x = $width;
		$original_y = $height;
			
		if ($width == 0 OR $height == 0)
		{
			return false;
		}
		else
		{
			if($original_x > $original_y)
			{
				$porcentagem = (100 * $max_x) / $original_x;
			}
			else
			{
				$porcentagem = (100 * $max_y) / $original_y;
			}
				
			$tamanho_x = $original_x * ($porcentagem / 100);
			$tamanho_y = $original_y * ($porcentagem / 100);
			$image_p = imagecreatetruecolor($max_x, $max_y);


			if($tipo == IMAGETYPE_JPEG)
			{
				$image = imagecreatefromjpeg($img);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $max_x, $max_y, $original_x, $original_y);
				return imagejpeg($image_p, $imgNome, 100);
			}
				
			else if($tipo == IMAGETYPE_GIF)
			{
				$image = imagecreatefromgif($img);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $max_x, $max_y, $original_x, $original_y);
				return imagegif($image_p, $imgNome, 100);
			}
				
			else if($tipo == IMAGETYPE_PNG)
			{
				$image = imagecreatefrompng($img);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $max_x, $max_y, $original_x, $original_y);
				return imagepng($image_p, $imgNome, 100);
			}
			else
			{
				return false;
			}

		}
	}


	function ziparArquivo($nomeZip,$caminho,$arquivo)
	{
		if(empty($caminho) or empty($arquivo))
		{
			return false;
		}
		else
		{
				
			$zip = new ZipArchive;
			if($zip->open($caminho."/".$nomeZip, ZIPARCHIVE::CREATE) === true )
			{
					
				$zip->addFile($caminho."/".$arquivo,$arquivo);
				$zip->close();
				return true;
			}
			else
			{
				return false;
			}
		}
	}


	// Fun��o CPF
	function verificarCPF($cpf)
	{
		$cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);
		if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '99999999999') {
			return false;
		} else {
			for ($t = 9; $t < 11; $t++) {
				for ($d = 0, $c = 0; $c < $t; $c++) {
					$d += $cpf{$c} * (($t + 1) - $c);
				}
				$d = ((10 * $d) % 11) % 10;
				if ($cpf{$c} != $d) {
					return false;
				}
			}
			return true;
		}
	}
		
	// valida hora 23:59
		
	function verificaHora($time)
	{
		list($hour,$minute) = explode(':',$time);
		if(strlen ($time)==5)
		{
			if ($hour > -1 && $hour < 24 && $minute > -1 && $minute < 60)
			{
				return true;
			}
		}
	}

	function verificarCNPJ($cnpj)
	{
		if (strlen($cnpj) <> 18) return 0;
		$soma1 = ($cnpj[0] * 5) +
		($cnpj[1] * 4) +
		($cnpj[3] * 3) +
		($cnpj[4] * 2) +
		($cnpj[5] * 9) +
		($cnpj[7] * 8) +
		($cnpj[8] * 7) +
		($cnpj[9] * 6) +
		($cnpj[11] * 5) +
		($cnpj[12] * 4) +
		($cnpj[13] * 3) +
		($cnpj[14] * 2);
		$resto = $soma1 % 11;
		$digito1 = $resto < 2 ? 0 : 11 - $resto;
		$soma2 = ($cnpj[0] * 6) +
		($cnpj[1] * 5) +
		($cnpj[3] * 4) +
		($cnpj[4] * 3) +
		($cnpj[5] * 2) +
		($cnpj[7] * 9) +
		($cnpj[8] * 8) +
		($cnpj[9] * 7) +
		($cnpj[11] * 6) +
		($cnpj[12] * 5) +
		($cnpj[13] * 4) +
		($cnpj[14] * 3) +
		($cnpj[16] * 2);
		$resto = $soma2 % 11;
		$digito2 = $resto < 2 ? 0 : 11 - $resto;
		return (($cnpj[16] == $digito1) && ($cnpj[17] == $digito2));
	}


	function tamanhoFilmeYoutubeAntigo($valString,$largura,$altura)
	{
		$pegaInicio=explode("width",$valString);
		$pegaInicio2=explode(">",$pegaInicio[1]);
		$mudar1="width".$pegaInicio2[0];
			
		$passo1Fim=explode("width",$valString);
		$pegaFim=array_reverse($passo1Fim);
		$pegaFim2=explode(">",$pegaFim[0]);
		$mudar2="width".$pegaFim2[0];
			
		$filme=$valString;
		$filmeRetorno = str_replace($mudar1, "width=\"$largura\" height=\"$altura\"",$filme);
		$filmeRetorno2 = str_replace($mudar2, "width=\"$largura\" height=\"$altura\"",$filmeRetorno);
		$filmeRetorno3 = str_replace("&", "&amp;",$filmeRetorno2);
		return $filmeRetorno3;
	}
	
	function tamanhoFilmeYoutube($valString,$largura,$altura)
	{
		$filme=$valString;
		
		
		$filmeRetorno = str_replace('width="420" height="315"', "width=\"$largura\" height=\"$altura\"",$filme);
		$filmeRetorno = str_replace('width="480" height="360"', "width=\"$largura\" height=\"$altura\"",$filme);
		$filmeRetorno = str_replace('width="640" height="480"', "width=\"$largura\" height=\"$altura\"",$filme);
		$filmeRetorno = str_replace('width="960" height="720"', "width=\"$largura\" height=\"$altura\"",$filme);				
		
		return $filmeRetorno;
		
	}
	

	
	
	function removeAspaInvertida($valor)
	{
		$retornoString=str_replace('\"','"',$valor);
		return $retornoString;
	}
		
	function codificaSenha($valor)
	{
		$senhaCodificada=md5($valor);
		return $senhaCodificada;
	}

	function gerar_senha($tamanho, $maiusculas, $minusculas, $numeros, $simbolos){
		$ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ"; // $ma contem as letras maiusculas
		$mi = "abcdefghijklmnopqrstuvyxwz"; // $mi contem as letras minusculas
		$nu = "0123456789"; // $nu contem os numeros
		$si = "!@#$%�&*()_+="; // $si contem os sibolos
		$senha="";
		
		if ($maiusculas){
			// se $maiusculas for "true", a variavel $ma � embaralhada e adicionada para a variavel $senha
			$senha .= str_shuffle($ma);
		}
	
		if ($minusculas){
			// se $minusculas for "true", a variavel $mi � embaralhada e adicionada para a variavel $senha
			$senha .= str_shuffle($mi);
		}
	
		if ($numeros){
			// se $numeros for "true", a variavel $nu � embaralhada e adicionada para a variavel $senha
			$senha .= str_shuffle($nu);
		}
	
		if ($simbolos){
			// se $simbolos for "true", a variavel $si � embaralhada e adicionada para a variavel $senha
			$senha .= str_shuffle($si);
		}
	
		// retorna a senha embaralhada com "str_shuffle" com o tamanho definido pela variavel $tamanho
		return substr(str_shuffle($senha),0,$tamanho);
	}
	
	
	function validaUrl($url)
	{
		if(preg_match('|^http(s)?://[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url) or $url=="#")
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function validaBanner($arquivo)
	{
		if($arquivo["type"]=="application/x-shockwave-flash")
		{
			return true;
		}
		elseif($arquivo["type"]=="image/pjpeg")
		{
			return true;
		}
		elseif($arquivo["type"]=="image/gif")
		{
			return true;
		}
		elseif($arquivo["type"]=="image/jpeg")
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	function verificaExtensao($arquivo)
	{
		if($arquivo["type"]=="application/x-shockwave-flash")
		{
			return ".swf";
		}
		elseif($arquivo["type"]=="image/pjpeg")
		{
			return ".jpg";
		}
		elseif($arquivo["type"]=="image/gif")
		{
			return ".gif";
		}
		elseif($arquivo["type"]=="image/jpeg")
		{
			return ".jpg";
		}
		else
		{
			return false;
		}

	}
?>