<?php

function geraPaginacaoBasica($paginator,$baseURL,$totalRegistros,$totalPorPaginas)
{
	$config['base_url'] = '';
	$config['total_rows'] = 200;
	$config['per_page'] = 20;
	
	$paginator->pagination->initialize($config);
	
	return $paginator->pagination->create_links();
}

?>