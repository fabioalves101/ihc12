<?php
function MontaCkeditor()
{
	// Array com as configurações pra essa instância do CKEditor ( você pode ter mais de uma )
	$data['ckeditor_texto1'] = array
	(
			//id da textarea a ser substituída pelo CKEditor
			'id'   => 'texto1',
	
			// caminho da pasta do CKEditor relativo a pasta raiz do CodeIgniter
			'path' => 'system/application/js/ckeditor',
	
			// configurações opcionais
			'config' => array
			(
					'toolbar' => "Full",
					'width'   => "400px",
					'height'  => "100px",
			)
	);
	return $ckeditor;
}
?>