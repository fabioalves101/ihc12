<div id="centralizaCadastro">
    	<div id="Cadastro">
    	<div id="msgCadastro"><?php echo $msgCadastro; ?></div>
    		<form action="<?php echo base_url(); ?>dicionario/<?php echo $valorOperacao; ?>" method="post" enctype="multipart/form-data">
	        	<fieldset class="fieldsetCadastro">
                	<legend class="legendaCadastro"><?php echo $tituloCadastro;?></legend>
                	<label id="lbltitulo" for="titulo" class="labelCadastro">T&iacute;tulo</label>
					<input type="text" name="titulo" id="titulo" class="inputCadastro" value="<?php echo $valorCampoTitulo;?>" /><?php if(isset($erroTitulo)){echo " ".$erroTitulo;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />
					<label id="labelstatusPalavra" for="statusPalavara" class="labelCadastro">
					Status:</label>
					<?php 
					echo form_radio($checkAtivo)."Ativa";
					echo form_radio($checkDesativo)."Desativada";
					?>
				    <br class="brCadastro" /> 					
					<br class="brCadastro" />
					<label id="nome" for="texto1" class="labelDescricao">Descri&ccedil;&atilde;o<?php if(isset($erroTexto)){echo " ".$erroTexto;}?></label>
					<textarea id="texto1" name="texto1">
					    <?php echo $textoDescricao; ?>	
					</textarea>
					<?php echo display_ckeditor($ckeditor_texto1); ?>					
					<br class="brCadastro" />
                 <input type="submit" name="button" id="button" value="Gravar" class="botaoCadastro" />
                <input type="reset" name="button2" id="button2" value="Resetar" class="botaoCadastro" />				
                </fieldset>
          </form>
    	</div>
</div>