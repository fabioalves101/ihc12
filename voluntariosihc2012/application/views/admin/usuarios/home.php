<table class="tabelaPrincipal" summary="Lista de Usu&aacute;rios com a finalidade de manuten&ccedil;&atilde;o de conte&uacute;do. O cabe&ccedil;alho lido da esquerda para direita apresenta a seguinte ordem: Del que significa remo&ccedil;&atilde;o, Edit que significa edi&ccedil;&atilde;o, Nome,Login,Perfil,Data de Cadastro,Ativo">
	<caption>Lista de Usu&aacute;rios</caption>
	<thead>
	<tr  align="center" class="cabacalhoPesquisa">
		<th width="31" ><acronym title="Deletar/Remover">Del.</acronym></th>
		<th width="31" ><acronym title="Editar">Edit.</acronym></th>        
		<th width="31" ><acronym title="Definir Status">Sts.</acronym></th>
		<th >Nome</th>
		<th >Login</th>
		<th >Perfil</th>
		<th>Data Cadastro</th>
		<th>Status</th>
        </tr>
	</thead>

	<tfoot>
	<tr  align="center" class="cabacalhoPesquisa">
		<th width="31" ><acronym title="Deletar/Remover">Del.</acronym></th>
		<th width="31" ><acronym title="Editar">Edit.</acronym></th>   
		<th width="31" ><acronym title="Definir Status">Sts.</acronym></th>		     
		<th >Nome</th>
		<th >Login</th>
		<th >Perfil</th>
		<th>Data Cadastro</th>
		<th>Status</th>	
        </tr>
	</tfoot>
	<tbody>	
		<?php
		$mudaCor=0;
		if(isset($lista) && $totalRegistros>0)
		{	
		foreach($lista as $item)
		{ 
		$mudaCor++;
		?>
		<tr <?php if($mudaCor%2==0){ echo "class='fundoZebra1'"; }else{ echo "class='fundoZebra2'";} ?>>
    	<td><a href="<?php echo base_url().$caminhoDeletar.$item->codigo; ?>" onclick="javascript:return window.confirm('Tem certeza que deseja remover?');" title="Remover Palavra:<?php echo $item->nome?> "><img src="<?php echo base_url();?>assets/imagens/deletar.jpg"  border="0" alt="Remover: <?php echo $item->nome?>> " /></a></td>
    	<td>
	 	<a href="<?php echo base_url().$caminhoEditar.$item->codigo; ?>" title="Editar: <?php echo $item->nome?>">
		<img src="<?php echo base_url();?>assets/imagens/icon_adm_edit.gif" width="17" height="15" border="0" alt="Editar: <?php echo $item->nome?>" /></a></td>
    	<td>
	 	<a href="<?php echo base_url().$caminhoDefinirStatus."2/".$item->valorStatus."/".$item->codigo; ?>" title="Definir status: <?php echo $item->nome?>">
		<img src="<?php echo base_url();?>assets/imagens/ok.gif" width="17" height="15" border="0" alt="Definir status: <?php echo $item->nome?>" /></a></td>
		<td>
			<?php echo $item->nome?>
		</td>
		<td>
			<?php echo $item->login?>
		</td>
		<td>
			<?php echo $item->perfil?>
		</td>			
		<td>
			<?php	echo $item->datacadastro; ?>
		</td>
		<td>
			<?php echo $item->status?>
		</td>		
        </tr>
        <?php 
		}
		}
        else
        {
        	echo "<tr><td colspan='8'>Nenhum registro encontrado</td></tr>";         	
        }
        ?>
	</tbody>
</table>	
<div><?php echo $paginacao; ?></div>