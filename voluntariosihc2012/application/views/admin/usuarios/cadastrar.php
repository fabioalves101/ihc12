<div id="centralizaCadastro">
    	<div id="Cadastro">
    	<div id="msgCadastro"><?php echo $msgCadastro; ?></div>
    		<form action="<?php echo base_url(); ?>usuarios/<?php echo $valorOperacao; ?>" method="post" enctype="multipart/form-data">
	        	<fieldset class="fieldsetCadastro">
                	<legend class="legendaCadastro"><?php echo $tituloCadastro;?></legend>
                	<?php if($mostraPerfil){ ?>
                	<label id="lblPerfil" for="perfil" class="labelCadastro">Perfil</label>
                	<? echo form_dropdown('perfil',$listaPerfil,$itemMarcadoListaPerfil,'class="inputCadastro"');?><?php if(isset($erroPerfil)){echo " ".$erroPerfil;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />
					<?php }?>                	
                	<label id="lblNome" for="nome" class="labelCadastro">Nome</label>
					<input type="text" name="nome" id="nome" class="inputCadastro" value="<?php echo $valorCampoNome;?>" /><?php if(isset($erroNome)){echo " ".$erroNome;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />

					<label id="lblCurso" for="curso" class="labelCadastro">Curso</label>
					<input type="text" name="curso" id="curso" class="inputCadastro" value="<?php echo $valorCampoCurso;?>" /><?php if(isset($erroCurso)){echo " ".$erroCurso;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />					
					
					<?php 
						echo form_label('Semestre','semestre',array('class'=>'labelCadastro'));
						echo form_dropdown('semestre',$listaSemestre,$itemMarcadoListaSemestre,'class="inputCadastro"');
					?>
					<br class="brCadastro" />
					<br class="brCadastro" />	

					<label id="lblInstituicao" for="instituicao" class="labelCadastro">Institui&ccedil;&atilde;o</label>
					<input type="text" name="instituicao" id="instituicao" class="inputCadastro" value="<?php echo $valorCampoInstituicao;?>" /><?php if(isset($erroInstituicao)){echo " ".$erroInstituicao;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />
					

					<label id="lblDataNascimento" for="datanascimento" class="labelCadastro">Data Nascimento</label>
					<input type="text" name="datanascimento" id="datanascimento" class="inputCadastro" value="<?php echo $valorCampoDataNascimento;?>" /><?php if(isset($erroDataNascimento)){echo " ".$erroDataNascimento;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />					
					
					<?php 
						echo form_label('Sexo','sexo',array('class'=>'labelCadastro'));
						echo form_dropdown('sexo',$listaSexo,$itemMarcadoListaSexo,'class="inputCadastro"');
					?>
					<br class="brCadastro" />
					<br class="brCadastro" />	
					
					<label id="lblTelefonePessoal" for="telefonepessoal" class="labelCadastro">Telefone Pessoal</label>
					<input type="text" name="telefonepessoal" id="telefonepessoal" class="inputCadastro" value="<?php echo $valorCampoTelefonePessoal;?>" /><?php if(isset($erroTelefonePessoal)){echo " ".$erroTelefonePessoal;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />
					
					<label id="lblTelefoneContato" for="telefonecontato" class="labelCadastro">Telefone Contato</label>
					<input type="text" name="telefonecontato" id="telefonecontato" class="inputCadastro" value="<?php echo $valorCampoTelefoneContato;?>" />
					<br class="brCadastro" />
					<br class="brCadastro" />
									
					<?php 
						echo form_label('SBC','sociosbc',array('class'=>'labelCadastro'));
						echo form_dropdown('sociosbc',$listaSocio,$itemMarcadoListaSocio,'class="inputCadastro"');
					?>
					<br class="brCadastro" />
					<br class="brCadastro" />	
					
					<label id="lblNumeroSocio" for="numerosocio" class="labelCadastro">N&uacute;mero S&oacute;cio</label>
					<input type="text" name="numerosocio" id="numerosocio" class="inputCadastro" value="<?php echo $valorCampoNumeroSocio;?>" /><?php if(isset($erroNumeroSocio)){echo " ".$erroNumeroSocio;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />

					<label id="lblPagina" for="pagina" class="labelCadastro">P&aacute;gina Pessoal</label>
					<input type="text" name="pagina" id="pagina" class="inputCadastro" value="<?php echo $valorCampoPagina;?>" />
					<br class="brCadastro" />
					<br class="brCadastro" />
					
					
					<label id="lblEmail" for="email" class="labelCadastro">Email</label>
					<input type="text" name="email" id="email" class="inputCadastro" value="<?php echo $valorCampoEmail;?>" /><?php if(isset($erroEmail)){echo " ".$erroEmail;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />

					<label id="lblSenha" for="senha" class="labelCadastro">Senha</label>
					<input type="password" name="senha" id="senha" class="inputCadastro" /><?php if(isset($erroSenha)){echo " ".$erroSenha;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />
					
                 <input type="submit" name="button" id="button" value="Gravar" class="botaoCadastro" />
                <input type="reset" name="button2" id="button2" value="Resetar" class="botaoCadastro" />				
                </fieldset>
          </form>
    	</div>
</div>