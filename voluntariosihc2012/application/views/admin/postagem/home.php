<table class="tabelaPrincipal" summary="Lista de Postagem com a finalidade de manuten&ccedil;&atilde;o de conte&uacute;do. O cabe&ccedil;alho lido da esquerda para direita apresenta a seguinte ordem: Del que significa remo&ccedil;&atilde;o, Edit que significa edi&ccedil;&atilde;o, T&iacutetulo, Descri&ccedil;&atilde;o, Foto, V&iacute;deo, Data, Quem Cadastrou">
	<caption>Lista de Postagem</caption>
	<thead>
	<tr  align="center" class="cabacalhoPesquisa">
		<th width="31" ><acronym title="Deletar/Remover">Del.</acronym></th>
		<th width="31" ><acronym title="Editar">Edit.</acronym></th>        
		<th width="31" ><acronym title="Definir Status">Sts.</acronym></th>		
		<th >T&iacute;tulo</th>
		<!--<th >Descri&ccedil;&atilde;o</th>-->
		<th >Foto</th>
		<th >V&iacutedeo</th>
		<th >Data</th>
		<th >Coment&aacuterios</th>
		<th>Status</th>		
		<th >Usu&aacute;rio</th>
        </tr>
	</thead>

	<tfoot>
	<tr  align="center" class="cabacalhoPesquisa">
		<th width="31" ><acronym title="Deletar/Remover">Del.</acronym></th>
		<th width="31" ><acronym title="Editar">Edit.</acronym></th>
		<th width="31" ><acronym title="Definir Status">Sts.</acronym></th>		        
		<th >T&iacute;tulo</th>
		<!--<th >Descri&ccedil;&atilde;o</th>-->
		<th >Foto</th>
		<th >V&iacutedeo</th>		
		<th >Data</th>
		<th >Coment&aacuterios</th>
		<th>Status</th>		
		<th >Usu&aacute;rio</th>		
        </tr>
	</tfoot>
	<tbody>	
		<?php
		$mudaCor=0;
		if(isset($lista) && $totalRegistros>0)
		{	
		foreach($lista as $item)
		{ 
		$mudaCor++;
		?>
		<tr <?php if($mudaCor%2==0){ echo "class='fundoZebra1'"; }else{ echo "class='fundoZebra2'";} ?>>
    	<td><a href="<?php echo base_url().$caminhoDeletar.$item->codigo; ?>" onclick="javascript:return window.confirm('Tem certeza que deseja remover?');" title="Remover Palavra:<?php echo $item->titulo?> "><img src="<?php echo base_url();?>assets/imagens/deletar.jpg"  border="0" alt="Remover: <?php echo $item->titulo?>> " /></a></td>
    	<td>
	 	<a href="<?php echo base_url().$caminhoEditar.$item->codigo; ?>" title="Editar: <?php echo $item->titulo?>">
		<img src="<?php echo base_url();?>assets/imagens/icon_adm_edit.gif" width="17" height="15" border="0" alt="Editar: <?php echo $item->titulo?>" /></a></td>
		<td>
	 	<a href="<?php echo base_url().$caminhoDefinirStatus."2/".$item->valorStatus."/".$item->codigo; ?>" title="Definir status: <?php echo $item->titulo?>">
		<img src="<?php echo base_url();?>assets/imagens/ok.gif" width="17" height="15" border="0" alt="Definir status: <?php echo $item->titulo?>" /></a></td>
        <td><?php echo $item->titulo?>
			</td>
		<!--<td><?php echo $item->descricao?>
			</td>-->
		<td>
			<?php 
				if($item->thumb!="")
				{
					echo "<img src='".base_url()."assets/imagenspostagens/".$item->thumb."'>";	
				}
			?>
		</td>
		<td>
			<?php
			 	if($item->videopequeno!="")
			 	{
					echo $item->videopequeno;
			 	}
			?>
		</td>
		
		<td>
			<?php
			 	if($item->data!="")
			 	{
					echo $item->data;
			 	}
			?>
		</td>
		<td>
			<?php
			 	if($item->comentarios!="")
			 	{
			 	
			 	?>
					<a href="<?php echo base_url().$caminhoComentario."1/0/".$item->codigo; ?>" title="Visualizar Coment&aacute;rio: (<?php echo $item->comentarios?>)">(<?php echo $item->comentarios?>)</a>			 		
				<?php
			 	}
			?>
				</td>
		<td><?php echo $item->status;?>
			</td>		
		<td><?php echo $item->nomeusuario;?>
			</td>			
        </tr>
        <?php 
		}
		}
        else
        {
        	echo "<tr><td colspan='8'>Nenhum registro encontrado</td></tr>";         	
        }
        ?>
	</tbody>
</table>	
<div><?php echo $paginacao; ?></div>