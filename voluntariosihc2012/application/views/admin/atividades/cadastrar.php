<div id="centralizaCadastro">
    	<div id="Cadastro">
    	<div id="msgCadastro"><?php echo $msgCadastro; ?></div>
    		<form action="<?php echo base_url(); ?><?php echo $valorOperacao; ?>" method="post" enctype="multipart/form-data">
	        	<fieldset class="fieldsetCadastro">
                	<legend class="legendaCadastro"><?php echo $tituloCadastro;?></legend>
                	<label id="lbltitulo" for="titulo" class="labelCadastro">T&iacute;tulo</label>
					<input type="text" name="titulo" id="titulo" class="inputCadastro" value="<?php echo $valorCampoTitulo;?>" /><?php if(isset($erroTitulo)){echo " ".$erroTitulo;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />
					<?php 
						echo form_label('Turno','turno',array('class'=>'labelCadastro'));
						echo form_dropdown('turno',$listaTurno,$itemMarcadoListaTurno,'class="inputCadastro"');
					?>
					<br class="brCadastro" />
					<br class="brCadastro" />					
                	<label id="lblDataInicial" for="datainicial" class="labelCadastro">Dt.Inicial</label>
					<input type="text" name="datainicial" id="datainicial" class="inputCadastro" maxlength="10" value="<?php echo $valorCampoDataInicial;?>" /><?php if(isset($erroDataInicial)){echo " ".$erroDataInicial;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />
										
                	<label id="lblDataFinal" for="datafinal" class="labelCadastro">Dt.Final</label>
					<input type="text" name="datafinal" id="datafinal" class="inputCadastro" maxlength="10" value="<?php echo $valorCampoDataFinal;?>" /><?php if(isset($erroDataFinal)){echo " ".$erroDataFinal;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />
															
                	<label id="lblHoras" for="horas" class="labelCadastro">Horas</label>
					<input type="text" name="horas" id="horas" class="inputCadastro datahora" maxlength="2" value="<?php echo $valorCampoHoras;?>" /><?php if(isset($erroHoras)){echo " ".$erroHoras;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />

					<label id="lblVagas" for="vagas" class="labelCadastro">Vagas</label>
					<input type="text" name="vagas" id="vagas" class="inputCadastro" maxlength="4" value="<?php echo $valorCampoVagas;?>" /><?php if(isset($erroVagas)){echo " ".$erroVagas;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />					
					
					<label id="lbldescricao" for="texto1" class="labelDescricao">Descri&ccedil;&atilde;o<?php if(isset($erroTexto)){echo " ".$erroTexto;}?></label>
					<textarea id="texto1" name="texto1">
					    <?php echo $valorCampoDescricao; ?>	
					</textarea>
					<?php echo display_ckeditor($ckeditor_texto1); ?>					
					<br class="brCadastro" />
					<br class="brCadastro" />
						
                 <input type="submit" name="button" id="button" value="Gravar" class="botaoCadastro" />
                <input type="reset" name="button2" id="button2" value="Resetar" class="botaoCadastro" />				
                </fieldset>
          </form>
    	</div>
</div>