<table class="tabelaPrincipal" summary="Lista de atividades com a finalidade de manuten&ccedil;&atilde;o de conte&uacute;do. O cabe&ccedil;alho lido da esquerda para direita apresenta a seguinte ordem: Del que significa remo&ccedil;&atilde;o, Edit que significa edi&ccedil;&atilde;o, Nome da atividade">
	<caption>Lista de Atividades</caption>
	<thead>
	<tr  align="center" class="cabacalhoPesquisa">
		<th width="31" ><acronym title="Deletar/Remover">Del.</acronym></th>
		<th width="31" ><acronym title="Editar">Edit.</acronym></th>        
		<th >T&iacute;tulo</th>
		<th >Descri&ccedil;&atilde;o</th>
		<th >Data Inicial</th>
		<th >Data Final</th>
		<th >Horas</th>
		<th >Turno</th>
		<th >Vagas</th>		
		<th >Usu&aacute;rio</th>
        </tr>
	</thead>

	<tfoot>
	<tr  align="center" class="cabacalhoPesquisa">
		<th width="31" ><acronym title="Deletar/Remover">Del.</acronym></th>
		<th width="31" ><acronym title="Editar">Edit.</acronym></th>        
		<th >T&iacute;tulo</th>
		<th >Descri&ccedil;&atilde;o</th>
		<th >Data Inicial</th>
		<th >Data Final</th>
		<th >Horas</th>
		<th >Turno</th>
		<th >Vagas</th>
		<th >Usu&aacute;rio</th>		
        </tr>
	</tfoot>
	<tbody>	
		<?php
		$mudaCor=0;
		if(isset($lista) && $totalRegistros>0)
		{	
		foreach($lista as $item)
		{ 
		$mudaCor++;
		?>
		<tr <?php if($mudaCor%2==0){ echo "class='fundoZebra1'"; }else{ echo "class='fundoZebra2'";} ?>>
    	<td><a href="<?php echo base_url().$caminhoDeletar.$item->codigo; ?>" onclick="javascript:return window.confirm('Tem certeza que deseja remover?');" title="Remover"><img src="<?php echo base_url();?>assets/imagens/deletar.jpg"  border="0" alt="Remover>" /></a></td>
    	<td>
	 	<a href="<?php echo base_url().$caminhoEditar.$item->codigo; ?>" title="Editar">
		<img src="<?php echo base_url();?>assets/imagens/icon_adm_edit.gif" width="17" height="15" border="0" alt="Editar" /></a></td>
		<td><?php echo $item->titulo?></td>
		<td><?php echo $item->descricao?></td>
		<td><?php echo $item->datainicial?></td>
		<td><?php echo $item->datafinal?></td>
		<td><?php echo $item->horas?></td>
		<td><?php echo $item->turno?></td>
		<td><?php echo $item->vagas?></td>
		<td><?php echo $item->nomeusuario?></td>			
        </tr>
        <?php 
		}
		}
        else
        {
        	echo "<tr><td colspan='6'>Nenhum registro encontrado</td></tr>";         	
        }
        ?>
	</tbody>
</table>	
<div><?php echo $paginacao; ?></div>