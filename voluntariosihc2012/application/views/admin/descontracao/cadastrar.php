<div id="centralizaCadastro">
    	<div id="Cadastro">
    	<div id="msgCadastro"><?php echo $msgCadastro; ?></div>
    		<form action="<?php echo base_url(); ?>descontracao/<?php echo $valorOperacao; ?>" method="post" enctype="multipart/form-data">
	        	<fieldset class="fieldsetCadastro">
                	<legend class="legendaCadastro"><?php echo $tituloCadastro;?></legend>
                	<label id="lbltitulo" for="titulo" class="labelCadastro">T&iacute;tulo</label>
					<input type="text" name="titulo" id="titulo" class="inputCadastro" value="<?php echo $valorCampoTitulo;?>" /><?php if(isset($erroTitulo)){echo " ".$erroTitulo;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />
                	<label id="lblVideo" for="video" class="labelCadastro">V&iacute;deo</label>
					<textarea name="video" id="video" class="inputCadastro"> <?php echo $valorCampoVideo;?></textarea><?php if(isset($erroVideo)){echo " ".$erroVideo;}?>
					<br class="brCadastro" />
					<br class="brCadastro" />
					<label id="lblfoto" for="userfile" class="labelCadastro">Foto</label>
					<input type="file" name="userfile" id="userfile" /><?php if(isset($erroArquivo["error"])){echo $erroArquivo["error"];}?>
					<input type="hidden" name="arquivoExistente" id="arquivoExistente" value="<?php echo $valorCampoArquivoExistente;?>" />
					<br class="brCadastro" />
					<br class="brCadastro" />										
					<label id="lbldescricao" for="texto1" class="labelDescricao">Descri&ccedil;&atilde;o<?php if(isset($erroTexto)){echo " ".$erroTexto;}?></label>
					<textarea id="texto1" name="texto1">
					    <?php echo $valorCampoDescricao; ?>	
					</textarea>
					<?php echo display_ckeditor($ckeditor_texto1); ?>					
					<br class="brCadastro" />
					<br class="brCadastro" />
                 <input type="submit" name="button" id="button" value="Gravar" class="botaoCadastro" />
                <input type="reset" name="button2" id="button2" value="Resetar" class="botaoCadastro" />				
                </fieldset>
          </form>
    	</div>
</div>