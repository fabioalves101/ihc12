<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-BR" lang="pt-BR" dir="ltr">
<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta name="authors" content="Vitor Odenor Aquino da Silva" />
		<meta name="keywords" content="Sistema, SGC, CMS, Acessibilidade" />
		<meta name="description" content="Sistema de Gerenciamento de Conte&uacute;do aplicando t&eacute;cnicas de acessbilidade"/>
		<meta name="language" content="pt-br" />
      <title>CMS </title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/principal.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/geral.css" />
	<script language="javascript1.2" src="<?php echo base_url();?>assets/js/geral.js" type="text/javascript" ></script>
	</head>
   <body>   
	<div id="geral">
         <div id="topo"></div>
		<div id="menu">
			<ul class="menu">
			<?php foreach($nav_list  as $id => $nav_item): ?>
				<li class="">
					<?= anchor($id, $nav_item) ?>
				</li>
			<?php endforeach ?>
			</ul>
         </div>
		<div id="conteudo">
		<?php 		
		
			echo $contents;		
		?>
		</div>         
         <div id="rodape"></div>
   </div>
      </body>
</html>