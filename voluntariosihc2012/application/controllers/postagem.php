<?php
	class Postagem extends CI_Controller
	{	
		public $idusuario="";
		public $caminho="./assets/imagenspostagens/";
		public $menuPorPerfil="";
		public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$idusuario=$this->session->userdata('idusuario');
			if($idusuario=="")
			{
				redirect('login/index/', 'refresh');
			}
				
			$this->load->model('postagens_model');
			$this->load->helper('MontaMenu');
			$this->load->library('image_lib');
			$this->load->model('perfil_model');
			$this->menuPorPerfil=$this->perfil_model->listarModulosPorPerfil($this->session->userdata('idperfil'));

			if(in_array("Postagens",$this->menuPorPerfil)==false)
			{
				redirect('login/index/', 'refresh');
			}
				
		}
		
		public function index($pagina=null,$limite=null,$acao=null,$valorStatus=null,$id=null)
		{
			//Carregando Bibliotecas
			$paginator=$this->load->library('pagination');
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/postagem/home.php'))
			{
				show_404();
			}
			if($limite==null)
			{
			$limite=10;
			}
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;
				
			if($acao==1)
			{
				$this->session->set_userdata('idpostagem',$id);
				redirect('comentarios/', 'refresh');
			}
			else if($acao==2)
			{
				if($this->postagens_model->definirStatus($id,$valorStatus))
				{
					redirect('postagem/index/'.$pagina."/".$limite, 'refresh');
				}
			}
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."postagem/index/";
			$config['total_rows'] = $this->postagens_model->contarRegistros(3,$this->session->userdata('usuariofiltra'));
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->postagens_model->listarTodos($limite,$indice,3,$this->session->userdata('usuariofiltra'));
			$data['caminhoDeletar']="postagem/confirmarexclusao/";
			$data['caminhoEditar']="postagem/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoComentario']="postagem/index/".$pagina."/".$limite."/";
			$data['caminhoDefinirStatus']="postagem/index/".$pagina."/".$limite."/";
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> Gerenciamento postagem");
			$this->template->set('caminhoCadastrar','postagem/inserir/');
			$this->template->set('tituloFuncionalidade','Postagem');
			$this->template->set('caminhoFormularioBusca',"postagem/buscar/".$pagina."/".$limite."/");
			$this->template->set('caminholistarTudo','postagem/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','titulo');			
			$this->template->set('listaPesquisa',array('titulo'=>'T&iacute;tulo','descricao'=>'Descri&ccedil;&atilde;o'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/postagem/home',$data);			
		}

		public function buscar($pagina=null,$limite=null,$acao=null,$valorStatus=null,$id=null)
		{
			//Carregando Bibliotecas
			$this->load->library('pagination');

			if($acao==1)
			{
				$this->session->set_userdata('idpostagem',$id);
				redirect('comentarios/', 'refresh');
			}
			else if($acao==2)
			{
				if($this->postagens_model->definirStatus($id,$valorStatus))
				{
					redirect('postagem/buscar/'.$pagina."/".$limite, 'refresh');
				}
			}
				
			
			$campo=$this->input->post('campo');
			
			if($campo!="")
			{
				
				$this->session->set_userdata('campo',$this->input->post('campo'));
				$this->session->set_userdata('palavra',$this->input->post('palavra'));
			}
			//Validando vari�veis
			$limite=10;
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;	
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."postagem/buscar/";
			$config['total_rows'] = $this->postagens_model->contarBusca($this->session->userdata('campo'),$this->session->userdata('palavra'),3,$this->session->userdata('usuariofiltra'));
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->postagens_model->buscarPaginado($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice,3,$this->session->userdata('usuariofiltra'));
			$data['caminhoDeletar']="postagem/confirmarexclusao/";
			$data['caminhoEditar']="postagem/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoComentario']="postagem/index/".$pagina."/".$limite."/";			
			$data['caminhoDefinirStatus']="postagem/buscar/".$pagina."/".$limite."/";
						
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> Gerenciamento postagem");
			$this->template->set('caminhoCadastrar','postagem/inserir/');
			$this->template->set('caminholistarTudo','postagem/');
			$this->template->set('tituloFuncionalidade','Postagem');
			$this->template->set('caminhoFormularioBusca',"postagem/buscar/".$pagina."/".$limite."/");
			$this->template->set('valuePalavraPesquisa',$this->session->userdata('palavra'));
			$this->template->set('itemMarcadoListaPesquisa',$this->session->userdata('campo'));
			$this->template->set('listaPesquisa',array('titulo'=>'T&iacute;tulo','descricao'=>'Descri&ccedil;&atilde;o'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));

			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/postagem/home',$data);
		}
		
		public function Inserir($acao=null)
		{
			//Carregando Bibliotecas
			$configArquivo['upload_path'] = $this->caminho;
			$configArquivo['allowed_types'] = 'gif|jpg|png';
			$configArquivo['max_size']	= '1000';
			$configArquivo['encrypt_name']	= 'true';
			
				
			$this->carregaBibliotecasCadastrar($configArquivo);
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/postagem/cadastrar.php'))
			{
				show_404();
			}
			
				
			//Atribuindo valores que ser�o repassados a p�gina
			$data['tituloCadastro']="Inserir postagem";
			$data['valorOperacao']="Inserir/1/";
			$data['valorCampoTitulo']="";
			$data['valorCampoVideo']="";
			$data['valorCampoDescricao']="";
			$data['valorCampoArquivo']="";
			$data['valorCampoArquivoExistente']="";
			$data['msgCadastro']='';
			$data['ckeditor_texto1'] = array
			(
					//id da textarea a ser substitu�da pelo CKEditor
					'id'   => 'texto1',
						
					// caminho da pasta do CKEditor relativo a pasta raiz do CodeIgniter
					'path' => 'assets/js/ckeditor',
						
					// configura��es opcionais
					'config' => array
					(
							'toolbar' => "Basic",
							'width'   => "400px",
							'height'  => "100px",
					)
			);
				
			//Atribuindo vari�veis de template
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."postagem' class='breadcumb' title='Gerenciamento postagem'>Gerenciamento postagem</a> -> Inserir postagem ");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			
			if(isset($acao))
			{
				$erro=false;
				
				//Mantendo estado dos campos
				$data['valorCampoTitulo']=$this->input->post('titulo');
				$data['valorCampoVideo']=$this->input->post('video');
				$data['valorCampoDescricao']=$this->input->post('texto1');
				
				// Validando informa��es
				if(!verificaString($this->input->post('titulo')))
				{
					$erro=true;
					$data['erroTitulo']='O campo t&iacutetulo &eacute obrigat&oacuterio';
				}else{$data['erroTitulo']='';}
				
				if(!verificaString($this->input->post('texto1')))
				{
					$erro=true;
					$data['erroTexto']='O campo descri&ccedil;&atilde;o &eacute obrigat&oacuterio';
				}else{$data['erroTexto']='';}
				
				if ($this->upload->do_upload('userfile')==false)
				{
					$erro =true;
					$data['erroArquivo']=array('error' => $this->upload->display_errors());
				}
				else
				{	
					$data['erroArquivo']='';
				}
				
				if($erro==false)
				{
					$valorUpload=$this->upload->data();
					
					
					//Gerando Imagem
					if(file_exists($this->caminho.$valorUpload["file_name"]))
					{
						
						$caminhoImagem=$this->caminho.$valorUpload["file_name"];
						
						//Gerando Thumb
						$novaImagem=$this->caminho."thumb_".$valorUpload["file_name"];
						$this->geraImagem($caminhoImagem,$novaImagem,100,75);
					
						//Gerando Imagem
						$novaImagem=$this->caminho.$valorUpload["file_name"];
						$this->geraImagem($caminhoImagem,$caminhoImagem,400,300);
					}	
						
					$arrayValores=array(
							'titulo'=>$this->input->post('titulo'),
							'video'=>$this->input->post('video'),
							'descricao'=>$this->input->post('texto1'),
							'thumb'=>"thumb_".$valorUpload["file_name"],
							'foto'=>$valorUpload["file_name"],
							'video'=>tamanhoFilmeYoutube($this->input->post('video'),400,300),
							'videopequeno'=>tamanhoFilmeYoutube($this->input->post('video'),100,75),
							'data'=>date('Y-m-d H:i:s'),
							'usuario_id'=>'1',
							'tipo'=>'3',
							'ativado'=>'0');
					$this->Gravar($arrayValores,$acao);
					$data['msgCadastro']='Sucesso';
				}
			}			
			
			$this->template->load('templates/adminCadastro', 'admin/postagem/cadastrar',$data);
		}
		
		public function Atualizar($id,$acao=0)
		{
			$retornoAtualiza=$this->obterPorID($id);
			
			$configArquivo['upload_path'] = $this->caminho;
			$configArquivo['allowed_types'] = 'gif|jpg|png|jpeg';
			$configArquivo['max_size']	= '1000';
			$configArquivo['encrypt_name']	= 'false';
			$configArquivo['overwrite']	= 'true';
			$configArquivo['file_name']=$retornoAtualiza->foto;
			

			$this->carregaBibliotecasCadastrar($configArquivo);
							
			
			if ( ! file_exists('application/views/admin/postagem/cadastrar.php'))
			{
				show_404();
			}				
			
			$data['tituloCadastro']="Atualizar Postagem ".$retornoAtualiza->titulo;
			$data['valorOperacao']="Atualizar/".$id."/2";
			$data['valorCampoTitulo']=$retornoAtualiza->titulo;
			$data['valorCampoVideo']=$retornoAtualiza->video;
			$data['valorCampoDescricao']=$retornoAtualiza->descricao;
			$data['valorCampoArquivoExistente']=$retornoAtualiza->foto;
			$data['ckeditor_texto1'] = array
			(
					//id da textarea a ser substitu�da pelo CKEditor
					'id'   => 'texto1',
			
					// caminho da pasta do CKEditor relativo a pasta raiz do CodeIgniter
					'path' => 'assets/js/ckeditor',
			
					// configura��es opcionais
					'config' => array
					(
							'toolbar' => "Basic",
							'width'   => "400px",
							'height'  => "100px",
					)
			);
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."postagem' class='breadcumb' title='Gerenciamento postagem'>Gerenciamento postagem</a> -> Atualizar Postagem");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			$data['msgCadastro']='';
		
			if($acao!=0)
			{
				$erro=false;
				$valorUpload="";
				//Mantendo estado dos campos
				$data['valorCampoTitulo']=$this->input->post('titulo');
				$data['valorCampoVideo']=$this->input->post('video');
				$data['valorCampoDescricao']=$this->input->post('texto1');
				$data['valorCampoArquivoExistente']=$this->input->post('arquivoExistente');
				
				// Validando informa��es
				if(!verificaString($this->input->post('titulo')))
				{
					$erro=true;
					$data['erroTitulo']='O campo t&iacutetulo &eacute obrigat&oacuterio';
				}else{$data['erroTitulo']='';}
				
				if(!verificaString($this->input->post('texto1')))
				{
					$erro=true;
					$data['erroTexto']='O campo descri&ccedil;&atilde;o &eacute obrigat&oacuterio';
				}else{$data['erroTexto']='';}
				
				
				if(verificaString($_FILES['userfile']['name']))
				{	
				if (!$this->upload->do_upload('userfile'))
					{
						$erro =true;
						$data['erroArquivo']=array('error' => $this->upload->display_errors());
					}
				else
					{	
						$this->load->helper('file');
						$valorUpload=$this->upload->data();
						if(file_exists($this->caminho.$valorUpload["file_name"]))
						{
							unlink($this->caminho."thumb_".$valorUpload["file_name"]);
						}
						$data['erroArquivo']='';
					}
				}
				
				if($erro==false)
				{	
					if(isset($valorUpload["file_name"]))
					{
					//Gerando Imagem
					if(file_exists($this->caminho.$valorUpload["file_name"]))
					{
					
						$caminhoImagem=$this->caminho.$valorUpload["file_name"];
					
						//Gerando Thumb
						$novaImagem=$this->caminho."thumb_".$valorUpload["file_name"];
						$this->geraImagem($caminhoImagem,$novaImagem,100,75);
							
						//Gerando Imagem
						$novaImagem=$this->caminho.$valorUpload["file_name"];
						$this->geraImagem($caminhoImagem,$caminhoImagem,400,300);
					}
					}
					
					if(!verificaString($valorUpload))
					{
						$arrayValores=array(
								'titulo'=>$this->input->post('titulo'),
								'descricao'=>$this->input->post('texto1'),
								'video'=>tamanhoFilmeYoutube($this->input->post('video'),400,300),
								'video'=>tamanhoFilmeYoutube($this->input->post('video'),400,300),
								'videopequeno'=>tamanhoFilmeYoutube($this->input->post('video'),100,75),
								'data'=>date('Y-m-d H:i:s'),
								'usuario_id'=>'1',
								'tipo'=>'3');
					}
					else
					{
					$arrayValores=array(
							'titulo'=>$this->input->post('titulo'),
							'descricao'=>$this->input->post('texto1'),
							'thumb'=>"thumb_".$valorUpload["file_name"],
							'foto'=>$valorUpload["file_name"],
							'video'=>tamanhoFilmeYoutube($this->input->post('video'),400,300),
							'videopequeno'=>tamanhoFilmeYoutube($this->input->post('video'),100,75),
							'data'=>date('Y-m-d H:i:s'),
							'usuario_id'=>'1',
							'tipo'=>'2');
					}
					$this->Gravar($arrayValores,$acao,$id);
					$data['msgCadastro']='Sucesso';
				}
			}
			$this->template->load('templates/adminCadastro', 'admin/postagem/cadastrar',$data);
		}

		public function ConfirmarExclusao($id=null)
		{
			$data['tituloCadastro']="Confirmar Exclus&atildeo";
			$data['valorOperacao']="postagem/Excluir/".$id."/1";
			
			$retornoAtualiza=$this->obterPorID($id);
			
			$data['valorCampoTitulo']=$retornoAtualiza->titulo;
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."postagem' class='breadcumb' title='Gerenciamento postagem'>Gerenciamento postagem</a> -> Confirmar Exclus&atildeo");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			$this->template->load('templates/adminCadastro', 'admin/postagem/confirmarexclusao',$data);				
		}

		public function Excluir($id=null)
		{
			$retorno=$this->postagens_model->ObterPorId($id);
			if($retorno!=null)
			{
				if(file_exists($this->caminho.$retorno->foto))
				{
					unlink($this->caminho.$retorno->foto);
					unlink($this->caminho.$retorno->thumb);
					$this->postagens_model->deletar($id);
					redirect('postagem/', 'refresh');
				}
			}
		}
		
		public function Gravar($valores=array(),$acao,$id=null)
		{
			if($acao==1)
			{
				$this->postagens_model->inserir($valores);
			}
			else
			{
				$this->postagens_model->atualizar($id,$valores);
			}
		}

		public function carregaBibliotecasCadastrar($configUp)
		{
			$this->load->helper('ckeditor');
			$this->load->helper('TrataDados');				
			$this->load->library('unit_test');	
			$this->load->library('upload', $configUp);
		}
		
		public function obterPorID($id)
		{
			return $this->postagens_model->obterPorID($id);			
		}		
		
		public function geraImagem($imagem,$nomeImagem,$altura,$largura)
		{
			$configImagem['image_library'] = 'gd2';
			$configImagem['source_image']	= $imagem;
			$config['maintain_ratio'] = TRUE;
			$config['master_dim']="auto";
			$configImagem['width']	 = $largura;
			$configImagem['height']	= $altura;
			$configImagem['new_image']=$nomeImagem;
			$this->image_lib->initialize($configImagem);
			
			$this->image_lib->resize();
			$this->image_lib->clear();
		}
	}

?>