<?php
	class Materiais extends CI_Controller
	{			
		public $idusuario="";
		public $caminho="./assets/arquivosmateriais/";
		public $menuPorPerfil="";
		public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$idusuario=$this->session->userdata('idusuario');
			if($idusuario=="")
			{
				redirect('login/index/', 'refresh');
			}
				
			$this->load->model('materiais_model');
			$this->load->helper('MontaMenu');
			$this->load->model('perfil_model');
			$this->menuPorPerfil=$this->perfil_model->listarModulosPorPerfil($this->session->userdata('idperfil'));

			if(in_array("Materiais",$this->menuPorPerfil)==false)
			{
				redirect('login/index/', 'refresh');
			}
				
		}
		
		public function index($pagina=null,$limite=null,$acao=null)
		{
			//Carregando Bibliotecas
			$paginator=$this->load->library('pagination');
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/materiais/home.php'))
			{
				show_404();
			}
			if($limite==null)
			{
			$limite=10;
			}
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;
				
			if($acao!=0)
			{
				$arquivo=$this->materiais_model->obterPorID($acao);
				$this->load->helper('download');
				
				$data = file_get_contents($this->caminho.$arquivo->arquivo); // Read the file's contents
				$name = $arquivo->titulo;				
				force_download($name, $data);
			}
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."materiais/index/";
			$config['total_rows'] = $this->materiais_model->contarRegistros();
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->materiais_model->listarTodos($limite,$indice);
			$data['caminhoDeletar']="materiais/confirmarexclusao/";
			$data['caminhoEditar']="materiais/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoDownload']="materiais/index/".$pagina."/".$limite."/";
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> Gerenciamento Materiais");
			$this->template->set('caminhoCadastrar','materiais/inserir/');
			$this->template->set('tituloFuncionalidade','Materiais');
			$this->template->set('caminhoFormularioBusca','materiais/buscar/');
			$this->template->set('caminholistarTudo','materiais/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','titulo');			
			$this->template->set('listaPesquisa',array('titulo'=>'T&iacute;tulo','tipo'=>'Tipo'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/materiais/home',$data);			
		}

		public function buscar($pagina=null,$limite=null,$acao=null)
		{
			//Carregando Bibliotecas
			$this->load->library('pagination');
			
			$campo=$this->input->post('campo');
			
			if($campo!="")
			{
				
				$this->session->set_userdata('campo',$this->input->post('campo'));
				$this->session->set_userdata('palavra',$this->input->post('palavra'));
			}
			//Validando vari�veis
			$limite=10;
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;	

			
			
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."materiais/buscar/";
			$config['total_rows'] = $this->materiais_model->contarBusca($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice);
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->materiais_model->buscarPaginado($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice);
			$data['caminhoDeletar']="materiais/confirmarexclusao/";
			$data['caminhoEditar']="materiais/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoDownload']="materiais/index/".$pagina."/".$limite."/";
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> Gerenciamento Materiais");
			$this->template->set('caminhoCadastrar','materiais/inserir/');
			$this->template->set('caminholistarTudo','materiais/');
			$this->template->set('tituloFuncionalidade','Materiais');
			$this->template->set('caminhoFormularioBusca','materiais/buscar/');
			$this->template->set('valuePalavraPesquisa',$this->session->userdata('palavra'));
			$this->template->set('itemMarcadoListaPesquisa',$this->session->userdata('campo'));
			$this->template->set('listaPesquisa',array('titulo'=>'T&iacute;tulo','tipo'=>'Tipo'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));

			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/materiais/home',$data);
		}
		
		
		public function Inserir($acao=null)
		{
			//Carregando Bibliotecas
			$configArquivo['upload_path'] = $this->caminho;
			$configArquivo['allowed_types'] = 'gif|jpg|png|txt|pdf|doc|xls|ppt';
			$configArquivo['max_size']	= '1000';
			$configArquivo['encrypt_name']	= 'true';
				
			$this->carregaBibliotecasCadastrar($configArquivo);
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/materiais/cadastrar.php'))
			{
				show_404();
			}
			
				
			//Atribuindo valores que ser�o repassados a p�gina
			$data['tituloCadastro']="Inserir Material";
			$data['valorOperacao']="Inserir/1/";
			$data['valorCampoTitulo']="";
			$data['valorCampoArquivo']="";
			$data['valorCampoArquivoExistente']="";
			$data['msgCadastro']='';
				
			//Atribuindo vari�veis de template
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."materiais' class='breadcumb' title='Gerenciamento Materiais'>Gerenciamento Materiais</a> -> Inserir Materiais ");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			
			if(isset($acao))
			{
				$erro=false;
				
				//Mantendo estado dos campos
				$data['valorCampoArquivo']=$this->input->post('userfile');
				
				// Validando informa��es
				if (!$this->upload->do_upload('userfile'))
				{
					$erro =true;
					$data['erroArquivo']=array('error' => $this->upload->display_errors());
				}
				else
				{	
					$data['erroArquivo']='';
				}
				
				if($erro==false)
				{
					$valorUpload=$this->upload->data();
					$arrayValores=array(
							'titulo'=>$valorUpload["orig_name"],
							'arquivo'=>$valorUpload["file_name"],
							'usuario_id'=>'1',
							'tipo'=>$valorUpload["file_type"]);
					$this->Gravar($arrayValores,$acao);
					$data['msgCadastro']='Sucesso';
				}
			}			
			
			$this->template->load('templates/adminCadastro', 'admin/materiais/cadastrar',$data);
		}
		
		public function Atualizar($id,$acao=0)
		{
			$retornoAtualiza=$this->obterPorID($id);
			
			$configArquivo['upload_path'] = $this->caminho;
			$configArquivo['allowed_types'] = 'gif|jpg|png|txt|pdf|doc|xls|ppt';
			$configArquivo['max_size']	= '1000';
			$configArquivo['encrypt_name']	= 'false';
			$configArquivo['overwrite']	= 'true';
			$configArquivo['file_name']=$retornoAtualiza->arquivo;
			
			$this->carregaBibliotecasCadastrar($configArquivo);
				
			
			if ( ! file_exists('application/views/admin/materiais/cadastrar.php'))
			{
				show_404();
			}
			
				
			

			$data['tituloCadastro']="Atualizar Arquivo ".$retornoAtualiza->titulo;
			$data['valorOperacao']="Atualizar/".$id."/2";
				
			$data['valorCampoArquivoExistente']=$retornoAtualiza->arquivo;
				
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."materiais' class='breadcumb' title='Gerenciamento Materiais'>Gerenciamento Materiais</a> -> Atualizar Material");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			$data['msgCadastro']='';

				
			
			if($acao!=0)
			{
				$erro=false;
				
				//Mantendo estado dos campos
				$data['valorCampoArquivoExistente']=$this->input->post('valorCampoArquivoExistente');
				
				// Validando informa��es
				if (!$this->upload->do_upload('userfile'))
				{
					$erro =true;
					$data['erroArquivo']=array('error' => $this->upload->display_errors());
				}
				else
				{	$this->load->helper('file');
					$data['erroArquivo']='';
				}
								
				if($erro==false)
				{
					$valorUpload=$this->upload->data();
					$arrayValores=array(
							'titulo'=>$valorUpload["client_name"],
							'arquivo'=>$valorUpload["file_name"],
							'usuario_id'=>'1',
							'tipo'=>$valorUpload["file_type"]);
					$this->Gravar($arrayValores,$acao,$id);
					$data['msgCadastro']='Sucesso';
				}
			}
			$this->template->load('templates/adminCadastro', 'admin/materiais/cadastrar',$data);
				
		}

		public function ConfirmarExclusao($id=null)
		{
			$data['tituloCadastro']="Confirmar Exclus&atildeo";
			$data['valorOperacao']="materiais/Excluir/".$id."/1";
			
			$retornoAtualiza=$this->obterPorID($id);
			
			$data['valorCampoTitulo']=$retornoAtualiza->titulo;
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."materiais' class='breadcumb' title='Gerenciamento Materiais'>Gerenciamento Materiais</a> -> Confirmar Exclus&atildeo");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			$this->template->load('templates/adminCadastro', 'admin/materiais/confirmarexclusao',$data);				
		}

		public function Excluir($id=null)
		{
			if(file_exists($this->caminho.$this->materiais_model->ObterPorId($id)->arquivo))
			{
				unlink($this->caminho.$this->materiais_model->ObterPorId($id)->arquivo);
				$this->materiais_model->deletar($id);
				redirect('materiais/', 'refresh');
			}
		}
		
		public function Gravar($valores=array(),$acao,$id=null)
		{
			if($acao==1)
			{
				$this->materiais_model->inserir($valores);
			}
			else
			{
				$this->materiais_model->atualizar($id,$valores);
			}
		}

		public function carregaBibliotecasCadastrar($config)
		{
			$this->load->library('unit_test');
			$this->load->helper('ckeditor');
			$this->load->helper('TrataDados');
			$this->load->library('upload', $config);
		}
		
		public function obterPorID($id)
		{
			return $this->materiais_model->obterPorID($id);			
		}		
	}

?>