<?php
	class Comentarios extends CI_Controller
	{				
		public $idusuario="";
		public $menuPorPerfil="";
		public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$idusuario=$this->session->userdata('idusuario');
			if($idusuario=="")
			{
				redirect('login/index/', 'refresh');
			}
			
			$this->load->model('comentarios_model');
			$this->load->model('postagens_model');
			$this->load->helper('MontaMenu');
			$this->load->model('perfil_model');
			$this->menuPorPerfil=$this->perfil_model->listarModulosPorPerfil($this->session->userdata('idperfil'));
			
			
		}
		
		public function index($pagina=null,$limite=null,$acao=null,$valorStatus=null,$id=null)
		{
			//Carregando Bibliotecas
			$paginator=$this->load->library('pagination');
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/comentarios/home.php'))
			{
				show_404();
			}
			if($limite==null)
			{
			$limite=10;
			}
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;

			if($acao==2)
			{
				if($this->comentarios_model->definirStatus($id,$valorStatus))
				{
					redirect('comentarios/index/'.$pagina."/".$limite, 'refresh');
				}							
			}
		
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."comentarios/index/";
			$config['total_rows'] = $this->comentarios_model->contarRegistros($this->session->userdata('idpostagem'));
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();
			
			//Obtendo valor da postagem
			$retornoPostagem=$this->postagens_model->obterPorID($this->session->userdata('idpostagem'));
			
			if($retornoPostagem->tipo=="1")
			{
				$areaPostagem=" da Descontra&ccedil;&atilde;o: ".$retornoPostagem->titulo;
				$linkBreadcrumb="descontracao/";
			}
			else if($retornoPostagem->tipo=="2")
			{
				$areaPostagem="de Dicas: ".$retornoPostagem->titulo;
				$linkBreadcrumb="dicas/";
			}
			else if($retornoPostagem->tipo=="3")
			{
				$areaPostagem="de Postagem: ".$retornoPostagem->titulo;
				$linkBreadcrumb="postagem/";
			}
			
			
			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->comentarios_model->listarTodos($limite,$indice,$this->session->userdata('idpostagem'));
			$data['caminhoDeletar']="comentarios/confirmarexclusao/";
			$data['caminhoEditar']="comentarios/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoDefinirStatus']="comentarios/index/".$pagina."/".$limite."/";
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaPostagem."' >Gerenciamento ".$areaPostagem." </a> -> Gerenciamento Coment&aacuterio");
			$this->template->set('caminhoCadastrar','comentarios/inserir/');
			$this->template->set('tituloFuncionalidade','Coment&aacuterio'.$areaPostagem);
			$this->template->set('caminhoFormularioBusca','comentarios/buscar/');
			$this->template->set('caminholistarTudo','comentarios/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','titulo');			
			$this->template->set('listaPesquisa',array('descricao'=>'Descri&ccedil;&atilde;o'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/comentarios/home',$data);			
		}

		public function buscar($pagina=null,$limite=null,$acao=null,$valorStatus=null,$id=null)
		{
			//Carregando Bibliotecas
			$this->load->library('pagination');
			
			$campo=$this->input->post('campo');
			
			if($campo!="")
			{				
				$this->session->set_userdata('campo',$this->input->post('campo'));
				$this->session->set_userdata('palavra',$this->input->post('palavra'));
			}
			//Validando vari�veis
			$limite=10;
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;	

			//Obtendo valor da postagem
			$retornoPostagem=$this->postagens_model->obterPorID($this->session->userdata('idpostagem'));
				
			if($retornoPostagem->tipo=="1")
			{
				$areaPostagem=" da Descontra&ccedil;&atilde;o: ".$retornoPostagem->titulo;
				$linkBreadcrumb="descontracao/";
			}
			else if($retornoPostagem->tipo=="2")
			{
				$areaPostagem="de Dicas: ".$retornoPostagem->titulo;
				$linkBreadcrumb="dicas/";
			}
			else if($retornoPostagem->tipo=="3")
			{
				$areaPostagem="de Postagem: ".$retornoPostagem->titulo;
				$linkBreadcrumb="postagem/";
			}
				
			if($acao==2)
			{
				if($this->comentarios_model->definirStatus($id,$valorStatus))
				{
					redirect('comentarios/index/'.$pagina."/".$limite, 'refresh');
				}
			}			
			
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."comentarios/buscar/";
			$config['total_rows'] = $this->comentarios_model->contarBusca($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice,$this->session->userdata('idpostagem'));
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->comentarios_model->buscarPaginado($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice,$this->session->userdata('idpostagem'));
			$data['caminhoDeletar']="comentarios/confirmarexclusao/";
			$data['caminhoEditar']="comentarios/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoDefinirStatus']="comentarios/index/".$pagina."/".$limite."/";
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$data['lista']=$this->comentarios_model->listarTodos($limite,$indice,$this->session->userdata('idpostagem'));
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaPostagem."' >Gerenciamento ".$areaPostagem." </a> -> Gerenciamento Coment&aacuterio");
			$this->template->set('caminhoCadastrar','comentarios/inserir/');
			$this->template->set('tituloFuncionalidade','Coment&aacuterio'.$areaPostagem);
			$this->template->set('caminhoFormularioBusca','comentarios/buscar/');
			$this->template->set('caminholistarTudo','comentarios/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','titulo');			
			$this->template->set('listaPesquisa',array('descricao'=>'Descri&ccedil;&atilde;o'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/comentarios/home',$data);
		}
		
		
		public function Inserir($acao=null)
		{
			//Carregando Bibliotecas
			$this->carregaBibliotecasCadastrar();
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/comentarios/cadastrar.php'))
			{
				show_404();
			}
			
			//Obtendo valor da postagem
			$retornoPostagem=$this->postagens_model->obterPorID($this->session->userdata('idpostagem'));
				
			if($retornoPostagem->tipo=="1")
			{
				$areaPostagem=" da Descontra&ccedil;&atilde;o: ".$retornoPostagem->titulo;
				$linkBreadcrumb="descontracao/";
			}
			else if($retornoPostagem->tipo=="2")
			{
				$areaPostagem="de Dicas: ".$retornoPostagem->titulo;
				$linkBreadcrumb="dicas/";
			}
			else if($retornoPostagem->tipo=="3")
			{
				$areaPostagem="de Postagem: ".$retornoPostagem->titulo;
				$linkBreadcrumb="postagem/";
			}
			
			
			//Atribuindo valores que ser�o repassados a p�gina
			$data['tituloCadastro']="Inserir Palavras";
			$data['valorOperacao']="Inserir/1/";
			$data['textoDescricao']="";
			$data['ckeditor_texto1'] = array
			(
					//id da textarea a ser substitu�da pelo CKEditor
					'id'   => 'texto1',
						
					// caminho da pasta do CKEditor relativo a pasta raiz do CodeIgniter
					'path' => 'assets/js/ckeditor',
						
					// configura��es opcionais
					'config' => array
					(
							'toolbar' => "Basic",
							'width'   => "400px",
							'height'  => "100px",
					)
			);
			$data['msgCadastro']='';
				
			//Atribuindo vari�veis de template
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaPostagem."' >Gerenciamento ".$areaPostagem." </a> -> <a href='".base_url()."comentarios/'>Gerenciamento Coment&aacuterio</a> -> Inserir Coment&aacute;rio");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			
			if(isset($acao))
			{
				$erro=false;
				
				//Mantendo estado dos campos
				$data['textoDescricao']=$this->input->post('texto1');
				
			
				// Validando informa��es
				if(!verificaString($this->input->post('texto1')))
				{
					$erro=true;
					$data['erroTexto']='O campo descri&ccedil;&atilde;o &eacute obrigat&oacuterio';
				}else{$data['erroTexto']='';}
				
				if($erro==false)
				{
					$arrayValores=array(
							'postagem_id'=>$this->session->userdata('idpostagem'),
							'descricao'=>$this->input->post('texto1'),
							'resposta'=>'',
							'usuario_id'=>'1',
							'ativado'=>'0',
						
							);
					$this->Gravar($arrayValores,$acao);
					$data['msgCadastro']='Sucesso';
				}
			}			
			
			$this->template->load('templates/adminCadastro', 'admin/comentarios/cadastrar',$data);
		}
		
		public function Atualizar($id,$acao=0)
		{
			$this->carregaBibliotecasCadastrar();
			
			if ( ! file_exists('application/views/admin/comentarios/cadastrar.php'))
			{
				show_404();
			}

			//Obtendo valor da postagem
			$retornoPostagem=$this->postagens_model->obterPorID($this->session->userdata('idpostagem'));
			
			if($retornoPostagem->tipo=="1")
			{
				$areaPostagem=" da Descontra&ccedil;&atilde;o: ".$retornoPostagem->titulo;
				$linkBreadcrumb="descontracao/";
			}
			else if($retornoPostagem->tipo=="2")
			{
				$areaPostagem="de Dicas: ".$retornoPostagem->titulo;
				$linkBreadcrumb="dicas/";
			}
			else if($retornoPostagem->tipo=="3")
			{
				$areaPostagem="de Postagem: ".$retornoPostagem->titulo;
				$linkBreadcrumb="postagem/";
			}
				
			
			$data['tituloCadastro']="Atualizar Coment&aacute;rios";
			$data['valorOperacao']="Atualizar/".$id."/2";
				
			$retornoAtualiza=$this->obterPorID($id);
				
			$data['textoDescricao']=$retornoAtualiza->descricao;
				
			$data['ckeditor_texto1'] = array
			(
					//id da textarea a ser substitu�da pelo CKEditor
					'id'   => 'texto1',
			
					// caminho da pasta do CKEditor relativo a pasta raiz do CodeIgniter
					'path' => 'assets/js/ckeditor',
			
					// configura��es opcionais
					'config' => array
					(
							'toolbar' => "Basic",
							'width'   => "400px",
							'height'  => "100px",
					)
			);
				
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaPostagem."' >Gerenciamento ".$areaPostagem." </a> -> <a href='".base_url()."comentarios/'>Gerenciamento Coment&aacuterio</a> -> Atualizar Coment&aacute;rio");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			$data['msgCadastro']='';
			
			if($acao!=0)
			{
				
				$erro=false;

				//Mantendo estado dos campos ap�s envio
				$data['textoDescricao']=$this->input->post('texto1');
					
						
				// Validando informa��es
				if(!verificaString($data['textoDescricao']))
				{
					$erro=true;
					$data['erroTexto']='O campo texto &eacute obrigat&oacuterio';
				}else{$data['erroTexto']='';}
				
				if($erro==false)
				{
				
				
				$arrayValores=array(
							'postagem_id'=>$this->session->userdata('idpostagem'),
							'descricao'=>$this->input->post('texto1'),
				);
				
				$this->Gravar($arrayValores,2,$id);
				$data['msgCadastro']='Sucesso';
				}
			}
			$this->template->load('templates/adminCadastro', 'admin/comentarios/cadastrar',$data);
				
		}

		public function ConfirmarExclusao($id=null)
		{
			$data['tituloCadastro']="Confirmar Exclus&atildeo";
			$data['valorOperacao']="comentarios/Excluir/".$id."/1";
			
			$retornoAtualiza=$this->obterPorID($id);
			
			$data['valorCampoTitulo']=$retornoAtualiza->descricao;
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."comentarios' class='breadcumb' title='Gerenciamento Dicion&aacuterio'>Gerenciamento Dicion&aacuterio</a> -> Confirmar Exclus&atildeo");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			$this->template->load('templates/adminCadastro', 'admin/comentarios/confirmarexclusao',$data);				
		}

		public function Excluir($id=null)
		{
			$this->comentarios_model->deletar($id);
			redirect('comentarios/', 'refresh');
		}
		
		public function Gravar($valores=array(),$acao,$id=null)
		{
			if($acao==1)
			{
				$this->comentarios_model->inserir($valores);
			}
			else
			{
				$this->comentarios_model->atualizar($id,$valores);
			}
		}

		public function carregaBibliotecasCadastrar()
		{
			$this->load->library('unit_test');
			$this->load->helper('ckeditor');
			$this->load->helper('TrataDados');
		}
		
		public function obterPorID($id)
		{
			return $this->comentarios_model->obterPorID($id);			
		}		
	}

?>