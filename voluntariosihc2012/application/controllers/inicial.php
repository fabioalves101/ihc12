<?php
// system/application/controllers/hello.php

class Inicial extends CI_Controller {

	public $idusuario="";
	public $menuPorPerfil="";
	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		$idusuario=$this->session->userdata('idusuario');
		if($idusuario=="")
		{
			redirect('login/index/', 'refresh');
		}
		$this->load->helper('MontaMenu');
		$this->load->model('perfil_model');
		$this->menuPorPerfil=$this->perfil_model->listarModulosPorPerfil($this->session->userdata('idperfil'));
			var_dump($this->perfil_model->listarModulosPorPerfil($this->session->userdata('idperfil')));
	}
	
	function Index() 
	{
		$this->template->set('msgAcao','');
		$this->template->set('Breadcrumb',"P&aacutegina Inicial");
		$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
		$data['msgInicial']="Bem Vindo";
		$this->template->load('templates/inicial', 'admin/inicial',$data);
		
	}
}
?>