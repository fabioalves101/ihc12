<?php
	class Admin extends CI_Controller
	{				
	public $idusuario="";
	public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$idusuario=$this->session->userdata('idusuario');
			if($idusuario=="")
			{
				redirect('login/index/', 'refresh');
			}
			$this->load->helper('MontaMenu');
		}
		
		public function index()
		{

			if ( ! file_exists('application/views/admin/dicionario/home.php'))
			{
				show_404();
			}
		
			$this->template->load('templates/admin', 'admin/dicionario/home');			
		}
	}