<?php
	class Login extends CI_Controller
	{				
		public function __construct()
		{
			parent::__construct();			
			$this->load->model('usuarios_model');
		}
		
		public function index()
		{
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/login/home.php'))
			{
				show_404();
			}
				
			//Atribuindo valores que ser�o repassados a p�gina
			$data['msgLogin']="";
			$data['valorOperacao']="login/autenticar/";
			$data['valorRecuperarSenha']="login/recuperarSenha/";
			
			//Carregando p�gina
			$this->template->load('templates/adminLogin', 'admin/login/home',$data);			
		}

		public function autenticar()
		{
			$this->load->helper('tratadados_helper');
			$erro=false;
			$data['msgLogin']="";
			if(!verificar_email($this->input->post('login')))
			{
				$erro=true;
				$data['msgLogin'].="O campo login deve ser preenchido com email v&aacute;lido<br />";
			}
			
			if(!verificaString($this->input->post('senha')))
			{
				$erro=true;
				$data['msgLogin'].="O campo senha deve ser preenchido<br />";				
			}
			
				
			if($erro==false)
			{
				$retorno=$this->usuarios_model->autenticar($this->input->post('login'),md5($this->input->post('senha')));
				if($retorno!=null)
				{
					if($retorno->id!="")
					{
						$this->session->set_userdata('idusuario',$retorno->id);
						$this->session->set_userdata('idperfil',$retorno->perfil);
						if($retorno->perfil=="1")
						{
							$this->session->set_userdata('usuariofiltra',0);
						}
						else
						{
							$this->session->set_userdata('usuariofiltra',$retorno->id);
						}
						$this->session->set_userdata('nomeUsuario',$retorno->nome);
						redirect('inicial/index/', 'refresh');
					}
					else
					{
						//Atribuindo valores que ser�o repassados a p�gina
						$data['msgLogin']="O nome de usu&aacute;rio ou a senha inserido est&aacute; incorreto";
						$data['valorOperacao']="login/autenticar/";
						$data['valorRecuperarSenha']="recuperar/";
							
						//Carregando p�gina
						$this->template->load('templates/adminLogin', 'admin/login/home',$data);						
					}				
				}
				else					
				{
					//Atribuindo valores que ser�o repassados a p�gina
					$data['msgLogin']="O nome de usu&aacute;rio ou a senha inserido est&aacute; incorreto";
					$data['valorOperacao']="login/autenticar/";
					$data['valorRecuperarSenha']="recuperar/";
					
					//Carregando p�gina
					$this->template->load('templates/adminLogin', 'admin/login/home',$data);
						
				}
			}
			else
			{
				//Atribuindo valores que ser�o repassados a p�gina
				$data['valorOperacao']="login/autenticar/";
				$data['valorRecuperarSenha']="login/recuperarSenha/";
				
				//Carregando p�gina
				$this->template->load('templates/adminLogin', 'admin/login/home',$data);
			}		
		}

		public function recuperarSenha($acao=0,$token=0)
		{
			$this->load->helper('tratadados_helper');
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/login/recuperarSenha.php'))
			{
				show_404();
			}
			$this->carregaBibliotecasCadastrar();
			
			//Atribuindo valores que ser�o repassados a p�gina
			$data['msgLogin']="";
			$data['valorOperacao']="login/recuperarSenha/2";

			if($acao==2)
			{
				$retorno=$this->usuarios_model->obterPorLogin($this->input->post('login'));
				if($retorno!=null)
				{
					if($retorno->id!="")
					{
						$novaSenha=gerar_senha(10, true,true,true,true);
						
						$arrayNovaSenha=array('recuperarsenha'=>codificaSenha($novaSenha));
						
						$msg="<h1>Solicita��o de senha</h1><br /><br />";
						$msg+="<p>Foi solicitado uma nova senha atrav�s do sistema de gerenciamento de volunt�rios<p>";
						$msg+="<p>A nova senha �: ".$novaSenha." </p>";
						$msg+="<p>Para valid�-la acesse o seguinte endere�o:<a href='".$_SERVER['HTTP_HOST']."/login/recuperarSenha/3/".$retorno->tokenativacao."' target='_blank'>".$_SERVER['HTTP_HOST']."/login/recuperarSenha/3/".$retorno->tokenativacao."</a></p>";
						
						$this->load->library('email');
						$config['protocol'] = 'sendmail';
						$config['mailpath'] = '/usr/sbin/sendmail';
						$config['charset'] = 'iso-8859-1';
						$config['wordwrap'] = TRUE;
						
						$this->email->initialize($config);
						$this->email->from('vitor_fidere@hotmail.com',$retorno->nome);
						$this->email->to('odenor@gmail.com');
						$this->email->subject('Solicita��o de senha para o sistema de gerenciamento de volunt�rios - IHC12');
						$this->email->message($msg);
						$this->usuarios_model->atualizar($retorno->id,$arrayNovaSenha);
						if($this->email->send())
						{
							$data['msgLogin']="Sua senha foi enviada por email";
						}
						
					}
					else
					{
						//Atribuindo valores que ser�o repassados a p�gina
						$data['msgLogin']="Usu�rio n�o cadastrado";
					}
				}
				else
				{
					//Atribuindo valores que ser�o repassados a p�gina
					$data['msgLogin']="Usu�rio n�o cadastrado";
				}
			}
			//Carregando p�gina
			$this->template->load('templates/adminLogin', 'admin/login/recuperarSenha',$data);				
		}	
		
		
		public function ativarRecuperacao($token=0)
		{}

		public function carregaBibliotecasCadastrar()
		{
			$this->load->library('unit_test');
			$this->load->helper('ckeditor');
			$this->load->helper('TrataDados');
		}
		
		public function logout()
		{
			$this->session->sess_destroy();
			redirect('login/index/', 'refresh');
			
		}
	}

?>