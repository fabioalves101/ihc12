<?php
	class Usuarios extends CI_Controller
	{	
		public $idusuario="";
		public $caminho="./assets/imagensusuarios/";
		public $menuPorPerfil="";
		public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$idusuario=$this->session->userdata('idusuario');
			if($idusuario=="")
			{
				redirect('login/index/', 'refresh');
			}
				
			$this->load->model('usuarios_model');
			$this->load->model('perfil_model');
			$this->load->helper('MontaMenu');
			$this->load->library('image_lib');
			$this->load->model('perfil_model');
			$this->menuPorPerfil=$this->perfil_model->listarModulosPorPerfil($this->session->userdata('idperfil'));
			$this->acessoPorPerfil=$this->perfil_model->acessarModulosPorPerfil($this->session->userdata('idperfil'));
			
			if(in_array("Usu&aacute;rios",$this->acessoPorPerfil)==false)
			{
				redirect('login/index/', 'refresh');
			}
				
		}
		
		public function index($pagina=null,$limite=null,$acao=null,$valorStatus=null,$id=null)
		{
			//Carregando Bibliotecas
			$paginator=$this->load->library('pagination');
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/usuarios/home.php'))
			{
				show_404();
			}
			if($limite==null)
			{
			$limite=10;
			}
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;
				
			 if($acao==2)
			{
				if($this->usuarios_model->definirStatus($id,$valorStatus))
				{
					redirect('usuarios/index/'.$pagina."/".$limite, 'refresh');
				}							
			}
				
			//Gerando Paginacao
			$config['base_url'] = base_url()."usuarios/index/";
			$config['total_rows'] = $this->usuarios_model->contarRegistros($this->session->userdata('usuariofiltra'));
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->usuarios_model->listarTodos($limite,$indice,$this->session->userdata('usuariofiltra'));
			$data['caminhoDeletar']="usuarios/confirmarexclusao/";
			$data['caminhoEditar']="usuarios/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoComentario']="usuarios/index/".$pagina."/".$limite."/";
			$data['caminhoDefinirStatus']="usuarios/index/".$pagina."/".$limite."/";
				
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> Gerenciamento usuarios");
			$this->template->set('caminhoCadastrar','usuarios/inserir/');
			$this->template->set('tituloFuncionalidade','Usu&aacuterios');
			$this->template->set('caminhoFormularioBusca','usuarios/buscar/');
			$this->template->set('caminholistarTudo','usuarios/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','titulo');			
			$this->template->set('listaPesquisa',array('nome'=>'Nome','Login'=>'login'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/usuarios/home',$data);			
		}

		public function buscar($pagina=null,$limite=null,$acao=null,$valorStatus=null,$id=null)
		{
			//Carregando Bibliotecas
			$this->load->library('pagination');
			
			$campo=$this->input->post('campo');
			
			if($campo!="")
			{
				
				$this->session->set_userdata('campo',$this->input->post('campo'));
				$this->session->set_userdata('palavra',$this->input->post('palavra'));
			}
			//Validando vari�veis
			$limite=10;
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."usuarios/buscar/";
			$config['total_rows'] = $this->usuarios_model->contarBusca($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice,$this->session->userdata('usuariofiltra'));
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();
			
			if($acao==2)
			{
				if($this->usuarios_model->definirStatus($id,$valorStatus))
				{
					redirect('usuarios/index/'.$pagina."/".$limite, 'refresh');
				}
			}
				

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->usuarios_model->buscarPaginado($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice,$this->session->userdata('usuariofiltra'));
			$data['caminhoDeletar']="usuarios/confirmarexclusao/";
			$data['caminhoEditar']="usuarios/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoComentario']="usuarios/index/".$pagina."/".$limite."/";
			$data['caminhoDefinirStatus']="usuarios/index/".$pagina."/".$limite."/";
				
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> Gerenciamento usuarios");
			$this->template->set('caminhoCadastrar','usuarios/inserir/');
			$this->template->set('caminholistarTudo','usuarios/');
			$this->template->set('tituloFuncionalidade','Usu&aacuterios');
			$this->template->set('caminhoFormularioBusca','usuarios/buscar/');
			$this->template->set('valuePalavraPesquisa',$this->session->userdata('palavra'));
			$this->template->set('itemMarcadoListaPesquisa',$this->session->userdata('campo'));
			$this->template->set('listaPesquisa',array('nome'=>'Nome','Login'=>'login'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));

			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/usuarios/home',$data);
		}
		
		public function Inserir($acao=null)
		{
			$this->carregaBibliotecasCadastrar();
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/usuarios/cadastrar.php'))
			{
				show_404();
			}
			
			
			//Atribuindo valores que ser�o repassados a p�gina
			$data['tituloCadastro']="Inserir Usu&aacuterios";
			$data['valorOperacao']="Inserir/1/";
			$data['valorCampoNome']="";
			$data['valorCampoCurso']="";
			$data['valorCampoInstituicao']="";
			$data['valorCampoDataNascimento']="";
			$data['valorCampoTelefonePessoal']="";
			$data['valorCampoTelefoneContato']="";
			$data['valorCampoNumeroSocio']="";
			$data['valorCampoPagina']="";			
			
			$data['valorCampoEmail']="";
			$data['valorCampoPerfil']="";
			
			$data['listaPerfil']=$this->perfil_model->listar();			
			$data['itemMarcadoListaPerfil']='';
			
			$data['listaSemestre']=array(''=>'Selecione o semestre','1&ordm;'=>'1&ordm;','2&ordm;'=>'2&ordm;','3&ordm;'=>'3&ordm;','4&ordm;'=>'4&ordm;','5&ordm;'=>'5&ordm;','6&ordm;'=>'6&ordm;','7&ordm;'=>'7&ordm;','8&ordm;'=>'8&ordm;','9&ordm;'=>'9&ordm;','10&ordm;'=>'10&ordm;');
			$data['itemMarcadoListaSemestre']='';

			$data['listaSexo']=array(''=>'Selecione o sexo','M'=>'Masculino','F'=>'Feminino');
			$data['itemMarcadoListaSexo']='';

			$data['listaSocio']=array(''=>'&Eacute; S&oacute;cio?','1'=>'Sim','0'=>'N&atilde;o');
			$data['itemMarcadoListaSocio']='';
				
			
			$data['msgCadastro']='';
			$data['mostraPerfil']=true;

			//Atribuindo vari�veis de template
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."usuarios' class='breadcumb' title='Gerenciamento usuarios'>Gerenciamento usuarios</a> -> Inserir usuarios ");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			if(isset($acao))
			{
				$erro=false;
				
				//Mantendo estado dos campos
				$data['valorCampoNome']=$this->input->post('nome');
				$data['valorCampoCurso']=$this->input->post('curso');
				$data['valorCampoInstituicao']=$this->input->post('instituicao');
				$data['valorCampoDataNascimento']=$this->input->post('datanascimento');
				$data['valorCampoTelefonePessoal']=$this->input->post('telefonepessoal');
				$data['valorCampoTelefoneContato']=$this->input->post('telefonecontato');
				$data['valorCampoNumeroSocio']=$this->input->post('numerosocio');
				$data['valorCampoPagina']=$this->input->post('pagina');
				
				$data['valorCampoEmail']=$this->input->post('email');
				$data['mostraPerfil']=true;
				$data['listaPerfil']=$this->perfil_model->listar();
				$data['itemMarcadoListaPerfil']=$this->input->post('perfil');
				
				$data['listaSemestre']=array(''=>'Selecione o semestre','1&ordm;'=>'1&ordm;','2&ordm;'=>'2&ordm;','3&ordm;'=>'3&ordm;','4&ordm;'=>'4&ordm;','5&ordm;'=>'5&ordm;','6&ordm;'=>'6&ordm;','7&ordm;'=>'7&ordm;','8&ordm;'=>'8&ordm;','9&ordm;'=>'9&ordm;','10&ordm;'=>'10&ordm;');
				$data['itemMarcadoListaSemestre']='';
				
				$data['listaSexo']=array(''=>'Selecione o sexo','M'=>'Masculino','F'=>'Feminino');
				$data['itemMarcadoListaSexo']='';
				
				$data['listaSocio']=array(''=>'&Eacute; S&oacute;cio?','1'=>'Sim','0'=>'N&atilde;o');
				$data['itemMarcadoListaSocio']='';
				
				
				// Validando informa��es
				if(!verificaNumerico($this->input->post('perfil')))
				{
					$erro=true;
					$data['erroPerfil']='O campo perfil &eacute obrigat&oacuterio';
				}else{$data['erroPerfil']='';}
				
				if(!verificaString($this->input->post('nome')))
				{
					$erro=true;
					$data['erroNome']='O campo nome &eacute obrigat&oacuterio';
				}else{$data['erroNome']='';}

				if(!verificaString($this->input->post('curso')))
				{
					$erro=true;
					$data['erroCurso']='O campo curso &eacute obrigat&oacuterio';
				}else{$data['erroCurso']='';}

				if(!verificaString($this->input->post('semestre')))
				{
					$erro=true;
					$data['erroSemestre']='O campo semestre &eacute obrigat&oacuterio';
				}else{$data['erroSemestre']='';}

				
				if(!verificaString($this->input->post('instituicao')))
				{
					$erro=true;
					$data['erroInstituicao']='O campo institui&ccedil;&atilde;o &eacute obrigat&oacuterio';
				}else{$data['erroInstituicao']='';}
				
				if(!verificaData($this->input->post('datanascimento')))
				{
					$erro=true;
					$data['erroDataNascimento']='O campo Data Nascimento &eacute obrigat&oacuterio';
				}else{$data['erroDataNascimento']='';}				

				if(!verificaString($this->input->post('sexo')) || ($this->input->post('sexo')==""))
				{
					$erro=true;
					$data['erroSexo']='O campo sexo &eacute obrigat&oacuterio';
				}else{$data['erroSexo']='';}				

				if(!verificaString($this->input->post('telefonepessoal')))
				{
					$erro=true;
					$data['erroTelefonePessoal']='O campo Telefone Pessoal &eacute obrigat&oacuterio';
				}else{$data['erroTelefonePessoal']='';}

				if(!verificaString($this->input->post('sociosbc')) || ($this->input->post('sociosbc')==""))
				{
					$erro=true;
					$data['erroSocioSBC']='O campo S&oacute;cio SBC &eacute obrigat&oacuterio';
				}else
					{
						$data['erroSocioSBC']='';
						$data['erroNumeroSocio']='';
						$valorsocio=$this->input->post('socioSBC');
						if($valorsocio=="1")
						{
							if(!verificaString($this->input->post('numerosocio')))
							{
								$erro=true;
								$data['erroNumeroSocio']='O campo N&uacute;mero S&oacute;cio &eacute obrigat&oacuterio';
							}else{$data['erroNumeroSocio']='';}							
						}
					}
								
				if(!verificar_email($this->input->post('email')))
				{
					$erro=true;
					$data['erroEmail']='Informe um e-mail v&aacute;lido';
				}else{$data['erroEmail']='';}				
				
				if(!verificaString($this->input->post('senha')))
				{
					$erro=true;
					$data['erroSenha']='O campo senha &eacute obrigat&oacuterio';
				}else{$data['erroSenha']='';}
				
								
				if($erro==false)
				{
					$arrayValores=array(
							'nome'=>$this->input->post('nome'),
							'login'=>$this->input->post('email'),
							'curso'=>$this->input->post('curso'),
							'semestre'=>$this->input->post('semestre'),
							'instituicao'=>$this->input->post('instituicao'),
							'datanascimento'=>$this->input->post('datanascimento'),
							'sexo'=>$this->input->post('sexo'),
							'telefonepessoal'=>$this->input->post('telefonepessoal'),
							'telefonecontato'=>$this->input->post('telefonecontato'),
							'sociosbc'=>$this->input->post('sociosbc'),
							'numerosocio'=>$this->input->post('numerosocio'),
							'pagina'=>$this->input->post('pagina'),
							'senha'=>md5($this->input->post('senha')),
							'perfil'=>$this->input->post('perfil'),
							'datacadastro'=>date('Y-m-d H:i:s'),
							'tokenativacao'=>md5(date('Y-m-d H:i:s')),
							'ativado'=>'0',);
					$this->Gravar($arrayValores,$acao);
					$data['msgCadastro']='Sucesso';
				}
			}			
			
			$this->template->load('templates/adminCadastro', 'admin/usuarios/cadastrar',$data);
		}
		
			
		public function Atualizar($id,$acao=0)
		{
			$this->carregaBibliotecasCadastrar();
			$retornoAtualiza=$this->obterPorID($id);
			
			if ( ! file_exists('application/views/admin/usuarios/cadastrar.php'))
			{
				show_404();
			}		

				
			
			$data['tituloCadastro']="Atualizar Usu&aacuterios ".$retornoAtualiza->nome;
			$data['valorOperacao']="Atualizar/".$id."/2";
			$data['valorCampoNome']=$retornoAtualiza->nome;
			$data['valorCampoEmail']=$retornoAtualiza->login;
			$data['valorCampoCurso']=$retornoAtualiza->curso;
			$data['valorCampoInstituicao']=$retornoAtualiza->instituicao;
			$data['valorCampoDataNascimento']=retornaData($retornoAtualiza->datanascimento);
			$data['valorCampoTelefonePessoal']=$retornoAtualiza->telefonepessoal;
			$data['valorCampoTelefoneContato']=$retornoAtualiza->telefonecontato;
			$data['valorCampoNumeroSocio']=$retornoAtualiza->numerosocio;
			$data['valorCampoPagina']=$retornoAtualiza->pagina;
				
			$data['mostraPerfil']=true;
			$data['listaPerfil']=$this->perfil_model->listar();
			$data['itemMarcadoListaPerfil']=$retornoAtualiza->perfil;

			$data['listaSemestre']=array(''=>'Selecione o semestre','1&ordm;'=>'1&ordm;','2&ordm;'=>'2&ordm;','3&ordm;'=>'3&ordm;','4&ordm;'=>'4&ordm;','5&ordm;'=>'5&ordm;','6&ordm;'=>'6&ordm;','7&ordm;'=>'7&ordm;','8&ordm;'=>'8&ordm;','9&ordm;'=>'9&ordm;','10&ordm;'=>'10&ordm;');
			$data['itemMarcadoListaSemestre']=$retornoAtualiza->semestre;
			
			$data['listaSexo']=array(''=>'Selecione o sexo','M'=>'Masculino','F'=>'Feminino');
			$data['itemMarcadoListaSexo']=$retornoAtualiza->sexo;
			
			$data['listaSocio']=array(''=>'&Eacute; S&oacute;cio?','1'=>'Sim','0'=>'N&atilde;o');
			$data['itemMarcadoListaSocio']=$retornoAtualiza->sociosbc;
				
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."usuarios' class='breadcumb' title='Gerenciamento usuarios'>Gerenciamento usuarios</a> -> Atualizar Usu&aacute;rio");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			$data['msgCadastro']='';
		
			if($acao!=0)
			{
							$erro=false;
				
				//Mantendo estado dos campos
				$data['valorCampoNome']=$this->input->post('nome');
				$data['valorCampoEmail']=$this->input->post('email');
				$data['mostraPerfil']=true;
				$data['listaPerfil']=$this->perfil_model->listar();
				$data['itemMarcadoListaPerfil']=$this->input->post('perfil');

				$data['listaSemestre']=array(''=>'Selecione o semestre','1&ordm;'=>'1&ordm;','2&ordm;'=>'2&ordm;','3&ordm;'=>'3&ordm;','4&ordm;'=>'4&ordm;','5&ordm;'=>'5&ordm;','6&ordm;'=>'6&ordm;','7&ordm;'=>'7&ordm;','8&ordm;'=>'8&ordm;','9&ordm;'=>'9&ordm;','10&ordm;'=>'10&ordm;');
				$data['itemMarcadoListaSemestre']=$this->input->post('semestre');
					
				$data['listaSexo']=array(''=>'Selecione o sexo','M'=>'Masculino','F'=>'Feminino');
				$data['itemMarcadoListaSexo']=$this->input->post('sexo');
					
				$data['listaSocio']=array(''=>'&Eacute; S&oacute;cio?','1'=>'Sim','0'=>'N&atilde;o');
				$data['itemMarcadoListaSocio']=$this->input->post('sociosbc');
				
				
				// Validando informa��es
				if(!verificaNumerico($this->input->post('perfil')))
				{
					$erro=true;
					$data['erroPerfil']='O campo perfil &eacute obrigat&oacuterio';
				}else{$data['erroPerfil']='';}
				
				if(!verificaString($this->input->post('nome')))
				{
					$erro=true;
					$data['erroNome']='O campo nome &eacute obrigat&oacuterio';
				}else{$data['erroNome']='';}

				if(!verificaString($this->input->post('curso')))
				{
					$erro=true;
					$data['erroCurso']='O campo curso &eacute obrigat&oacuterio';
				}else{$data['erroCurso']='';}

				if(!verificaString($this->input->post('semestre')))
				{
					$erro=true;
					$data['erroSemestre']='O campo semestre &eacute obrigat&oacuterio';
				}else{$data['erroSemestre']='';}

				
				if(!verificaString($this->input->post('instituicao')))
				{
					$erro=true;
					$data['erroInstituicao']='O campo institui&ccedil;&atilde;o &eacute obrigat&oacuterio';
				}else{$data['erroInstituicao']='';}
				
				if(!verificaData($this->input->post('datanascimento')))
				{
					$erro=true;
					$data['erroDataNascimento']='O campo Data Nascimento &eacute obrigat&oacuterio';
				}else{$data['erroDataNascimento']='';}				

				if(!verificaString($this->input->post('sexo')) || ($this->input->post('sexo')==""))
				{
					$erro=true;
					$data['erroSexo']='O campo sexo &eacute obrigat&oacuterio';
				}else{$data['erroSexo']='';}				

				if(!verificaString($this->input->post('telefonepessoal')))
				{
					$erro=true;
					$data['erroTelefonePessoal']='O campo Telefone Pessoal &eacute obrigat&oacuterio';
				}else{$data['erroTelefonePessoal']='';}

				if(!verificaString($this->input->post('sociosbc')) || ($this->input->post('sociosbc')==""))
				{
					$erro=true;
					$data['erroSocioSBC']='O campo S&oacute;cio SBC &eacute obrigat&oacuterio';
				}else
					{
						$data['erroSocioSBC']='';
						$data['erroNumeroSocio']='';
						$valorsocio=$this->input->post('socioSBC');
						if($valorsocio=="1")
						{
							if(!verificaString($this->input->post('numerosocio')))
							{
								$erro=true;
								$data['erroNumeroSocio']='O campo N&uacute;mero S&oacute;cio &eacute obrigat&oacuterio';
							}else{$data['erroNumeroSocio']='';}							
						}
					}
								
				if(!verificar_email($this->input->post('email')))
				{
					$erro=true;
					$data['erroEmail']='Informe um e-mail v&aacute;lido';
				}else{$data['erroEmail']='';}				
				
											
				
				
				if($erro==false)
				{	
					if(verificaString($this->input->post('senha')))
					{
						$arrayValores=array(
								'nome'=>$this->input->post('nome'),
								'curso'=>$this->input->post('curso'),
								'semestre'=>$this->input->post('semestre'),
								'instituicao'=>$this->input->post('instituicao'),
								'datanascimento'=>$this->input->post('datanascimento'),
								'sexo'=>$this->input->post('sexo'),
								'telefonepessoal'=>$this->input->post('telefonepessoal'),
								'telefonecontato'=>$this->input->post('telefonecontato'),
								'sociosbc'=>$this->input->post('sociosbc'),
								'numerosocio'=>$this->input->post('numerosocio'),
								'pagina'=>$this->input->post('pagina'),
								'login'=>$this->input->post('email'),
								'senha'=>md5($this->input->post('senha')),
								'perfil'=>$this->input->post('perfil'));
					}
					else 
					{
						$arrayValores=array(
								'nome'=>$this->input->post('nome'),
								'login'=>$this->input->post('email'),
								'perfil'=>$this->input->post('perfil'));						
					}	
					$this->Gravar($arrayValores,$acao,$id);
					$data['msgCadastro']='Sucesso';
				}
			}
			$this->template->load('templates/adminCadastro', 'admin/usuarios/cadastrar',$data);
		}


		public function Interesses($acao=0)
		{
			$this->carregaBibliotecasCadastrar();
		
			if ( ! file_exists('application/views/admin/usuarios/interesses.php'))
			{
				show_404();
			}
		
			$data['tituloCadastro']="Meus interesses";
			$data['valorOperacao']="interesses/2";
			
			$this->load->model('categoriaatividades_model');
			$data['listaCategorias']=$this->categoriaatividades_model->listarChecked($this->session->userdata('idusuario'));
		
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."usuarios' class='breadcumb' title='Gerenciamento usuarios'>Gerenciamento usuarios</a> -> Interesses");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
		
			$data['msgCadastro']='';
		
			if($acao!=0)
			{
				$erro=false;
		
				//Mantendo estado dos campos
				$arrayValores=$this->input->post('interesses');
				$this->usuarios_model->associainteresse($arrayValores,$this->session->userdata('idusuario'));
				$data['msgCadastro']='Sucesso';
					
				$data['tituloCadastro']="Meus interesses";
				$data['valorOperacao']="interesses/2";
				$data['listaCategorias']=$this->categoriaatividades_model->listarChecked($this->session->userdata('idusuario'));
						
			}
			$this->template->load('templates/adminCadastro', 'admin/usuarios/interesses',$data);
		}
		
		
		public function MeuPerfil($acao=0)
		{
			$this->carregaBibliotecasCadastrar();
			$retornoAtualiza=$this->obterPorID($this->session->userdata('idusuario'));
		
			if ( ! file_exists('application/views/admin/usuarios/cadastrar.php'))
			{
				show_404();
			}
		
		
		
			$data['tituloCadastro']="Atualizar Perfil ".$retornoAtualiza->nome;
			$data['valorOperacao']="meuperfil/2";
			$data['valorCampoNome']=$retornoAtualiza->nome;
			$data['valorCampoEmail']=$retornoAtualiza->login;
			$data['valorCampoCurso']=$retornoAtualiza->curso;
			$data['valorCampoInstituicao']=$retornoAtualiza->instituicao;
			$data['valorCampoDataNascimento']=retornaData($retornoAtualiza->datanascimento);
			$data['valorCampoTelefonePessoal']=$retornoAtualiza->telefonepessoal;
			$data['valorCampoTelefoneContato']=$retornoAtualiza->telefonecontato;
			$data['valorCampoNumeroSocio']=$retornoAtualiza->numerosocio;
			$data['valorCampoPagina']=$retornoAtualiza->pagina;
				
			$data['mostraPerfil']=false;
			$data['listaPerfil']=$this->perfil_model->listar();
			$data['itemMarcadoListaPerfil']=$retornoAtualiza->perfil;

			$data['listaSemestre']=array(''=>'Selecione o semestre','1&ordm;'=>'1&ordm;','2&ordm;'=>'2&ordm;','3&ordm;'=>'3&ordm;','4&ordm;'=>'4&ordm;','5&ordm;'=>'5&ordm;','6&ordm;'=>'6&ordm;','7&ordm;'=>'7&ordm;','8&ordm;'=>'8&ordm;','9&ordm;'=>'9&ordm;','10&ordm;'=>'10&ordm;');
			$data['itemMarcadoListaSemestre']=$retornoAtualiza->semestre;
				
			$data['listaSexo']=array(''=>'Selecione o sexo','M'=>'Masculino','F'=>'Feminino');
			$data['itemMarcadoListaSexo']=$retornoAtualiza->sexo;
				
			$data['listaSocio']=array(''=>'&Eacute; S&oacute;cio?','1'=>'Sim','0'=>'N&atilde;o');
			$data['itemMarcadoListaSocio']=$retornoAtualiza->sociosbc;
				
			
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."usuarios' class='breadcumb' title='Gerenciamento usuarios'>Gerenciamento usuarios</a> -> Atualizar Meu perfil");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
		
			$data['msgCadastro']='';
		
			if($acao!=0)
			{
				$erro=false;
		
				//Mantendo estado dos campos
				$data['valorCampoNome']=$this->input->post('nome');
				$data['valorCampoCurso']=$this->input->post('curso');
				$data['valorCampoInstituicao']=$this->input->post('instituicao');
				$data['valorCampoDataNascimento']=retornaData($this->input->post('datanascimento'));
				$data['valorCampoTelefonePessoal']=$this->input->post('telefonepessoal');
				$data['valorCampoTelefoneContato']=$this->input->post('telefonecontato');
				$data['valorCampoNumeroSocio']=$this->input->post('numerosocio');
				$data['valorCampoPagina']=$this->input->post('pagina');
				
				$data['valorCampoEmail']=$this->input->post('email');
				
				$data['mostraPerfil']=false;
				$data['listaPerfil']=$this->perfil_model->listar();
				$data['itemMarcadoListaPerfil']=$this->input->post('perfil');

				$data['listaSemestre']=array(''=>'Selecione o semestre','1&ordm;'=>'1&ordm;','2&ordm;'=>'2&ordm;','3&ordm;'=>'3&ordm;','4&ordm;'=>'4&ordm;','5&ordm;'=>'5&ordm;','6&ordm;'=>'6&ordm;','7&ordm;'=>'7&ordm;','8&ordm;'=>'8&ordm;','9&ordm;'=>'9&ordm;','10&ordm;'=>'10&ordm;');
				$data['itemMarcadoListaSemestre']=$retornoAtualiza->semestre;
					
				$data['listaSexo']=array(''=>'Selecione o sexo','M'=>'Masculino','F'=>'Feminino');
				$data['itemMarcadoListaSexo']=$retornoAtualiza->sexo;
					
				$data['listaSocio']=array(''=>'&Eacute; S&oacute;cio?','1'=>'Sim','0'=>'N&atilde;o');
				$data['itemMarcadoListaSocio']=$retornoAtualiza->sociosbc;
				
				
				// Validando informa��es
				/*if(!verificaNumerico($this->input->post('perfil')))
				 {
				$erro=true;
				$data['erroPerfil']='O campo perfil &eacute obrigat&oacuterio';
				}else{$data['erroPerfil']='';}*/
		
				
				if(!verificaString($this->input->post('nome')))
				{
					$erro=true;
					$data['erroNome']='O campo nome &eacute obrigat&oacuterio';
				}else{$data['erroNome']='';}

				if(!verificaString($this->input->post('curso')))
				{
					$erro=true;
					$data['erroCurso']='O campo curso &eacute obrigat&oacuterio';
				}else{$data['erroCurso']='';}

				if(!verificaString($this->input->post('semestre')))
				{
					$erro=true;
					$data['erroSemestre']='O campo semestre &eacute obrigat&oacuterio';
				}else{$data['erroSemestre']='';}

				
				if(!verificaString($this->input->post('instituicao')))
				{
					$erro=true;
					$data['erroInstituicao']='O campo institui&ccedil;&atilde;o &eacute obrigat&oacuterio';
				}else{$data['erroInstituicao']='';}
				
				if(!verificaData($this->input->post('datanascimento')))
				{
					$erro=true;
					$data['erroDataNascimento']='O campo Data Nascimento &eacute obrigat&oacuterio';
				}else{$data['erroDataNascimento']='';}				

				if(!verificaString($this->input->post('sexo')) || ($this->input->post('sexo')==""))
				{
					$erro=true;
					$data['erroSexo']='O campo sexo &eacute obrigat&oacuterio';
				}else{$data['erroSexo']='';}				

				if(!verificaString($this->input->post('telefonepessoal')))
				{
					$erro=true;
					$data['erroTelefonePessoal']='O campo Telefone Pessoal &eacute obrigat&oacuterio';
				}else{$data['erroTelefonePessoal']='';}

				if(!verificaString($this->input->post('sociosbc')) || ($this->input->post('sociosbc')==""))
				{
					$erro=true;
					$data['erroSocioSBC']='O campo S&oacute;cio SBC &eacute obrigat&oacuterio';
				}else
					{
						$data['erroSocioSBC']='';
						$data['erroNumeroSocio']='';
						$valorsocio=$this->input->post('socioSBC');
						if($valorsocio=="1")
						{
							if(!verificaString($this->input->post('numerosocio')))
							{
								$erro=true;
								$data['erroNumeroSocio']='O campo N&uacute;mero S&oacute;cio &eacute obrigat&oacuterio';
							}else{$data['erroNumeroSocio']='';}							
						}
					}
								
				if(!verificar_email($this->input->post('email')))
				{
					$erro=true;
					$data['erroEmail']='Informe um e-mail v&aacute;lido';
				}else{$data['erroEmail']='';}				
						
		
		
				if($erro==false)
				{
					if(verificaString($this->input->post('senha')))
					{
						$arrayValores=array(
								'nome'=>$this->input->post('nome'),
								'login'=>$this->input->post('email'),
								'curso'=>$this->input->post('curso'),
								'semestre'=>$this->input->post('semestre'),
								'instituicao'=>$this->input->post('instituicao'),
								'datanascimento'=>$this->input->post('datanascimento'),
								'sexo'=>$this->input->post('sexo'),
								'telefonepessoal'=>$this->input->post('telefonepessoal'),
								'telefonecontato'=>$this->input->post('telefonecontato'),
								'sociosbc'=>$this->input->post('sociosbc'),
								'numerosocio'=>$this->input->post('numerosocio'),
								'pagina'=>$this->input->post('pagina'),
								'senha'=>md5($this->input->post('senha'))
								);
		
					}
					else
					{
						$arrayValores=array(
								'nome'=>$this->input->post('nome'),
								'curso'=>$this->input->post('curso'),
								'semestre'=>$this->input->post('semestre'),
								'instituicao'=>$this->input->post('instituicao'),
								'datanascimento'=>$this->input->post('datanascimento'),
								'sexo'=>$this->input->post('sexo'),
								'telefonepessoal'=>$this->input->post('telefonepessoal'),
								'telefonecontato'=>$this->input->post('telefonecontato'),
								'sociosbc'=>$this->input->post('sociosbc'),
								'numerosocio'=>$this->input->post('numerosocio'),
								'pagina'=>$this->input->post('pagina'),								
								'login'=>$this->input->post('email')
								);
		
					}
					
					$this->Gravar($arrayValores,2,$this->session->userdata('idusuario'));
					$data['msgCadastro']='Sucesso';
				}
			}
			$this->template->load('templates/adminCadastro', 'admin/usuarios/cadastrar',$data);
		}
		
		
		
		public function ConfirmarExclusao($id=null)
		{
			$data['tituloCadastro']="Confirmar Exclus&atildeo";
			$data['valorOperacao']="usuarios/Excluir/".$id."/1";
			
			$retornoAtualiza=$this->obterPorID($id);
			
			$data['valorCampoTitulo']=$retornoAtualiza->titulo;
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."usuarios' class='breadcumb' title='Gerenciamento usuarios'>Gerenciamento usuarios</a> -> Confirmar Exclus&atildeo");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			$this->template->load('templates/adminCadastro', 'admin/usuarios/confirmarexclusao',$data);				
		}

		public function Excluir($id=null)
		{
			$retorno=$this->usuarios_model->ObterPorId($id);
			if($retorno!=null)
			{
				$this->usuarios_model->deletar($id);
				redirect('usuarios/', 'refresh');
			}
		}
		
		public function Gravar($valores=array(),$acao,$id=null)
		{
			if($acao==1)
			{
				$this->usuarios_model->inserir($valores);
			}
			else
			{
				$this->usuarios_model->atualizar($id,$valores);
			}
		}
		
		public function GravarInteresses($valoresInteresses=array(),$id)
		{
			
		}

		public function carregaBibliotecasCadastrar($configUp=null)
		{
			$this->load->helper('ckeditor');
			$this->load->helper('TrataDados');				
			$this->load->library('unit_test');	
			//$this->load->library('upload', $configUp);
		}
		
		public function obterPorID($id)
		{
			return $this->usuarios_model->obterPorID($id);			
		}		
		
		public function geraImagem($imagem,$nomeImagem,$altura,$largura)
		{
			$configImagem['image_library'] = 'gd2';
			$configImagem['source_image']	= $imagem;
			$config['maintain_ratio'] = TRUE;
			$config['master_dim']="auto";
			$configImagem['width']	 = $largura;
			$configImagem['height']	= $altura;
			$configImagem['new_image']=$nomeImagem;
			$this->image_lib->initialize($configImagem);
			
			$this->image_lib->resize();
			$this->image_lib->clear();
		}
	}

?>