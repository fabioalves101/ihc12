<?php
	class Atividades extends CI_Controller
	{				
		public $idusuario="";
		public $menuPorPerfil="";
		public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$idusuario=$this->session->userdata('idusuario');
			if($idusuario=="")
			{
				redirect('login/index/', 'refresh');
			}
			$this->load->model('atividades_model');
			$this->load->model('categoriaatividades_model');		
			$this->load->helper('MontaMenu');
			$this->load->model('perfil_model');
			$this->menuPorPerfil=$this->perfil_model->listarModulosPorPerfil($this->session->userdata('idperfil'));
			$this->acessoPorPerfil=$this->perfil_model->acessarModulosPorPerfil($this->session->userdata('idperfil'));
			
			if(in_array("Atividades",$this->acessoPorPerfil)==false)
			{	
				redirect('login/index/', 'refresh');
			}
						
		}
		
		public function index($pagina=null,$limite=null,$acao=null,$valorStatus=null,$id=null)
		{
			//Carregando Bibliotecas
			$paginator=$this->load->library('pagination');
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/atividades/home.php'))
			{
				show_404();
			}
			if($limite==null)
			{
			$limite=10;
			}
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;

			if($acao==2)
			{
				if($this->atividades_model->definirStatus($id,$valorStatus))
				{
					redirect('categoriaatividades/atividades/index/'.$pagina."/".$limite, 'refresh');
				}							
			}
		
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."categoriaatividades/atividades/index/";
			$config['total_rows'] = $this->atividades_model->contarRegistros($this->session->userdata('idcategoriaatividades'));
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();
			
			//Obtendo valor da postagem
			
			$retornoCategoria=$this->categoriaatividades_model->obterPorID($this->session->userdata('idcategoriaatividades'));
			
			$areaCategoria=" da Categoria: ".$retornoCategoria->titulo;
			$linkBreadcrumb="categoriaatividades/";
			
			
			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->atividades_model->listarTodos($limite,$indice,$this->session->userdata('idcategoriaatividades'));
			$data['caminhoDeletar']="categoriaatividades/atividades/confirmarexclusao/";
			$data['caminhoEditar']="categoriaatividades/atividades/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoDefinirStatus']="categoriaatividades/atividades/".$pagina."/".$limite."/";
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaCategoria."' >Gerenciamento ".$areaCategoria." </a> -> Gerenciamento Atividades");
			$this->template->set('caminhoCadastrar','categoriaatividades/atividades/inserir/');
			$this->template->set('tituloFuncionalidade','Atividades'.$areaCategoria);
			$this->template->set('caminhoFormularioBusca','categoriaatividades/atividades/buscar/');
			$this->template->set('caminholistarTudo','categoriaatividades/atividades/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','titulo');			
			$this->template->set('listaPesquisa',array('titulo'=>'T&iacute;tulo'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/atividades/home',$data);			
		}

		public function buscar($pagina=null,$limite=null,$acao=null,$valorStatus=null,$id=null)
		{
			//Carregando Bibliotecas
			$this->load->library('pagination');
			
			$campo=$this->input->post('campo');
			
			if($campo!="")
			{				
				$this->session->set_userdata('campo',$this->input->post('campo'));
				$this->session->set_userdata('palavra',$this->input->post('palavra'));
			}
			//Validando vari�veis
			$limite=10;
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;	

			//Obtendo valor da postagem
			$retornoCategoria=$this->categoriaatividades_model->obterPorID($this->session->userdata('idcategoriaatividades'));
				
			$areaCategoria=" da Categoria: ".$retornoCategoria->titulo;
			$linkBreadcrumb="categoriaatividades/";
				
			if($acao==2)
			{
				if($this->atividades_model->definirStatus($id,$valorStatus))
				{
					redirect('atividades/index/'.$pagina."/".$limite, 'refresh');
				}
			}			
			
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."categoriaatividades/atividades/buscar/";
			$config['total_rows'] = $this->atividades_model->contarBusca($this->session->userdata('campo'),$this->session->userdata('palavra'),$this->session->userdata('idcategoriaatividades'));

			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->atividades_model->buscarPaginado($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice,$this->session->userdata('idcategoriaatividades'));
			$data['caminhoDeletar']="categoriaatividades/atividades/confirmarexclusao/";
			$data['caminhoEditar']="categoriaatividades/atividades/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoDefinirStatus']="categoriaatividades/atividades/index/".$pagina."/".$limite."/";
			
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaCategoria."' >Gerenciamento ".$areaCategoria." </a> -> Gerenciamento Atividades");
			$this->template->set('caminhoCadastrar','categoriaatividades/atividades/inserir/');
			$this->template->set('tituloFuncionalidade','Atividades'.$areaCategoria);
			$this->template->set('caminhoFormularioBusca','categoriaatividades/atividades/buscar/');
			$this->template->set('caminholistarTudo','categoriaatividades/atividades/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','titulo');			
			$this->template->set('listaPesquisa',array('titulo'=>'T&iacute;tulo'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/atividades/home',$data);
		}
		
		
		public function Inserir($acao=null)
		{
			//Carregando Bibliotecas
			$this->carregaBibliotecasCadastrar();
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/atividades/cadastrar.php'))
			{
				show_404();
			}
			
			//Obtendo valor da categoria
			$retornoCategoria=$this->categoriaatividades_model->obterPorID($this->session->userdata('idcategoriaatividades'));
				
			$areaCategoria=" da Categoria: ".$retornoCategoria->titulo;
			$linkBreadcrumb="categoriaatividades/";
			
			
			//Atribuindo valores que ser�o repassados a p�gina
			$data['tituloCadastro']="Inserir Atividade";
			$data['valorOperacao']="categoriaatividades/atividades/Inserir/1/";
			$data['valorCampoTitulo']="";
			$data['valorCampoDescricao']="";
			$data['valorCampoDataInicial']="";
			$data['valorCampoDataFinal']="";
			$data['valorCampoHoras']="";
			$data['msgCadastro']='';
			$data['itemMarcadoListaTurno']='';
			$data['listaTurno']=array(''=>'Selecione o turno','Matutino'=>'Matutino','Vespertino'=>'Vespertino','Noturno'=>'Noturno');
			$data['ckeditor_texto1'] = array
			(
					//id da textarea a ser substitu�da pelo CKEditor
					'id'   => 'texto1',
			
					// caminho da pasta do CKEditor relativo a pasta raiz do CodeIgniter
					'path' => 'assets/js/ckeditor',
			
					// configura��es opcionais
					'config' => array
					(
							'toolbar' => "Basic",
							'width'   => "400px",
							'height'  => "100px",
					)
			);			
				
			//Atribuindo vari�veis de template
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaCategoria."' >Gerenciamento ".$areaCategoria." </a> -> <a href='".base_url()."atividades/'>Gerenciamento Atividades</a> -> Inserir Atividade");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			if(isset($acao))
			{
				$erro=false;
				
				//Mantendo estado dos campos
				$data['valorCampoTitulo']=$this->input->post('titulo');
				$data['valorCampoDescricao']=$this->input->post('texto1');
				$data['valorCampoDataInicial']=$this->input->post('datainicial');
				$data['valorCampoDataFinal']=$this->input->post('datafinal');
				$data['valorCampoHoras']=$this->input->post('horas');
				$data['valorCampoVagas']=$this->input->post('vagas');
				$data['itemMarcadoListaTurno']=$this->input->post('turno');
				
				// Validando informa��es
				if(!verificaString($this->input->post('titulo')))
				{
					$erro=true;
					$data['erroTitulo']='O campo T&iacute; &eacute obrigat&oacuterio';
				}else{$data['erroTitulo']='';}

				if(!verificaString($this->input->post('datainicial')))
				{
					$erro=true;
					$data['erroDataInicial']='O campo Data Inicial &eacute obrigat&oacuterio';
				}else{$data['erroDataInicial']='';}
				
				
				if(!verificaString($this->input->post('datafinal')))
				{
					$erro=true;
					$data['erroDataFinal']='O campo Data Final &eacute obrigat&oacuterio';
				}else{$data['erroDataFinal']='';}
				
				
				if(!verificaString($this->input->post('horas')))
				{
					$erro=true;
					$data['erroHoras']='O campo Horas &eacute obrigat&oacuterio';
				}else{$data['erroHoras']='';}
				
				if(!verificaString($this->input->post('turno')))
				{
					$erro=true;
					$data['erroTurno']='O campo Turno &eacute obrigat&oacuterio';
				}else{$data['erroTurno']='';}				

				if(!verificaString($this->input->post('vagas')))
				{
					$erro=true;
					$data['erroVagas']='O campo Vagas &eacute obrigat&oacuterio';
				}else{$data['erroVagas']='';}
				
				
				if($erro==false)
				{
					$dataInicialLocal=$this->input->post('datainicial');
					$dataFinalLocal=$this->input->post('datafinal');
						
					$dataInicialLocal=insereData($dataInicialLocal);
					$dataFinalLocal=insereData($dataFinalLocal);
								
					$arrayValores=array(
							'categoriaatividades_id'=>$this->session->userdata('idcategoriaatividades'),
							'titulo'=>$this->input->post('titulo'),
							'descricao'=>$this->input->post('texto1'),
							'datainicial'=>$dataInicialLocal,
							'datafinal'=>$dataFinalLocal,
							'horas'=>$this->input->post('horas'),
							'turno'=>$this->input->post('turno'),
							'vagas'=>$this->input->post('vagas'),
							'usuario_id'=>$this->session->userdata('idusuario'),
							'ativado'=>'1',						
							);
					$this->Gravar($arrayValores,$acao);
					$data['msgCadastro']='Sucesso';
				}
			}			
			
			$this->template->load('templates/adminCadastro', 'admin/atividades/cadastrar',$data);
		}
		
		public function Atualizar($id,$acao=0)
		{
				
			$this->carregaBibliotecasCadastrar();
			
			if ( ! file_exists('application/views/admin/atividades/cadastrar.php'))
			{
				show_404();
			}

			//Obtendo valor da postagem
			$retornoCategoria=$this->categoriaatividades_model->obterPorID($this->session->userdata('idcategoriaatividades'));
			
			$areaCategoria=" da Categoria: ".$retornoCategoria->titulo;
			$linkBreadcrumb="categoriaatividades/";
				
			
			$data['tituloCadastro']="Atualizar Atividade";
			$data['valorOperacao']="categoriaatividades/atividades/Atualizar/".$id."/2";
			$data['listaTurno']=array(''=>'Selecione o turno','Matutino'=>'Matutino','Vespertino'=>'Vespertino','Noturno'=>'Noturno');			
			$data['ckeditor_texto1'] = array
			(
					//id da textarea a ser substitu�da pelo CKEditor
					'id'   => 'texto1',
						
					// caminho da pasta do CKEditor relativo a pasta raiz do CodeIgniter
					'path' => 'assets/js/ckeditor',
						
					// configura��es opcionais
					'config' => array
					(
							'toolbar' => "Basic",
							'width'   => "400px",
							'height'  => "100px",
					)
			);
						
			$retornoAtualiza=$this->atividades_model->obterPorID($id);
						     
			$data['valorCampoTitulo']=$retornoAtualiza->titulo;
			$data['valorCampoDescricao']=$retornoAtualiza->descricao;
			$data['valorCampoDataInicial']=$retornoAtualiza->datainicial;
			$data['valorCampoDataFinal']=$retornoAtualiza->datafinal;
			$data['valorCampoHoras']=$retornoAtualiza->horas;
			$data['itemMarcadoListaTurno']=$retornoAtualiza->turno;
			$data['valorCampoVagas']=$retornoAtualiza->vagas;
				
				
				
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaCategoria."' >Gerenciamento ".$areaCategoria." </a> -> <a href='".base_url()."atividades/'>Gerenciamento Atividades</a> -> Atualizar Atividade");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			$data['msgCadastro']='';
			
			if($acao!=0)
			{
				
				$erro=false;

				//Mantendo estado dos campos ap�s envio
				$data['valorCampoTitulo']=$this->input->post('titulo');
				$data['valorCampoDescricao']=$this->input->post('texto1');
				$data['valorCampoDataInicial']=$this->input->post('datainicial');
				$data['valorCampoDataFinal']=$this->input->post('datafinal');
				$data['valorCampoHoras']=$this->input->post('horas');
				$data['itemMarcadoListaTurno']=$this->input->post('turno');
				$data['valorCampoVagas']=$this->input->post('vagas');
				
													
						
				// Validando informa��es
				// Validando informa��es
				if(!verificaString($this->input->post('titulo')))
				{
					$erro=true;
					$data['erroTitulo']='O campo T&iacute; &eacute obrigat&oacuterio';
				}else{$data['erroTitulo']='';}
				
				if(!verificaString($this->input->post('datainicial')))
				{
					$erro=true;
					$data['erroDataInicial']='O campo Data Inicial &eacute obrigat&oacuterio';
				}else{$data['erroDataInicial']='';}
				
				
				if(!verificaString($this->input->post('datafinal')))
				{
					$erro=true;
					$data['erroDataFinal']='O campo Data Final &eacute obrigat&oacuterio';
				}else{$data['erroDataFinal']='';}
				
				
				if(!verificaString($this->input->post('horas')))
				{
					$erro=true;
					$data['erroHoras']='O campo Horas &eacute obrigat&oacuterio';
				}else{$data['erroHoras']='';}

				if(!verificaString($this->input->post('turno')))
				{
					$erro=true;
					$data['erroTurno']='O campo Turno &eacute obrigat&oacuterio';
				}else{$data['erroTurno']='';}
				
				if(!verificaString($this->input->post('vagas')))
				{
					$erro=true;
					$data['erroVagas']='O campo Vagas &eacute obrigat&oacuterio';
				}else{$data['erroVagas']='';}
				
				
				if($erro==false)
				{
					$dataInicialLocal=$this->input->post('datainicial');
					$dataFinalLocal=$this->input->post('datafinal');
					
					$dataInicialLocal=insereData($dataInicialLocal);
					$dataFinalLocal=insereData($dataFinalLocal);
						
					$arrayValores=array(
							'categoriaatividades_id'=>$this->session->userdata('idcategoriaatividades'),
							'titulo'=>$this->input->post('titulo'),
							'descricao'=>$this->input->post('texto1'),
							'datainicial'=>$dataInicialLocal,
							'datafinal'=>$dataFinalLocal,
							'horas'=>$this->input->post('horas'),
							'turno'=>$this->input->post('turno'),
							'vagas'=>$this->input->post('vagas'),
							);
										
					$this->Gravar($arrayValores,2,$id);
					$data['msgCadastro']='Sucesso';
				}
			}
			$this->template->load('templates/adminCadastro', 'admin/atividades/cadastrar',$data);
				
		}

		public function ConfirmarExclusao($id=null)
		{
			$data['tituloCadastro']="Confirmar Exclus&atildeo";
			$data['valorOperacao']="categoriaatividades/atividades/Excluir/".$id."/1";
			
			$retornoAtualiza=$this->obterPorID($id);
			
			$data['valorCampoTitulo']=$retornoAtualiza->titulo;
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."atividades' class='breadcumb' title='Gerenciamento Atividades'>Gerenciamento Atividades</a> -> Confirmar Exclus&atildeo");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			$this->template->load('templates/adminCadastro', 'admin/atividades/confirmarexclusao',$data);	
		}

		public function Excluir($id=null)
		{
			$this->atividades_model->deletar($id);
			redirect('categoriaatividades/atividades/', 'refresh');
		}
		
		public function Gravar($valores=array(),$acao,$id=null)
		{
			if($acao==1)
			{
				$this->atividades_model->inserir($valores);
			}
			else
			{
				$this->atividades_model->atualizar($id,$valores);
			}
		}

		public function carregaBibliotecasCadastrar()
		{
			$this->load->library('unit_test');
			$this->load->helper('ckeditor');
			$this->load->helper('TrataDados');
		}
		
		public function obterPorID($id)
		{
			return $this->atividades_model->obterPorID($id);			
		}		
	}

?>