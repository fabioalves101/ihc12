<?php
	class Categoria extends CI_Controller
	{		
		public $idusuario="";
		public $menuPorPerfil="";
		public function __construct()
		{
			parent::__construct();

			$this->load->library('session');
			$this->idusuario=$this->session->userdata('idusuario');
			if($this->idusuario=="")
			{
				redirect('login/index/', 'refresh');
			}
				
			$this->load->model('categoria_model');
			$this->load->helper('MontaMenu');
			$this->load->library('image_lib');
			$this->load->model('perfil_model');
			$this->menuPorPerfil=$this->perfil_model->listarModulosPorPerfil($this->session->userdata('idperfil'));
			
			if(in_array("Question&aacuterio",$this->menuPorPerfil)==false)
			{	
				redirect('login/index/', 'refresh');
			}
		}
		
		public function index($pagina=null,$limite=null,$acao=null,$id=null)
		{
			//Carregando Bibliotecas
			$paginator=$this->load->library('pagination');
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/categoria/home.php'))
			{
				show_404();
			}

			if($acao==1)
			{
				$this->session->set_userdata('idcategoria',$id);
				redirect('questionario/questoes/', 'refresh');
			}
				
			
			if($limite==null)
			{
			$limite=10;
			}
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;
				
				
			//Gerando Paginacao
			$config['base_url'] = base_url()."questionario/index/";
			$config['total_rows'] = $this->categoria_model->contarRegistros();
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->categoria_model->listarTodos($limite,$indice);
			$data['caminhoDeletar']="questionario/confirmarexclusao/";
			$data['caminhoEditar']="questionario/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoQuestoes']="questionario/index/".$pagina."/".$limite."/";
				
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> Gerenciamento categoria");
			$this->template->set('caminhoCadastrar','questionario/inserir/');
			$this->template->set('tituloFuncionalidade','Categoria');
			$this->template->set('caminhoFormularioBusca','questionario/buscar/');
			$this->template->set('caminholistarTudo','questionario/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','titulo');			
			$this->template->set('listaPesquisa',array('titulo'=>'T&iacute;tulo'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/categoria/home',$data);			
		}

		public function buscar($pagina=null,$limite=null,$acao=null,$id=null)
		{
			//Carregando Bibliotecas
			$this->load->library('pagination');
			
			$campo=$this->input->post('campo');
			
			if($campo!="")
			{
				
				$this->session->set_userdata('campo',$this->input->post('campo'));
				$this->session->set_userdata('palavra',$this->input->post('palavra'));
			}

			if($acao==1)
			{
				$this->session->set_userdata('idcategoria',$id);
				redirect('questionario/questoes/', 'refresh');
			}
				
			
			//Validando vari�veis
			$limite=10;
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."questionario/buscar/";
			$config['total_rows'] = $this->categoria_model->contarBusca($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice);
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->categoria_model->buscarPaginado($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice);
			$data['caminhoDeletar']="questionario/confirmarexclusao/";
			$data['caminhoEditar']="questionario/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoQuestoes']="questionario/index/".$pagina."/".$limite."/";
				
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> Gerenciamento categoria");
			$this->template->set('caminhoCadastrar','questionario/inserir/');
			$this->template->set('caminholistarTudo','questionario/');
			$this->template->set('tituloFuncionalidade','Categoria');
			$this->template->set('caminhoFormularioBusca','questionario/buscar/');
			$this->template->set('valuePalavraPesquisa',$this->session->userdata('palavra'));
			$this->template->set('itemMarcadoListaPesquisa',$this->session->userdata('campo'));
			$this->template->set('listaPesquisa',array('titulo'=>'T&iacute;tulo'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));

			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/categoria/home',$data);
		}
		
		public function Inserir($acao=null)
		{
				
			$this->carregaBibliotecasCadastrar();
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/categoria/cadastrar.php'))
			{
				show_404();
			}
			
				
			//Atribuindo valores que ser�o repassados a p�gina
			$data['tituloCadastro']="Inserir Categoria";
			$data['valorOperacao']="Inserir/1/";
			$data['valorCampoTitulo']="";
			$data['msgCadastro']='';
				
			//Atribuindo vari�veis de template
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."questionario' class='breadcumb' title='Gerenciamento categoria'>Gerenciamento categoria</a> -> Inserir categoria ");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			
			if(isset($acao))
			{
				$erro=false;
				
				//Mantendo estado dos campos
				$data['valorCampoTitulo']=$this->input->post('titulo');
				
					
				// Validando informa��es
				if(!verificaString($this->input->post('titulo')))
				{
					$erro=true;
					$data['erroTitulo']='O campo t&iacutetulo &eacute obrigat&oacuterio';
				}else{$data['erroTitulo']='';}
				
				if($erro==false)
				{
					$arrayValores=array(
							'titulo'=>$this->input->post('titulo'),
							'usuario_id'=>'1');
					$this->Gravar($arrayValores,$acao);
					$data['msgCadastro']='Sucesso';
				}
			}			
			
			$this->template->load('templates/adminCadastro', 'admin/categoria/cadastrar',$data);
		}
		
		public function Atualizar($id,$acao=0)
		{
			$retornoAtualiza=$this->obterPorID($id);
			

			$this->carregaBibliotecasCadastrar();
							
			
			if ( ! file_exists('application/views/admin/categoria/cadastrar.php'))
			{
				show_404();
			}				
			
			$data['tituloCadastro']="Atualizar Categoria ".$retornoAtualiza->titulo;
			$data['valorOperacao']="Atualizar/".$id."/2";
			$data['valorCampoTitulo']=$retornoAtualiza->titulo;
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."questionario' class='breadcumb' title='Gerenciamento categoria'>Gerenciamento categoria</a> -> Atualizar Descontra");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			$data['msgCadastro']='';
		
			if($acao!=0)
			{
				$erro=false;

				//Mantendo estado dos campos
				$data['valorCampoTitulo']=$this->input->post('titulo');

				// Validando informa��es
				if(!verificaString($this->input->post('titulo')))
				{
					$erro=true;
					$data['erroTitulo']='O campo t&iacutetulo &eacute obrigat&oacuterio';
				}else{$data['erroTitulo']='';}
				
				
				if($erro==false)
				{	
						$arrayValores=array(
								'titulo'=>$this->input->post('titulo'),
								'usuario_id'=>'1');
					$this->Gravar($arrayValores,$acao,$id);
					$data['msgCadastro']='Sucesso';
				}
			}
			$this->template->load('templates/adminCadastro', 'admin/categoria/cadastrar',$data);
		}

		public function ConfirmarExclusao($id=null)
		{
			$data['tituloCadastro']="Confirmar Exclus&atildeo";
			$data['valorOperacao']="questionario/Excluir/".$id."/1";
			
			$retornoAtualiza=$this->obterPorID($id);
			
			$data['valorCampoTitulo']=$retornoAtualiza->titulo;
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."questionario' class='breadcumb' title='Gerenciamento categoria'>Gerenciamento categoria</a> -> Confirmar Exclus&atildeo");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			$this->template->load('templates/adminCadastro', 'admin/categoria/confirmarexclusao',$data);				
		}

		public function Excluir($id=null)
		{
			$retorno=$this->categoria_model->ObterPorId($id);
			if($retorno!=null)
			{
				$this->categoria_model->deletar($id);
				redirect('questionario/', 'refresh');
			}
		}
		
		public function Gravar($valores=array(),$acao,$id=null)
		{
			if($acao==1)
			{
				$this->categoria_model->inserir($valores);
			}
			else
			{
				$this->categoria_model->atualizar($id,$valores);
			}
		}

		public function carregaBibliotecasCadastrar()
		{
			$this->load->helper('TrataDados');				
			$this->load->library('unit_test');	
		}
		
		public function obterPorID($id)
		{
			return $this->categoria_model->obterPorID($id);			
		}		
	}

?>