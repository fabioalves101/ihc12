<?php
	class Questoes extends CI_Controller
	{				
		public $idusuario="";
		public $menuPorPerfil="";
		public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$idusuario=$this->session->userdata('idusuario');
			if($idusuario=="")
			{
				redirect('login/index/', 'refresh');
			}
						$this->load->model('questoes_model');
			$this->load->model('categoria_model');		
			$this->load->helper('MontaMenu');
			$this->load->model('perfil_model');
			$this->menuPorPerfil=$this->perfil_model->listarModulosPorPerfil($this->session->userdata('idperfil'));
			
			if(in_array("Question&aacuterio",$this->menuPorPerfil)==false)
			{	
				redirect('login/index/', 'refresh');
			}
						
		}
		
		public function index($pagina=null,$limite=null,$acao=null,$valorStatus=null,$id=null)
		{
			//Carregando Bibliotecas
			$paginator=$this->load->library('pagination');
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/questoes/home.php'))
			{
				show_404();
			}
			if($limite==null)
			{
			$limite=10;
			}
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;

			if($acao==2)
			{
				if($this->questoes_model->definirStatus($id,$valorStatus))
				{
					redirect('questionario/questoes/index/'.$pagina."/".$limite, 'refresh');
				}							
			}
		
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."questionario/questoes/index/";
			$config['total_rows'] = $this->questoes_model->contarRegistros($this->session->userdata('idcategoria'));
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();
			
			//Obtendo valor da postagem
			$retornoCategoria=$this->categoria_model->obterPorID($this->session->userdata('idcategoria'));
			
			$areaCategoria=" da Categoria: ".$retornoCategoria->titulo;
			$linkBreadcrumb="questionario/";
			
			
			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->questoes_model->listarTodos($limite,$indice,$this->session->userdata('idcategoria'));
			$data['caminhoDeletar']="questionario/questoes/confirmarexclusao/";
			$data['caminhoEditar']="questionario/questoes/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoDefinirStatus']="questionario/questoes/".$pagina."/".$limite."/";
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaCategoria."' >Gerenciamento ".$areaCategoria." </a> -> Gerenciamento Quest&otilde;es");
			$this->template->set('caminhoCadastrar','questionario/questoes/inserir/');
			$this->template->set('tituloFuncionalidade','Quest&otilde;es'.$areaCategoria);
			$this->template->set('caminhoFormularioBusca','questionario/questoes/buscar/');
			$this->template->set('caminholistarTudo','questionario/questoes/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','questao1');			
			$this->template->set('listaPesquisa',array('questao1'=>'Quest&atilde;o 1','questao2'=>'Quest&atilde;o 2','questao3'=>'Quest&atilde;o 3','questao4'=>'Quest&atilde;o 4','questao5'=>'Quest&atilde;o 5'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/questoes/home',$data);			
		}

		public function buscar($pagina=null,$limite=null,$acao=null,$valorStatus=null,$id=null)
		{
			//Carregando Bibliotecas
			$this->load->library('pagination');
			
			$campo=$this->input->post('campo');
			
			if($campo!="")
			{				
				$this->session->set_userdata('campo',$this->input->post('campo'));
				$this->session->set_userdata('palavra',$this->input->post('palavra'));
			}
			//Validando vari�veis
			$limite=10;
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;	

			//Obtendo valor da postagem
			$retornoCategoria=$this->categoria_model->obterPorID($this->session->userdata('idcategoria'));
				
			$areaCategoria=" da Categoria: ".$retornoCategoria->titulo;
			$linkBreadcrumb="questionario/";
				
			if($acao==2)
			{
				if($this->questoes_model->definirStatus($id,$valorStatus))
				{
					redirect('questoes/index/'.$pagina."/".$limite, 'refresh');
				}
			}			
			
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."questionario/questoes/buscar/";
			$config['total_rows'] = $this->questoes_model->contarBusca($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice,$this->session->userdata('idcategoria'));
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->questoes_model->buscarPaginado($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice,$this->session->userdata('idcategoria'));
			$data['caminhoDeletar']="questionario/questoes/confirmarexclusao/";
			$data['caminhoEditar']="questionario/questoes/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			$data['caminhoDefinirStatus']="questionario/questoes/index/".$pagina."/".$limite."/";
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$data['lista']=$this->questoes_model->listarTodos($limite,$indice,$this->session->userdata('idcategoria'));
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaCategoria."' >Gerenciamento ".$areaCategoria." </a> -> Gerenciamento Quest&otilde;es");
			$this->template->set('caminhoCadastrar','questionario/questoes/inserir/');
			$this->template->set('tituloFuncionalidade','Quest&otilde;es'.$areaCategoria);
			$this->template->set('caminhoFormularioBusca','questionario/questoes/buscar/');
			$this->template->set('caminholistarTudo','questionario/questoes/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','questao1');			
			$this->template->set('listaPesquisa',array('questao1'=>'Quest&atilde;o 1','questao2'=>'Quest&atilde;o 2','questao3'=>'Quest&atilde;o 3','questao4'=>'Quest&atilde;o 4','questao5'=>'Quest&atilde;o 5'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/questoes/home',$data);
		}
		
		
		public function Inserir($acao=null)
		{
			//Carregando Bibliotecas
			$this->carregaBibliotecasCadastrar();
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/questoes/cadastrar.php'))
			{
				show_404();
			}
			
			//Obtendo valor da categoria
			$retornoCategoria=$this->categoria_model->obterPorID($this->session->userdata('idcategoria'));
				
			$areaCategoria=" da Categoria: ".$retornoCategoria->titulo;
			$linkBreadcrumb="questionario/";
			
			
			//Atribuindo valores que ser�o repassados a p�gina
			$data['tituloCadastro']="Inserir Palavras";
			$data['valorOperacao']="questionario/questoes/Inserir/1/";
			$data['textoQuestao1']="";
			$data['textoQuestao2']="";
			$data['textoQuestao3']="";
			$data['textoQuestao4']="";
			$data['textoQuestao5']="";
			$data['listaQuestoes']=array(''=>'Selecione a quest&atilde;o correta','1'=>'Quest&atilde;o 1','2'=>'Quest&atilde;o 2','3'=>'Quest&atilde;o 3','4'=>'Quest&atilde;o 4','5'=>'Quest&atilde;o 5');			
			$data['questaocorreta']="";
			$data['msgCadastro']='';
				
			//Atribuindo vari�veis de template
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaCategoria."' >Gerenciamento ".$areaCategoria." </a> -> <a href='".base_url()."questoes/'>Gerenciamento Quest&otilde;es</a> -> Inserir Coment&aacute;rio");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			if(isset($acao))
			{
				$erro=false;
				
				//Mantendo estado dos campos
				$data['textoQuestao1']=$this->input->post('questao1');
				$data['textoQuestao2']=$this->input->post('questao2');
				$data['textoQuestao3']=$this->input->post('questao3');
				$data['textoQuestao4']=$this->input->post('questao4');
				$data['textoQuestao5']=$this->input->post('questao5');
				$data['questaocorreta']=$this->input->post('questaocorreta');
								
			
				// Validando informa��es
				if(!verificaString($this->input->post('questao1')))
				{
					$erro=true;
					$data['erroQuestao1']='O campo Quest&atilde;o 1 &eacute obrigat&oacuterio';
				}else{$data['erroQuestao1']='';}

				if(!verificaString($this->input->post('questao2')))
				{
					$erro=true;
					$data['erroQuestao2']='O campo Quest&atilde;o 2 &eacute obrigat&oacuterio';
				}else{$data['erroQuestao2']='';}
				
				
				if(!verificaString($this->input->post('questao3')))
				{
					$erro=true;
					$data['erroQuestao3']='O campo Quest&atilde;o 3 &eacute obrigat&oacuterio';
				}else{$data['erroQuestao3']='';}
				
				
				if(!verificaString($this->input->post('questao4')))
				{
					$erro=true;
					$data['erroQuestao4']='O campo Quest&atilde;o 4 &eacute obrigat&oacuterio';
				}else{$data['erroQuestao4']='';}
				
				
				if(!verificaString($this->input->post('questao5')))
				{
					$erro=true;
					$data['erroQuestao5']='O campo Quest&atilde;o 5 &eacute obrigat&oacuterio';
				}else{$data['erroQuestao5']='';}

				if(!verificaString($this->input->post('questaocorreta')))
				{
					$erro=true;
					$data['erroQuestaoCorreta']='O campo Quest&atilde;o correta &eacute obrigat&oacuterio';
				}else{$data['erroQuestaoCorreta']='';}
				
				
				if($erro==false)
				{
					$arrayValores=array(
							'categoria_id'=>$this->session->userdata('idcategoria'),
							'questao1'=>$this->input->post('questao1'),
							'questao2'=>$this->input->post('questao2'),
							'questao3'=>$this->input->post('questao3'),
							'questao4'=>$this->input->post('questao4'),
							'questao5'=>$this->input->post('questao5'),
							'questaocorreta'=>$this->input->post('questaocorreta'),
							'usuario_id'=>'1',
							'ativado'=>'1',						
							);
					$this->Gravar($arrayValores,$acao);
					$data['msgCadastro']='Sucesso';
				}
			}			
			
			$this->template->load('templates/adminCadastro', 'admin/questoes/cadastrar',$data);
		}
		
		public function Atualizar($id,$acao=0)
		{
			$this->carregaBibliotecasCadastrar();
			
			if ( ! file_exists('application/views/admin/questoes/cadastrar.php'))
			{
				show_404();
			}

			//Obtendo valor da postagem
			$retornoCategoria=$this->categoria_model->obterPorID($this->session->userdata('idcategoria'));
			
			$areaCategoria=" da Categoria: ".$retornoCategoria->titulo;
			$linkBreadcrumb="questionario/";
				
			
			$data['tituloCadastro']="Atualizar Coment&aacute;rios";
			$data['valorOperacao']="questionario/questoes/Atualizar/".$id."/2";
			$retornoAtualiza=$this->obterPorID($id);
			       
			$data['textoQuestao1']=$retornoAtualiza->questao1;
			$data['textoQuestao2']=$retornoAtualiza->questao2;
			$data['textoQuestao3']=$retornoAtualiza->questao3;
			$data['textoQuestao4']=$retornoAtualiza->questao4;
			$data['textoQuestao5']=$retornoAtualiza->questao5;
			$data['listaQuestoes']=array(''=>'Selecione a quest&atilde;o correta','1'=>'Quest&atilde;o 1','2'=>'Quest&atilde;o 2','3'=>'Quest&atilde;o 3','4'=>'Quest&atilde;o 4','5'=>'Quest&atilde;o 5');
			$data['questaocorreta']=$retornoAtualiza->questaocorreta;
				
				
				
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a>-><a href='".base_url().$linkBreadcrumb."' class='breadcumb' title='Gerenciamento ".$areaCategoria."' >Gerenciamento ".$areaCategoria." </a> -> <a href='".base_url()."questoes/'>Gerenciamento Quest&otilde;es</a> -> Atualizar Coment&aacute;rio");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			$data['msgCadastro']='';
			
			if($acao!=0)
			{
				
				$erro=false;

				//Mantendo estado dos campos ap�s envio
				$data['textoQuestao1']=$this->input->post('questao1');
				$data['textoQuestao2']=$this->input->post('questao2');
				$data['textoQuestao3']=$this->input->post('questao3');
				$data['textoQuestao4']=$this->input->post('questao4');
				$data['textoQuestao5']=$this->input->post('questao5');
				$data['questaocorreta']=$this->input->post('questaocorreta');
									
						
							// Validando informa��es
				if(!verificaString($this->input->post('questao1')))
				{
					$erro=true;
					$data['erroQuestao1']='O campo Quest&atilde;o 1 &eacute obrigat&oacuterio';
				}else{$data['erroQuestao1']='';}

				if(!verificaString($this->input->post('questao2')))
				{
					$erro=true;
					$data['erroQuestao2']='O campo Quest&atilde;o 2 &eacute obrigat&oacuterio';
				}else{$data['erroQuestao2']='';}
				
				
				if(!verificaString($this->input->post('questao3')))
				{
					$erro=true;
					$data['erroQuestao3']='O campo Quest&atilde;o 3 &eacute obrigat&oacuterio';
				}else{$data['erroQuestao3']='';}
				
				
				if(!verificaString($this->input->post('questao4')))
				{
					$erro=true;
					$data['erroQuestao4']='O campo Quest&atilde;o 4 &eacute obrigat&oacuterio';
				}else{$data['erroQuestao4']='';}
				
				
				if(!verificaString($this->input->post('questao5')))
				{
					$erro=true;
					$data['erroQuestao5']='O campo Quest&atilde;o 5 &eacute obrigat&oacuterio';
				}else{$data['erroQuestao5']='';}

				if(!verificaString($this->input->post('questaocorreta')))
				{
					$erro=true;
					$data['erroQuestaoCorreta']='O campo Quest&atilde;o correta &eacute obrigat&oacuterio';
				}else{$data['erroQuestaoCorreta']='';}
								
				if($erro==false)
				{
					$arrayValores=array(
							'categoria_id'=>$this->session->userdata('idcategoria'),
							'questao1'=>$this->input->post('questao1'),
							'questao2'=>$this->input->post('questao2'),
							'questao3'=>$this->input->post('questao3'),
							'questao4'=>$this->input->post('questao4'),
							'questao5'=>$this->input->post('questao5'),
							'questaocorreta'=>$this->input->post('questaocorreta'),
							);
					
					$this->Gravar($arrayValores,2,$id);
					$data['msgCadastro']='Sucesso';
				}
			}
			$this->template->load('templates/adminCadastro', 'admin/questoes/cadastrar',$data);
				
		}

		public function ConfirmarExclusao($id=null)
		{
			$data['tituloCadastro']="Confirmar Exclus&atildeo";
			$data['valorOperacao']="questionario/questoes/Excluir/".$id."/1";
			
			$retornoAtualiza=$this->obterPorID($id);
			
			$data['valorCampoTitulo']=$retornoAtualiza->questao1;
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."questoes' class='breadcumb' title='Gerenciamento Question&aacuterio'>Gerenciamento Question&aacuterio</a> -> Confirmar Exclus&atildeo");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			$this->template->load('templates/adminCadastro', 'admin/questoes/confirmarexclusao',$data);	
		}

		public function Excluir($id=null)
		{
			$this->questoes_model->deletar($id);
			redirect('questionario/questoes/', 'refresh');
		}
		
		public function Gravar($valores=array(),$acao,$id=null)
		{
			if($acao==1)
			{
				$this->questoes_model->inserir($valores);
			}
			else
			{
				$this->questoes_model->atualizar($id,$valores);
			}
		}

		public function carregaBibliotecasCadastrar()
		{
			$this->load->library('unit_test');
			$this->load->helper('ckeditor');
			$this->load->helper('TrataDados');
		}
		
		public function obterPorID($id)
		{
			return $this->questoes_model->obterPorID($id);			
		}		
	}

?>