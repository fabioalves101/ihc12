<?php
	class Contato extends CI_Controller
	{
		public $caminho="./assets/imagenspostagens/";
		public function __construct()
		{
			parent::__construct();
			$this->load->helper('MontaMenu');
			$this->load->library('image_lib');
		}
		
		public function index()
		{
			$this->load->helper('MontaMenu');
			
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"P&aacutegina Inicial");
			$this->template->set('nav_list',MontandoMenu());
			
			$data['msgSucesso']="";
			$data['valorCampoNome']="";
			$data['valorCampoEmail']="";
			$data['valorCampoMensagem']="";
			$data['erroNome']="";
			$data['erroEmail']="";
			$data['erroMensagem']="";
			$data['actionFormulario']="./contato/enviar/";
			
			
			$this->template->load('templates/inicial', 'contato',$data);
				
		}
		
		public function enviar()
		{
			$this->load->helper('TrataDados');
			$erro=false;
			$data['msgSucesso']="";
			$data['valorCampoNome']="";
			$data['valorCampoEmail']="";
			$data['valorCampoMensagem']="";
			$data['erroNome']="";
			$data['erroEmail']="";
			$data['erroMensagem']="";
				

			if(!verificaString($this->input->post('nome')))
			{
				$erro=true;
				$data['erroNome']="O campo nome &eacute; obrigat&oacute;rio";
			}

			if(!verificar_email($this->input->post('email')))
			{
				$erro=true;
				$data['erroEmail']="O campo email deve ser v&aacute,lido";
					
			}
				
			
			if(!verificaString($this->input->post('mensagem')))
			{
				$erro=true;
				$data['erroMensagem']="O campo mensagem &eacute; obrigat&oacute;rio";
			}
			
			$data['valorCampoNome']=$this->input->post('nome');
			$data['valorCampoEmail']=$this->input->post('email');
			$data['valorCampoMensagem']=$this->input->post('mensagem');
				
				

			if($erro==false)
			{
				$this->load->library('email');
				$config['protocol']  = 'smtp'; // Podendo ser alterado para mail caso voc� queira enviar com o mail do php.
				$config['charset'] = 'utf8';
				$config['wordwrap'] = TRUE;
				$config['smtp_host'] = 'smtp.gmail.com';
				$config['smtp_user'] = 'odenor@gmail.com';
				$config['smtp_pass'] = '92269951';
				$config['smtp_timeout'] = 20;
				$config['mailtype'] = 'html';
								
	
				$this->email->initialize($config);			
				$this->email->from($this->input->post('email'),$this->input->post('nome'));
				$this->email->to('odenor@gmail.com');
				$this->email->subject('Email de Contato');
				$this->email->message($this->input->post('mensagem'));
				
				if($this->email->send())
				{
					$this->load->helper('MontaMenu');
						
					$this->template->set('msgAcao','');
					$this->template->set('Breadcrumb',"P&aacutegina Inicial");
					$this->template->set('nav_list',MontandoMenu());
	
					$data['msgSucesso']="Email Enviado com sucesso.";
					$data['valorCampoNome']="";
					$data['valorCampoEmail']="";
					$data['valorCampoMensagem']="";
					$data['erroNome']="";
					$data['erroEmail']="";
					$data['erroMensagem']="";
					$data['actionFormulario']="./contato/enviar/";
										
					$this->template->load('templates/inicial', 'contato',$data);
				}
			}
			else
			{
				$this->load->helper('MontaMenu');
					
				$this->template->set('msgAcao','');
				$this->template->set('Breadcrumb',"P&aacutegina Inicial");
				$this->template->set('nav_list',MontandoMenu());
				$data['actionFormulario']="./contato/enviar/";
					
				$this->template->load('templates/inicial', 'contato',$data);
				
				
			}
		}
		
		public function Indique()
		{
			$this->load->helper('MontaMenu');
				
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"P&aacutegina Inicial");
			$this->template->set('nav_list',MontandoMenu());
				
			$data['msgSucesso']="";
			$data['valorCampoNome']="";
			$data['valorCampoEmail']="";
			$data['valorCampoMensagem']="";
			$data['erroNome']="";
			$data['erroEmail']="";
			$data['erroMensagem']="";
			$data['actionFormulario']="contato/Indicar/";
				
				
			$this->template->load('templates/inicial', 'contato',$data);
				
		}
		
		public function Indicar()
		{}
	}
?>