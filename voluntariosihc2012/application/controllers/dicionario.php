<?php
	class Dicionario extends CI_Controller
	{				
		public $idusuario="";
		public $menuPorPerfil="";
		public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$idusuario=$this->session->userdata('idusuario');
			if($idusuario=="")
			{
				redirect('login/index/', 'refresh');
			}
				
			$this->load->model('dicionario_model');
			$this->load->helper('MontaMenu');
			$this->load->model('perfil_model');
			$this->menuPorPerfil=$this->perfil_model->listarModulosPorPerfil($this->session->userdata('idperfil'));

			if(in_array("Dicion&aacute;rio",$this->menuPorPerfil)==false)
			{
				redirect('login/index/', 'refresh');
			}
				
		}
		
		public function index($pagina=null,$limite=null)
		{
			//Carregando Bibliotecas
			$paginator=$this->load->library('pagination');
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/dicionario/home.php'))
			{
				show_404();
			}
			if($limite==null)
			{
			$limite=10;
			}
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;
				
		
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."dicionario/index/";
			$config['total_rows'] = $this->dicionario_model->contarRegistros();
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->dicionario_model->listarTodos($limite,$indice);
			$data['caminhoDeletar']="dicionario/confirmarexclusao/";
			$data['caminhoEditar']="dicionario/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> Gerenciamento Dicion&aacuterio");
			$this->template->set('caminhoCadastrar','dicionario/inserir/');
			$this->template->set('tituloFuncionalidade','Dicion&aacute;rio');
			$this->template->set('caminhoFormularioBusca','dicionario/buscar/');
			$this->template->set('caminholistarTudo','dicionario/');
			$this->template->set('valuePalavraPesquisa','');
			$this->template->set('itemMarcadoListaPesquisa','titulo');			
			$this->template->set('listaPesquisa',array('titulo'=>'T&iacute;tulo','descricao'=>'Descri&ccedil;&atilde;o'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/dicionario/home',$data);			
		}

		public function buscar($pagina=null,$limite=null)
		{
			//Carregando Bibliotecas
			$this->load->library('pagination');
			
			$campo=$this->input->post('campo');
			
			if($campo!="")
			{
				
				$this->session->set_userdata('campo',$this->input->post('campo'));
				$this->session->set_userdata('palavra',$this->input->post('palavra'));
			}
			//Validando vari�veis
			$limite=10;
			if(isset($pagina)) {
				$pagina = $pagina;
			} else {
				$pagina = 1;
			}
				
			$indice= ($pagina-1)  * $limite;	

			
			
			
			//Gerando Paginacao
			$config['base_url'] = base_url()."dicionario/buscar/";
			$config['total_rows'] = $this->dicionario_model->contarBusca($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice);
			$config['per_page'] = $limite;
			$config['use_page_numbers'] = TRUE;
			$this->pagination->initialize($config);
			$data['paginacao']=$this->pagination->create_links();

			//Atribuindo valores que ser�o repassados a p�gina
			$data['lista']=$this->dicionario_model->buscarPaginado($this->session->userdata('campo'),$this->session->userdata('palavra'),$limite,$indice);
			$data['caminhoDeletar']="dicionario/confirmarexclusao/";
			$data['caminhoEditar']="dicionario/atualizar/";
			$data['totalRegistros']=$config['total_rows'];
			
			//Atribuindo vari�veis de template
			$this->template->set('msgAcao','');
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> Gerenciamento Dicion&aacuterio");
			$this->template->set('caminhoCadastrar','dicionario/inserir/');
			$this->template->set('caminholistarTudo','dicionario/');
			$this->template->set('tituloFuncionalidade','Dicion&aacute;rio');
			$this->template->set('caminhoFormularioBusca','dicionario/buscar/');
			$this->template->set('valuePalavraPesquisa',$this->session->userdata('palavra'));
			$this->template->set('itemMarcadoListaPesquisa',$this->session->userdata('campo'));
			$this->template->set('listaPesquisa',array('titulo'=>'T&iacute;tulo','descricao'=>'Descri&ccedil;&atilde;o'));
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));

			//Carregando p�gina
			$this->template->load('templates/admin', 'admin/dicionario/home',$data);
		}
		
		
		public function Inserir($acao=null)
		{
			//Carregando Bibliotecas
			$this->carregaBibliotecasCadastrar();
			
			//Validando vari�veis
			if ( ! file_exists('application/views/admin/dicionario/cadastrar.php'))
			{
				show_404();
			}
				
			//Atribuindo valores que ser�o repassados a p�gina
			$data['tituloCadastro']="Inserir Palavras";
			$data['valorOperacao']="Inserir/1/";
			$data['valorCampoTitulo']="";
			$data['textoDescricao']="";
			$data['ckeditor_texto1'] = array
			(
					//id da textarea a ser substitu�da pelo CKEditor
					'id'   => 'texto1',
						
					// caminho da pasta do CKEditor relativo a pasta raiz do CodeIgniter
					'path' => 'assets/js/ckeditor',
						
					// configura��es opcionais
					'config' => array
					(
							'toolbar' => "Basic",
							'width'   => "400px",
							'height'  => "100px",
					)
			);
			$data['checkAtivo']=array('name'=>'statusPalavra','id'=>'statusPalavara_A','value'=>'1','checked'=> TRUE);
			$data['checkDesativo']=array('name'=>'statusPalavra','id'=>'statusPalavara_D','value'=>'0','checked'=> FALSE);
			$data['msgCadastro']='';
				
			//Atribuindo vari�veis de template
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."dicionario' class='breadcumb' title='Gerenciamento Dicion&aacuterio'>Gerenciamento Dicion&aacuterio</a> -> Inserir Palavras ");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			
			if(isset($acao))
			{
				$erro=false;
				
				//Mantendo estado dos campos
				$data['valorCampoTitulo']=$this->input->post('titulo');
				$data['textoDescricao']=$this->input->post('texto1');
				
				if($this->input->post('statusPalavra')=="1")
				{
					$data['checkAtivo']=array('name'=>'statusPalavra','id'=>'statusPalavara_A','value'=>'1','checked'=> TRUE);
					$data['checkDesativo']=array('name'=>'statusPalavra','id'=>'statusPalavara_D','value'=>'0','checked'=> FALSE);
				}
				else
				{
					$data['checkAtivo']=array('name'=>'statusPalavra','id'=>'statusPalavara_A','value'=>'1','checked'=> FALSE);
					$data['checkDesativo']=array('name'=>'statusPalavra','id'=>'statusPalavara_D','value'=>'0','checked'=> TRUE);
				}
				
				// Validando informa��es
				if(!verificaString($this->input->post('titulo')))
				{
					$erro=true;
					$data['erroTitulo']='O campo t&iacutetulo &eacute obrigat&oacuterio';
				}else{$data['erroTitulo']='';}

				if(!verificaString($this->input->post('texto1')))
				{
					$erro=true;
					$data['erroTexto']='O campo descri&ccedil;&atilde;o &eacute obrigat&oacuterio';
				}else{$data['erroTexto']='';}
				
				if($erro==false)
				{
					$arrayValores=array(
							'titulo'=>$this->input->post('titulo'),
							'descricao'=>$this->input->post('texto1'),
							'usuario_id'=>'1',
							'ativado'=>$this->input->post('statusPalavra'),
						
							);
					$this->Gravar($arrayValores,$acao);
					$data['msgCadastro']='Sucesso';
				}
			}			
			
			$this->template->load('templates/adminCadastro', 'admin/dicionario/cadastrar',$data);
		}
		
		public function Atualizar($id,$acao=0)
		{
			$this->carregaBibliotecasCadastrar();
			
			if ( ! file_exists('application/views/admin/dicionario/cadastrar.php'))
			{
				show_404();
			}
			
			$data['tituloCadastro']="Atualizar Palavras";
			$data['valorOperacao']="Atualizar/".$id."/2";
				
			$retornoAtualiza=$this->obterPorID($id);
				
			$data['valorCampoTitulo']=$retornoAtualiza->titulo;
			$data['textoDescricao']=$retornoAtualiza->descricao;
			$data['statusPalavra']=$retornoAtualiza->statusPalavra;
				
			$data['ckeditor_texto1'] = array
			(
					//id da textarea a ser substitu�da pelo CKEditor
					'id'   => 'texto1',
			
					// caminho da pasta do CKEditor relativo a pasta raiz do CodeIgniter
					'path' => 'assets/js/ckeditor',
			
					// configura��es opcionais
					'config' => array
					(
							'toolbar' => "Basic",
							'width'   => "400px",
							'height'  => "100px",
					)
			);
				
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."dicionario' class='breadcumb' title='Gerenciamento Dicion&aacuterio'>Gerenciamento Dicion&aacuterio</a> -> Inserir Palavras ");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			
			if($data['statusPalavra']==="1")
			{
			$data['checkAtivo']=array('name'=>'statusPalavra','id'=>'statusPalavara_A','value'=>'1','checked'=> TRUE);
			$data['checkDesativo']=array('name'=>'statusPalavra','id'=>'statusPalavara_D','value'=>'0','checked'=> FALSE);
			}
			else
			{
				$data['checkAtivo']=array('name'=>'statusPalavra','id'=>'statusPalavara_A','value'=>'1','checked'=> FALSE);
				$data['checkDesativo']=array('name'=>'statusPalavra','id'=>'statusPalavara_D','value'=>'0','checked'=> TRUE);				
			}	
			$data['msgCadastro']='';

				
			
			if($acao!=0)
			{
				
				$erro=false;

				//Mantendo estado dos campos ap�s envio
				$data['valorCampoTitulo']=$this->input->post('titulo');
				$data['textoDescricao']=$this->input->post('texto1');
				$data['statusPalavra']=$this->input->post('statusPalavra');
					
				if($data['statusPalavra']==="1")
				{
					$data['checkAtivo']=array('name'=>'statusPalavra','id'=>'statusPalavara_A','value'=>'1','checked'=> TRUE);
					$data['checkDesativo']=array('name'=>'statusPalavra','id'=>'statusPalavara_D','value'=>'0','checked'=> FALSE);
				}
				else
				{
					$data['checkAtivo']=array('name'=>'statusPalavra','id'=>'statusPalavara_A','value'=>'1','checked'=> FALSE);
					$data['checkDesativo']=array('name'=>'statusPalavra','id'=>'statusPalavara_D','value'=>'0','checked'=> TRUE);
				}
				

				
				// Validando informa��es
				if(!verificaString($data['valorCampoTitulo']))
				{
					$erro=true;
					$data['erroTitulo']='O campo t&iacutetulo &eacute obrigat&oacuterio';
				}else{$data['erroTitulo']='';}
				
				if(!verificaString($data['textoDescricao']))
				{
					$erro=true;
					$data['erroTexto']='O campo texto &eacute obrigat&oacuterio';
				}else{$data['erroTexto']='';}
				
				if($erro==false)
				{
				
				
				$arrayValores=array(
						'titulo'=>$this->input->post('titulo'),
						'descricao'=>$this->input->post('texto1'),
						'usuario_id'=>'1',
						'ativado'=>$this->input->post('statusPalavra'),
			
				);
				
				$this->Gravar($arrayValores,2,$id);
				$data['msgCadastro']='Sucesso';
				}
			}
			$this->template->load('templates/adminCadastro', 'admin/dicionario/cadastrar',$data);
				
		}

		public function ConfirmarExclusao($id=null)
		{
			$data['tituloCadastro']="Confirmar Exclus&atildeo";
			$data['valorOperacao']="dicionario/Excluir/".$id."/1";
			
			$retornoAtualiza=$this->obterPorID($id);
			
			$data['valorCampoTitulo']=$retornoAtualiza->titulo;
			
			$this->template->set('Breadcrumb',"<a href='".base_url()."inicial' class='breadcumb' title='P&aacutegina Inicial'>P&aacutegina Inicial</a> -> <a href='".base_url()."dicionario' class='breadcumb' title='Gerenciamento Dicion&aacuterio'>Gerenciamento Dicion&aacuterio</a> -> Confirmar Exclus&atildeo");
			$this->template->set('nav_list',MontandoMenu($this->menuPorPerfil));
			$this->template->load('templates/adminCadastro', 'admin/dicionario/confirmarexclusao',$data);				
		}

		public function Excluir($id=null)
		{
			$this->dicionario_model->deletar($id);
			redirect('dicionario/', 'refresh');
		}
		
		public function Gravar($valores=array(),$acao,$id=null)
		{
			if($acao==1)
			{
				$this->dicionario_model->inserir($valores);
			}
			else
			{
				$this->dicionario_model->atualizar($id,$valores);
			}
		}

		public function carregaBibliotecasCadastrar()
		{
			$this->load->library('unit_test');
			$this->load->helper('ckeditor');
			$this->load->helper('TrataDados');
		}
		
		public function obterPorID($id)
		{
			return $this->dicionario_model->obterPorID($id);			
		}		
	}

?>