<?php
class Comentarios_Model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}
	
	/*Create*/
	public function inserir($valores=array())
	{
		$this->db->insert('comentarios', $valores);		
	}
	
	/*Retrive*/
	//public function listarPaginado($limite,$pag);
	public function contarRegistros($idPostagem)
	{
		$query=$this->db->query("SELECT c.id as codigo,c.descricao as descricao, c.ativado as valorStatus,case c.ativado  when '1' then 'Ativada' else 'Desativada' end as status ,u.nome as nomeusuario FROM comentarios as c inner join usuarios as u on u.id=c.usuario_id
inner join postagens p on c.postagem_id=p.id WHERE c.postagem_id=".$idPostagem);
		return $query->num_rows();		
	}
	
	public function listarTodos($limite,$indice,$idPostagem)
	{
		$query=$this->db->query("SELECT c.id as codigo,c.descricao as descricao, c.ativado as valorStatus,case c.ativado  when '1' then 'Ativada' else 'Desativada' end as status ,u.nome as nomeusuario FROM comentarios as c inner join usuarios as u on u.id=c.usuario_id
inner join postagens p on c.postagem_id=p.id WHERE c.postagem_id='".$idPostagem."'  Order BY c.id ASC LIMIT ".$indice." , ".$limite);
		return $query->result();
	}

	public function contarBusca($campo,$parametro,$idPostagem)
	{

		$query=$this->db->query("SELECT c.id as codigo,c.descricao as descricao, c.ativado as valorStatus,case c.ativado  when '1' then 'Ativada' else 'Desativada' end as status ,u.nome as nomeusuario FROM comentarios as c inner join usuarios as u on u.id=c.usuario_id
inner join postagens p on c.postagem_id=p.id WHERE c.postagem_id='".$idPostagem."' and c.".$campo." like '%".$parametro."%'");						
		return $query->num_rows();
		
	}
	
	public function buscarPaginado($campo,$parametro,$limite,$indice,$idPostagem)
	{
		$sql="SELECT c.id as codigo,c.descricao as descricao, c.ativado as valorStatus,case c.ativado  when '1' then 'Ativada' else 'Desativada' end as status ,u.nome as nomeusuario FROM comentarios as c inner join usuarios as u on u.id=c.usuario_id
inner join postagens p on c.postagem_id=p.id WHERE c.postagem_id='".$idPostagem."' and c.".$campo." like '%".$parametro."%' Order BY c.id ASC Limit ".$indice.",".$limite;
		
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function obterPorID($id)
	{
		$query=$this->db->query("SELECT c.id as codigo,c.descricao as descricao, c.ativado as valorStatus,c.ativado as status,u.nome as nomeusuario FROM comentarios as c inner join usuarios as u on u.id=c.usuario_id
inner join postagens p on c.postagem_id=p.id WHERE c.id='".$id."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
		   return $query->row();
		}		
	}
	
	/*Update*/
	public function atualizar($id,$valores=array())
	{
		$this->db->where('id',$id);
		$this->db->update('comentarios', $valores);
	}

	public function definirStatus($id,$valorStatus)
	{
		$valor=1-$valorStatus;
		$sql="UPDATE comentarios SET ativado='".$valor."' WHERE id='".$id."'";
		if($this->db->query($sql))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	

	/*Delete*/
	public function deletar($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('comentarios');
		
	}
}
?>