<?php
class Dicionario_Model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}
	
	/*Create*/
	public function inserir($valores=array())
	{
		$this->db->insert('dicionarios', $valores);		
	}
	
	/*Retrive*/
	
	//public function listarPaginado($limite,$pag);
	
	
	public function contarRegistros()
	{
		$query=$this->db->query("SELECT d.id as codigo,d.titulo as titulo,d.descricao as descricao, d.ativado as valorStatus,case d.ativado  when '1' then 'Ativada' else 'Desativada' end as statusPalavra ,u.nome as nomeusuario FROM dicionarios as d inner join usuarios as u on u.id=d.usuario_id ");
		return $query->num_rows();		
	}
	
	public function listarTodos($limite,$indice)
	{
		$query=$this->db->query("SELECT d.id as codigo,d.titulo as titulo,d.descricao as descricao, d.ativado as valorStatus,case d.ativado  when '1' then 'Ativada' else 'Desativada' end as statusPalavra ,u.nome as nomeusuario FROM dicionarios as d inner join usuarios as u on u.id=d.usuario_id Order BY d.titulo ASC LIMIT ".$indice." , ".$limite);
		return $query->result();
	}

	public function contarBusca($campo,$parametro)
	{
		if($campo=="id")
		{
			$query=$this->db->query("SELECT d.id as codigo,d.titulo as titulo,d.descricao as descricao, d.ativado as valorStatus,case d.ativado  when '1' then 'Ativada' else 'Desativada' end as statusPalavra ,u.nome as nomeusuario FROM dicionarios as d inner join usuarios as u on u.id=d.usuario_id WHERE d.id='".$parametro."'");
		}
		else
		{
			$query=$this->db->query("SELECT d.id as codigo,d.titulo as titulo,d.descricao as descricao, d.ativado as valorStatus,case d.ativado  when '1' then 'Ativada' else 'Desativada' end as statusPalavra ,u.nome as nomeusuario FROM dicionarios as d inner join usuarios as u on u.id=d.usuario_id WHERE d.".$campo." like '%".$parametro."%'");						
		}
		return $query->num_rows();
		
	}
	
	public function buscarPaginado($campo,$parametro,$limite,$indice)
	{
		
		if($campo=="id")
		{
			$sql="SELECT d.id as codigo,d.titulo as titulo,d.descricao as descricao, d.ativado as valorStatus,case d.ativado  when '1' then 'Ativada' else 'Desativada' end as statusPalavra ,u.nome as nomeusuario FROM dicionarios as d inner join usuarios as u on u.id=d.usuario_id WHERE d.".$campo."='".$parametro."' Order BY d.titulo ASC Limit ".$indice.",".$limite;
		}
		else
		{
			$sql="SELECT d.id as codigo,d.titulo as titulo,d.descricao as descricao, d.ativado as valorStatus,case d.ativado  when '1' then 'Ativada' else 'Desativada' end as statusPalavra ,u.nome as nomeusuario FROM dicionarios as d inner join usuarios as u on u.id=d.usuario_id WHERE d.".$campo." like '%".$parametro."%' Order BY d.titulo ASC Limit ".$indice.",".$limite;
		}
		
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function obterPorID($id)
	{
		$query=$this->db->query("SELECT id as codigo,titulo as titulo,descricao as descricao, d.ativado as valorStatus,ativado as statusPalavra FROM dicionarios WHERE id='".$id."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
		   return $query->row();
		}		
	}
	
	/*Update*/
	public function atualizar($id,$valores=array())
	{
		$this->db->where('id',$id);
		$this->db->update('dicionarios', $valores);
	}

	/*Delete*/
	public function deletar($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('dicionarios');
		
	}
}
?>