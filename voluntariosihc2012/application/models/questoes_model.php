<?php
class Questoes_Model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}
	
	/*Create*/
	public function inserir($valores=array())
	{
		$this->db->insert('questoes', $valores);		
	}
	
	/*Retrive*/
	//public function listarPaginado($limite,$pag);
	public function contarRegistros($idCategoria)
	{
		$query=$this->db->query("Select q.id as codigo, questao1, questao2,questao3,questao4,questao5,questaocorreta, u.nome as nomeusuario FROM questoes q inner join usuarios u on u.id=q.usuario_id WHERE q.categoria_id=".$idCategoria);
		return $query->num_rows();		
	}
	
	public function listarTodos($limite,$indice,$idCategoria)
	{
		$query=$this->db->query("Select q.id as codigo, questao1, questao2,questao3,questao4,questao5,questaocorreta, u.nome as nomeusuario FROM questoes q inner join usuarios u on u.id=q.usuario_id WHERE q.categoria_id='".$idCategoria."'  Order BY q.id ASC LIMIT ".$indice." , ".$limite);
		return $query->result();
	}

	public function contarBusca($campo,$parametro,$idCategoria)
	{

		$query=$this->db->query("Select q.id as codigo, questao1, questao2,questao3,questao4,questao5,questaocorreta, u.nome as nomeusuario FROM questoes q inner join usuarios u on u.id=q.usuario_id WHERE q.categoria_id='".$idCategoria."' and q.".$campo." like '%".$parametro."%'");						
		return $query->num_rows();
		
	}
	
	public function buscarPaginado($campo,$parametro,$limite,$indice,$idCategoria)
	{
		$sql="Select q.id as codigo, questao1, questao2,questao3,questao4,questao5,questaocorreta, u.nome as nomeusuario FROM questoes q inner join usuarios u on u.id=q.usuario_id WHERE q.categoria_id='".$idCategoria."' and q.".$campo." like '%".$parametro."%' Order BY q.id ASC Limit ".$indice.",".$limite;
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function obterPorID($id)
	{
		$query=$this->db->query("Select q.id as codigo, questao1, questao2,questao3,questao4,questao5,questaocorreta FROM questoes as q WHERE q.id='".$id."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
		   return $query->row();
		}		
	}
	
	/*Update*/
	public function atualizar($id,$valores=array())
	{
		$this->db->where('id',$id);
		$this->db->update('questoes', $valores);
	}

	public function definirStatus($id,$valorStatus)
	{
		$valor=1-$valorStatus;
		$sql="UPDATE questoes SET ativado='".$valor."' WHERE id='".$id."'";
		if($this->db->query($sql))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	

	/*Delete*/
	public function deletar($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('questoes');
		
	}
}
?>