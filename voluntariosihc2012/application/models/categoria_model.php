<?php
class Categoria_Model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}
	
	/*Create*/
	public function inserir($valores=array())
	{
		$this->db->insert('categoria', $valores);		
	}
	
	/*Retrive*/
	
	//public function listarPaginado($limite,$pag);
	
	
	public function contarRegistros()
	{
		$query=$this->db->query("SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoria_id) FROM questoes q where q.categoria_id=c.id) as questoes FROM categoria as c inner join usuarios as u on u.id=c.usuario_id");
		return $query->num_rows();		
	}
	
	public function listarTodos($limite,$indice)
	{
		$query=$this->db->query("SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoria_id) FROM questoes q where q.categoria_id=c.id) as questoes FROM categoria as c 
inner join usuarios as u on u.id=c.usuario_id Order BY c.titulo ASC LIMIT ".$indice." , ".$limite);
		return $query->result();
	}

	public function contarBusca($campo,$parametro)
	{
		if($campo=="id")
		{
			$query=$this->db->query("SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoria_id) FROM questoes q where q.categoria_id=c.id) as questoes FROM categoria as c 
inner join usuarios as u on u.id=c.usuario_id where c.id='".$parametro."'");
		}
		else
		{
			$query=$this->db->query("SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoria_id) FROM questoes q where q.categoria_id=c.id) as questoes FROM categoria as c 
inner join usuarios as u on u.id=c.usuario_id where c.".$campo." like '%".$parametro."%'");						
		}
		return $query->num_rows();
		
	}
	
	public function buscarPaginado($campo,$parametro,$limite,$indice)
	{
		
		if($campo=="id")
		{
			$sql="SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoria_id) FROM questoes q where q.categoria_id=c.id) as questoes FROM categoria as c 
inner join usuarios as u on u.id=c.usuario_id WHERE p.".$campo."='".$parametro."' Order BY p.titulo ASC Limit ".$indice.",".$limite;
		}
		else
		{
			$sql="SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoria_id) FROM questoes q where q.categoria_id=c.id) as questoes FROM categoria as c 
inner join usuarios as u on u.id=c.usuario_id WHERE c.".$campo." like '%".$parametro."%' Order BY c.titulo ASC Limit ".$indice.",".$limite;
		}
		
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function obterPorID($id)
	{
		$query=$this->db->query("SELECT * from categoria WHERE id='".$id."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
		   return $query->row();
		}		
	}
	
	/*Update*/
	public function atualizar($id,$valores=array())
	{
		$this->db->where('id',$id);
		$this->db->update('categoria', $valores);
	}

	/*Delete*/
	public function deletar($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('categoria');
		
	}
}
?>