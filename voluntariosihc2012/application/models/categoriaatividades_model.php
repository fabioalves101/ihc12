<?php
class CategoriaAtividades_Model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}
	
	/*Create*/
	public function inserir($valores=array())
	{
		$this->db->insert('categoriaatividades', $valores);		
	}
	
	/*Retrive*/
	
	//public function listarPaginado($limite,$pag);
	
	
	public function contarRegistros()
	{
		$query=$this->db->query("SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoriaatividades_id) FROM atividades q where q.categoriaatividades_id=c.id) as atividades FROM categoriaatividades as c inner join usuarios as u on u.id=c.usuario_id");
		return $query->num_rows();		
	}
	
	public function listarTodos($limite,$indice)
	{
		$query=$this->db->query("SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoriaatividades_id) FROM atividades q where q.categoriaatividades_id=c.id) as atividades FROM categoriaatividades as c 
inner join usuarios as u on u.id=c.usuario_id Order BY c.titulo ASC LIMIT ".$indice." , ".$limite);
		return $query->result();
	}
	
	public function listarChecked($usuario)
	{
		$sql="select ca.id,ca.titulo, CASE (select count(ai.categoriaatividades_id) from areainteresseusuario ai where ca.id=ai.categoriaatividades_id and ai.usuario_id=".$usuario.") WHEN 0 THEN '0' ELSE '1' END as status from categoriaatividades as ca
	ORDER BY ca.titulo";
		$query=$this->db->query($sql);
		return $query->result();		
	}

	public function contarBusca($campo,$parametro)
	{
		if($campo=="id")
		{
			$query=$this->db->query("SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoriaatividades_id) FROM atividades q where q.categoriaatividades_id=c.id) as atividades FROM categoriaatividades as c 
inner join usuarios as u on u.id=c.usuario_id where c.id='".$parametro."'");
		}
		else
		{
			$query=$this->db->query("SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoriaatividades_id) FROM atividades q where q.categoriaatividades_id=c.id) as atividades FROM categoriaatividades as c 
inner join usuarios as u on u.id=c.usuario_id where c.".$campo." like '%".$parametro."%'");						
		}
		return $query->num_rows();
		
	}
	
	public function buscarPaginado($campo,$parametro,$limite,$indice)
	{
		
		if($campo=="id")
		{
			$sql="SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoriaatividades_id) FROM atividades q where q.categoriaatividades_id=c.id) as atividades FROM categoriaatividades as c 
inner join usuarios as u on u.id=c.usuario_id WHERE p.".$campo."='".$parametro."' Order BY p.titulo ASC Limit ".$indice.",".$limite;
		}
		else
		{
			$sql="SELECT c.id as codigo,c.titulo as titulo,u.nome as nomeusuario,(SELECT count(q.categoriaatividades_id) FROM atividades q where q.categoriaatividades_id=c.id) as atividades FROM categoriaatividades as c 
inner join usuarios as u on u.id=c.usuario_id WHERE c.".$campo." like '%".$parametro."%' Order BY c.titulo ASC Limit ".$indice.",".$limite;
		}
		
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function obterPorID($id)
	{
		$query=$this->db->query("SELECT * from categoriaatividades WHERE id='".$id."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
		   return $query->row();
		}		
	}
	
	/*Update*/
	public function atualizar($id,$valores=array())
	{
		$this->db->where('id',$id);
		$this->db->update('categoriaatividades', $valores);
	}

	/*Delete*/
	public function deletar($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('categoriaatividades');
		
	}
}
?>