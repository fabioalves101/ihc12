<?php
class Perfil_Model extends CI_Model
{
	
	public function __construct()
	{
		$this->load->database();
	}
	
	public function listar()
	{
		$query=$this->db->query("SELECT p.id,p.titulo as titulo FROM perfil p");
		$retorno['']="Selecione um perfil";
		foreach($query->result() as $item)
		{
			$retorno[$item->id]=$item->titulo;
		}
		return $retorno;
	}
	
	public function listarModulosPorPerfil($idPerfil)
	{
		$query=$this->db->query("SELECT m.id,m.titulo, m.link,m.menu  FROM `modulo_perfil` as mp inner join modulo m on m.id=mp.modulo_id WHERE  mp.perfil_id=".$idPerfil." ORDER BY m.ordenacao");
		/*foreach($query->result() as $item)
		{
			$retorno[$item->link]=$item->titulo;
		}		
		return $retorno;*/
		
		return $query->result();		
	}
	
	public function acessarModulosPorPerfil($idPerfil)
	{
		$query=$this->db->query("SELECT m.id,m.titulo, m.link,m.menu  FROM `modulo_perfil` as mp inner join modulo m on m.id=mp.modulo_id WHERE  mp.perfil_id=".$idPerfil);
		foreach($query->result() as $item)
			{
		$retorno[$item->link]=$item->titulo;
		}
		
		
		return $retorno;
		
		//return $query->result();
	}	
}