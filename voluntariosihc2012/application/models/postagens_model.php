<?php
class Postagens_Model extends CI_Model
{
	/*
	 * A entidade postagem contempla v�rias outras entidades sendo
	 * Tipo 1: Descontra��o
	 * Tipo 2: Dicas
	 * Tipo 3: Postagens
	 * */
	public function __construct()
	{
		$this->load->database();
	}
	
	/*Create*/
	public function inserir($valores=array())
	{
		$this->db->insert('postagens', $valores);		
	}
	
	/*Retrive*/
	
	//public function listarPaginado($limite,$pag);
	
	
	public function contarRegistros($tipo,$filtraidusuario)
	{
		$query=$this->db->query("SELECT p.id as codigo,p.titulo as titulo, p.ativado as valorStatus,case p.ativado  when '1' then 'Ativada' else 'Desativada' end as status,p.descricao,p.thumb, p.foto,p.videopequeno,p.video,DATE_FORMAT(p.data, '%d/%m/%Y') as data, p.tipo,u.nome as nomeusuario,(SELECT count(c.postagem_id) FROM comentarios c where c.postagem_id=p.id) as comentarios FROM postagens as p 
inner join usuarios as u on u.id=p.usuario_id WHERE p.tipo='".$tipo."' and (usuario_id=".$filtraidusuario." or 0=".$filtraidusuario.")");
		return $query->num_rows();		
	}
	
	public function listarTodos($limite,$indice,$tipo,$filtraidusuario)
	{
		$query=$this->db->query("SELECT p.id as codigo,p.titulo as titulo, p.ativado as valorStatus,case p.ativado  when '1' then 'Ativada' else 'Desativada' end as status,p.descricao,p.thumb, p.foto,p.videopequeno,p.video,DATE_FORMAT(p.data, '%d/%m/%Y') as data, p.tipo,u.nome as nomeusuario,(SELECT count(c.postagem_id) FROM comentarios c where c.postagem_id=p.id) as comentarios FROM postagens as p 
inner join usuarios as u on u.id=p.usuario_id WHERE p.tipo='".$tipo."' and (usuario_id=".$filtraidusuario." or 0=".$filtraidusuario.") Order BY p.titulo ASC LIMIT ".$indice." , ".$limite);
		return $query->result();
	}

	public function contarBusca($campo,$parametro,$tipo,$filtraidusuario)
	{
		if($campo=="id")
		{
			$query=$this->db->query("SELECT p.id as codigo,p.titulo as titulo, p.ativado as valorStatus,case p.ativado  when '1' then 'Ativada' else 'Desativada' end as status,p.descricao,p.thumb, p.foto,p.videopequeno,p.video,DATE_FORMAT(p.data, '%d/%m/%Y') as data, p.tipo,u.nome as nomeusuario,(SELECT count(c.postagem_id) FROM comentarios c where c.postagem_id=p.id) as comentarios FROM postagens as p 
inner join usuarios as u on u.id=p.usuario_id WHERE p.tipo='".$tipo."' and p.id='".$parametro."' and (usuario_id=".$filtraidusuario." or 0=".$filtraidusuario.")");
		}
		else
		{
			$query=$this->db->query("SELECT p.id as codigo,p.titulo as titulo, p.ativado as valorStatus,case p.ativado  when '1' then 'Ativada' else 'Desativada' end as status,p.descricao,p.thumb, p.foto,p.videopequeno,p.video,DATE_FORMAT(p.data, '%d/%m/%Y') as data, p.tipo,u.nome as nomeusuario,(SELECT count(c.postagem_id) FROM comentarios c where c.postagem_id=p.id) as comentarios FROM postagens as p 
inner join usuarios as u on u.id=p.usuario_id WHERE p.tipo='".$tipo."' and p.".$campo." like '%".$parametro."%' and (usuario_id=".$filtraidusuario." or 0=".$filtraidusuario.")");						
		}
		return $query->num_rows();
		
	}
	
	public function buscarPaginado($campo,$parametro,$limite,$indice,$tipo,$filtraidusuario)
	{
		
		if($campo=="id")
		{
			$sql="SELECT p.id as codigo,p.titulo as titulo, p.ativado as valorStatus,case p.ativado  when '1' then 'Ativada' else 'Desativada' end as status,p.descricao,p.thumb, p.foto,p.videopequeno,p.video,DATE_FORMAT(p.data, '%d/%m/%Y') as data, p.tipo,u.nome as nomeusuario,(SELECT count(c.postagem_id) FROM comentarios c where c.postagem_id=p.id) as comentarios FROM postagens as p 
inner join usuarios as u on u.id=p.usuario_id WHERE p.tipo='".$tipo."' and p.".$campo."='".$parametro."' and (usuario_id=".$filtraidusuario." or 0=".$filtraidusuario.") Order BY p.titulo ASC Limit ".$indice.",".$limite;
		}
		else
		{
			$sql="SELECT p.id as codigo,p.titulo as titulo, p.ativado as valorStatus,case p.ativado  when '1' then 'Ativada' else 'Desativada' end as status,p.descricao,p.thumb, p.foto,p.videopequeno,p.video,DATE_FORMAT(p.data, '%d/%m/%Y') as data, p.tipo,u.nome as nomeusuario,(SELECT count(c.postagem_id) FROM comentarios c where c.postagem_id=p.id) as comentarios FROM postagens as p 
inner join usuarios as u on u.id=p.usuario_id WHERE p.tipo='".$tipo."' and p.".$campo." like '%".$parametro."%' and (usuario_id=".$filtraidusuario." or 0=".$filtraidusuario.") Order BY p.titulo ASC Limit ".$indice.",".$limite;
		}
		
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function obterPorID($id)
	{
		$query=$this->db->query("SELECT p.id as codigo,p.titulo as titulo, p.ativado as status,p.descricao,p.thumb, p.foto,p.videopequeno,p.video,DATE_FORMAT(p.data, '%d/%m/%Y') as data, p.tipo FROM postagens as p WHERE id='".$id."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
		   return $query->row();
		}		
	}
	
	/*Update*/
	public function atualizar($id,$valores=array())
	{
		$this->db->where('id',$id);
		$this->db->update('postagens', $valores);
	}

	public function definirStatus($id,$valorStatus)
	{
		$valor=1-$valorStatus;
		$sql="UPDATE postagens SET ativado='".$valor."' WHERE id='".$id."'";
		if($this->db->query($sql))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	/*Delete*/
	public function deletar($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('postagens');
		
	}
}
?>