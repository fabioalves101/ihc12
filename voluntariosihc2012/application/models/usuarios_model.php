<?php
class Usuarios_Model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();		
	}
	
	/*Create*/
	public function inserir($valores=array())
	{
		$this->db->insert('usuarios', $valores);		
	}
	
	/*Retrive*/
	public function contarRegistros($filtraidusuario)
	{
		$sql=@"SELECT 
			   u.id as codigo, 
			   u.nome,
			   u.curso,
			   u.semestre,
			   u.instituicao,
			   DATE_FORMAT(u.datanascimento, '%d/%m/%Y') as datanascimento,
			   case u.sexo when 'M' then 'Masculine' else 'Feminino' end as sexo,
			   u.telefonepessoal,
			   u.telefonecontato,
			   case u.sociosbc when'1' then 'Sim' else 'N�o' end as sociosbc,
			   u.numerosocio,
			   u.pagina,
			   u.login, 
			   p.titulo as perfil,
			   u.ativado as valorStatus,
			   case u.ativado  when '1' then 'Ativado' else 'Desativado' end as status, DATE_FORMAT(u.datacadastro, '%d/%m/%Y') as datacadastro, DATE_FORMAT(u.dataativacao, '%d/%m/%Y') as dataativacao 
			   FROM usuarios u inner join perfil p on u.perfil=p.id 
			   WHERE (u.id=".$filtraidusuario." or 0=".$filtraidusuario.")";
			$query=$this->db->query($sql);
			return $query->num_rows();		
	}
	
	public function listarTodos($limite,$indice,$filtraidusuario)
	{
		$sql=@"SELECT
		u.id as codigo,
		u.nome,
		u.curso,
		u.semestre,
		u.instituicao,
		DATE_FORMAT(u.datanascimento, '%d/%m/%Y') as datanascimento,
		case u.sexo when 'M' then 'Masculine' else 'Feminino' end as sexo,
		u.telefonepessoal,
		u.telefonecontato,
		case u.sociosbc when'1' then 'Sim' else 'N�o' end as sociosbc,
		u.numerosocio,
		u.pagina,
		u.login,
		p.titulo as perfil,
		u.ativado as valorStatus,
		case u.ativado  when '1' then 'Ativado' else 'Desativado' end as status, DATE_FORMAT(u.datacadastro, '%d/%m/%Y') as datacadastro, DATE_FORMAT(u.dataativacao, '%d/%m/%Y') as dataativacao
		FROM usuarios u inner join perfil p on u.perfil=p.id
		WHERE (u.id=".$filtraidusuario." or 0=".$filtraidusuario.") Order BY u.nome ASC LIMIT ".$indice." , ".$limite;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function contarBusca($campo,$parametro,$filtraidusuario)
	{
		if($campo=="id")
		{
			$sql=@"SELECT
			       u.id as codigo,
			       u.nome,
			       u.curso,
			       u.semestre,
			       u.instituicao,
			       DATE_FORMAT(u.datanascimento, '%d/%m/%Y') as datanascimento,
			       case u.sexo when 'M' then 'Masculine' else 'Feminino' end as sexo,
			       u.telefonepessoal,
			       u.telefonecontato,
			       case u.sociosbc when'1' then 'Sim' else 'N�o' end as sociosbc,
			       u.numerosocio,
			       u.pagina,
			       u.login,
				   p.titulo as perfil,
			       u.ativado as valorStatus,
			       case u.ativado  when '1' then 'Ativado' else 'Desativado' end as status, DATE_FORMAT(u.datacadastro, '%d/%m/%Y') as datacadastro, DATE_FORMAT(u.dataativacao, '%d/%m/%Y') as dataativacao
			       FROM usuarios u inner join perfil p on u.perfil=p.id";
		    $sql.="WHERE u.id='".$parametro."' and (u.id=".$filtraidusuario." or 0=".$filtraidusuario.")";
			$query=$this->db->query($sql);
		}
		else
		{
			$sql=@"SELECT
			       u.id as codigo,
			       u.nome,
			       u.curso,
			       u.semestre,
			       u.instituicao,
			       DATE_FORMAT(u.datanascimento, '%d/%m/%Y') as datanascimento,
			       case u.sexo when 'M' then 'Masculine' else 'Feminino' end as sexo,
			       u.telefonepessoal,
			       u.telefonecontato,
			       case u.sociosbc when'1' then 'Sim' else 'N�o' end as sociosbc,
			       u.numerosocio,
			       u.pagina,
			       u.login,
				   p.titulo as perfil,
			       u.ativado as valorStatus,
			       case u.ativado  when '1' then 'Ativado' else 'Desativado' end as status, DATE_FORMAT(u.datacadastro, '%d/%m/%Y') as datacadastro, DATE_FORMAT(u.dataativacao, '%d/%m/%Y') as dataativacao
			       FROM usuarios u inner join perfil p on u.perfil=p.id";
			$sql.= "WHERE u.".$campo." like '%".$parametro."%' and (u.id=".$filtraidusuario." or 0=".$filtraidusuario.")";			
			$query=$this->db->query($sql);						
		}
		return $query->num_rows();
		
	}
	
	public function buscarPaginado($campo,$parametro,$limite,$indice,$filtraidusuario)
	{
		
		if($campo=="id")
		{
			$sql=@"SELECT
			       u.id as codigo,
			       u.nome,
			       u.curso,
			       u.semestre,
			       u.instituicao,
			       DATE_FORMAT(u.datanascimento, '%d/%m/%Y') as datanascimento,
			       case u.sexo when 'M' then 'Masculine' else 'Feminino' end as sexo,
			       u.telefonepessoal,
			       u.telefonecontato,
			       case u.sociosbc when'1' then 'Sim' else 'N�o' end as sociosbc,
			       u.numerosocio,
			       u.pagina,
			       u.login,
				   p.titulo as perfil,
			       u.ativado as valorStatus,
			       case u.ativado  when '1' then 'Ativado' else 'Desativado' end as status, DATE_FORMAT(u.datacadastro, '%d/%m/%Y') as datacadastro, DATE_FORMAT(u.dataativacao, '%d/%m/%Y') as dataativacao
			       FROM usuarios u inner join perfil p on u.perfil=p.id";
			$sql= "WHERE u.".$campo."='".$parametro."' and (u.id=".$filtraidusuario." or 0=".$filtraidusuario.") Order BY u.nome ASC Limit ".$indice.",".$limite;
		}
		else
		{
			$sql=@"SELECT
			       u.id as codigo,
			       u.nome,
			       u.curso,
			       u.semestre,
			       u.instituicao,
			       DATE_FORMAT(u.datanascimento, '%d/%m/%Y') as datanascimento,
			       case u.sexo when 'M' then 'Masculine' else 'Feminino' end as sexo,
			       u.telefonepessoal,
			       u.telefonecontato,
			       case u.sociosbc when'1' then 'Sim' else 'N�o' end as sociosbc,
			       u.numerosocio,
			       u.pagina,
			       u.login,
				   p.titulo as perfil,
			       u.ativado as valorStatus,
			       case u.ativado  when '1' then 'Ativado' else 'Desativado' end as status, DATE_FORMAT(u.datacadastro, '%d/%m/%Y') as datacadastro, DATE_FORMAT(u.dataativacao, '%d/%m/%Y') as dataativacao
			       FROM usuarios u inner join perfil p on u.perfil=p.id";			
			$sql="WHERE u.".$campo." like '%".$parametro."%' and (u.id=".$filtraidusuario." or 0=".$filtraidusuario.") Order BY u.nome ASC Limit ".$indice.",".$limite;
		}
			
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function obterPorID($id)
	{
		$query=$this->db->query("SELECT * FROM usuarios WHERE id='".$id."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
		   return $query->row();
		}		
	}
	
	public function autenticar($login,$senha)
	{
		$query=$this->db->query("SELECT * FROM usuarios WHERE login='".$login."' and senha='".$senha."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}		
	}
	
	public function obterPorLogin($Login)
	{
		$query=$this->db->query("SELECT * FROM usuarios WHERE login='".$Login."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
	}
	
	/*Update*/
	public function atualizar($id,$valores=array())
	{
		$this->db->where('id',$id);
		$this->db->update('usuarios', $valores);
	}
	
	
	public function definirStatus($id,$valorStatus)
	{
		$valor=1-$valorStatus;
		$sql="UPDATE usuarios SET ativado='".$valor."' WHERE id='".$id."'";
		if($this->db->query($sql))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*Delete*/
	public function deletar($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('usuarios');		
	}
	
	public function associainteresse($valores=array(),$id)
	{
		$this->deletaInteresseAssociado($id);
		foreach($valores as $interesse)
		{
			$arrayValores=array(
					'usuario_id'=>$id,
					'categoriaatividades_id'=>$interesse);
			$this->insereInteresse($arrayValores);
		}
	}
		
	public function insereInteresse($valores)
	{
		$this->db->insert('areainteresseusuario', $valores);
	}
	
	public function deletaInteresseAssociado($usuario)
	{
		$sql="DELETE FROM areainteresseusuario WHERE usuario_id=".$usuario;
		if($this->db->query($sql))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>