<?php
class Atividades_Model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}
	
	/*Create*/
	public function inserir($valores=array())
	{
		$this->db->insert('atividades', $valores);		
	}
	
	/*Retrive*/
	//public function listarPaginado($limite,$pag);
	public function contarRegistros($idCategoria)
	{
		$query=$this->db->query("Select a.id as codigo, a.titulo, a.descricao,DATE_FORMAT(a.datainicial, '%d/%m/%Y') as datainicial,DATE_FORMAT(a.datafinal, '%d/%m/%Y') as datafinal,a.horas,a.turno,a.vagas, u.nome as nomeusuario FROM atividades a inner join usuarios u on u.id=a.usuario_id WHERE a.categoriaatividades_id='".$idCategoria."'");
		return $query->num_rows();		
	}
	
	public function listarTodos($limite,$indice,$idCategoria)
	{
		$query=$this->db->query("Select a.id as codigo, a.titulo, a.descricao,DATE_FORMAT(a.datainicial, '%d/%m/%Y') as datainicial,DATE_FORMAT(a.datafinal, '%d/%m/%Y') as datafinal,a.horas,a.turno,a.vagas, u.nome as nomeusuario FROM atividades a inner join usuarios u on u.id=a.usuario_id WHERE a.categoriaatividades_id='".$idCategoria."'  Order BY a.id ASC LIMIT ".$indice." , ".$limite);
		return $query->result();
	}

	public function contarBusca($campo,$parametro,$idCategoria)
	{
		$sql="Select a.id as codigo, a.titulo, a.descricao,DATE_FORMAT(a.datainicial, '%d/%m/%Y') as datainicial,DATE_FORMAT(a.datafinal, '%d/%m/%Y') as datafinal,a.horas,a.turno,a.vagas, u.nome as nomeusuario FROM atividades a inner join usuarios u on u.id=a.usuario_id WHERE a.categoriaatividades_id='".$idCategoria."' and a.".$campo." like '%".$parametro."%'";
		$query=$this->db->query($sql);						
		return $query->num_rows();
		
	}
	
	public function buscarPaginado($campo,$parametro,$limite,$indice,$idCategoria)
	{
		$sql="Select a.id as codigo, a.titulo, a.descricao,DATE_FORMAT(a.datainicial, '%d/%m/%Y') as datainicial,DATE_FORMAT(a.datafinal, '%d/%m/%Y') as datafinal,a.horas,a.turno,a.vagas, u.nome as nomeusuario FROM atividades a inner join usuarios u on u.id=a.usuario_id WHERE a.categoriaatividades_id='".$idCategoria."' and a.".$campo." like '%".$parametro."%' Order BY a.id ASC Limit ".$indice.",".$limite;
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function obterPorID($id)
	{
		$query=$this->db->query("Select a.id as codigo, a.titulo, a.descricao,DATE_FORMAT(a.datainicial, '%d/%m/%Y') as datainicial,DATE_FORMAT(a.datafinal, '%d/%m/%Y') as datafinal,a.horas,a.turno,a.vagas, u.nome as nomeusuario FROM atividades a inner join usuarios u on u.id=a.usuario_id WHERE a.id='".$id."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
		   return $query->row();
		}		
	}
	
	/*Update*/
	public function atualizar($id,$valores=array())
	{
		$this->db->where('id',$id);
		$this->db->update('atividades', $valores);
	}

	public function definirStatus($id,$valorStatus)
	{
		$valor=1-$valorStatus;
		$sql="UPDATE atividades SET ativado='".$valor."' WHERE id='".$id."'";
		if($this->db->query($sql))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	

	/*Delete*/
	public function deletar($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('atividades');
		
	}
}
?>