<?php
class Materiais_Model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}
	
	/*Create*/
	public function inserir($valores=array())
	{
		$this->db->insert('materiais', $valores);		
	}
	
	/*Retrive*/
	
	//public function listarPaginado($limite,$pag);
	
	
	public function contarRegistros()
	{
		$query=$this->db->query("SELECT m.id as codigo,m.titulo as titulo,m.arquivo as arquivo,m.tipo as tipo ,u.nome as nomeusuario FROM materiais as m inner join usuarios as u on u.id=m.usuario_id");
		return $query->num_rows();		
	}
	
	public function listarTodos($limite,$indice)
	{
		$query=$this->db->query("SELECT m.id as codigo,m.titulo as titulo,m.arquivo as arquivo,m.tipo as tipo ,u.nome as nomeusuario FROM materiais as m inner join usuarios as u on u.id=m.usuario_id Order BY m.titulo ASC LIMIT ".$indice." , ".$limite);
		return $query->result();
	}

	public function contarBusca($campo,$parametro)
	{
		if($campo=="id")
		{
			$query=$this->db->query("SELECT m.id as codigo,m.titulo as titulo,m.arquivo as arquivo,m.tipo as tipo ,u.nome as nomeusuario FROM materiais as m inner join usuarios as u on u.id=m.usuario_id WHERE m.id='".$parametro."'");
		}
		else
		{
			$query=$this->db->query("SELECT m.id as codigo,m.titulo as titulo,m.arquivo as arquivo,m.tipo as tipo ,u.nome as nomeusuario FROM materiais as m inner join usuarios as u on u.id=m.usuario_id WHERE m.".$campo." like '%".$parametro."%'");						
		}
		return $query->num_rows();
		
	}
	
	public function buscarPaginado($campo,$parametro,$limite,$indice)
	{
		
		if($campo=="id")
		{
			$sql="SELECT m.id as codigo,m.titulo as titulo,m.arquivo as arquivo,m.tipo as tipo ,u.nome as nomeusuario FROM materiais as m inner join usuarios as u on u.id=m.usuario_id WHERE m.".$campo."='".$parametro."' Order BY m.titulo ASC Limit ".$indice.",".$limite;
		}
		else
		{
			$sql="SELECT m.id as codigo,m.titulo as titulo,m.arquivo as arquivo,m.tipo as tipo ,u.nome as nomeusuario FROM materiais as m inner join usuarios as u on u.id=m.usuario_id WHERE m.".$campo." like '%".$parametro."%' Order BY m.titulo ASC Limit ".$indice.",".$limite;
		}
		
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	public function obterPorID($id)
	{
		$query=$this->db->query("SELECT id as codigo, titulo, arquivo, tipo FROM materiais WHERE id='".$id."' LIMIT 1");
		if ($query->num_rows() > 0)
		{
		   return $query->row();
		}		
	}
	
	/*Update*/
	public function atualizar($id,$valores=array())
	{
		$this->db->where('id',$id);
		$this->db->update('materiais', $valores);
	}

	/*Delete*/
	public function deletar($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('materiais');
		
	}
}
?>