<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "inicial";

$route['404_override'] = '';

$route['categoriaatividades'] = 'categoriaatividades';
$route['categoriaatividades/index/(:num)/(:num)/(:num)/(:num)'] = "categoriaatividades/index/$1/$2/$3/$4";
$route['categoriaatividades/buscar/(:num)/(:num)/(:num)/(:num)'] = "categoriaatividades/buscar/$1/$2/$3/$4";
$route['categoriaatividades/index/(:num)/(:num)'] = "categoriaatividades/index/$1/$2";
$route['categoriaatividades/buscar/(:num)/(:num)'] = "categoriaatividades/buscar/$1/$2";
$route['categoriaatividades/index'] = 'categoriaatividades/index';
$route['categoriaatividades/Excluir/(:num)/(:num)'] = 'categoriaatividades/excluir/$1/$2';
$route['categoriaatividades/inserir'] = 'categoriaatividades/inserir';
$route['categoriaatividades/Inserir/(:num)'] = 'categoriaatividades/Inserir/$1';
$route['categoriaatividades/atualizar/(:num)'] = 'categoriaatividades/atualizar/$1';
$route['categoriaatividades/Atualizar/(:num)/(:num)'] = 'categoriaatividades/Atualizar/$1/$2';
$route['categoriaatividades/confirmarexclusao/(:num)'] = 'categoriaatividades/confirmarexclusao/$1';
$route['categoriaatividades/atualizar'] = 'categoriaatividades/buscar';
$route['categoriaatividades/buscar'] = 'categoriaatividades/buscar';


$route['categoriaatividades/atividades'] = 'atividades';
$route['categoriaatividades/atividades/index/(:num)/(:num)/(:num)/(:num)'] = "atividades/index/$1/$2/$3/$4";
$route['categoriaatividades/atividades/buscar/(:num)/(:num)/(:num)/(:num)'] = "atividades/buscar/$1/$2/$3/$4";
$route['categoriaatividades/atividades/index/(:num)/(:num)'] = "atividades/index/$1/$2";
$route['categoriaatividades/atividades/buscar/(:num)/(:num)'] = "atividades/buscar/$1/$2";
$route['categoriaatividades/atividades/index'] = 'atividades/index';
$route['categoriaatividades/atividades/Excluir/(:num)/(:num)'] = 'atividades/excluir/$1/$2';
$route['categoriaatividades/atividades/inserir'] = 'atividades/inserir';
$route['categoriaatividades/atividades/Inserir/(:num)'] = 'atividades/Inserir/$1';
$route['categoriaatividades/atividades/atualizar/(:num)'] = 'atividades/atualizar/$1';
$route['categoriaatividades/atividades/Atualizar/(:num)/(:num)'] = 'atividades/Atualizar/$1/$2';
$route['categoriaatividades/atividades/confirmarexclusao/(:num)'] = 'atividades/confirmarexclusao/$1';
$route['categoriaatividades/atividades/atualizar'] = 'atividades/buscar';
$route['categoriaatividades/atividades/buscar'] = 'atividades/buscar';
/*Rotas para dicionário*/


/* End of file routes.php */
/* Location: ./application/config/routes.php */