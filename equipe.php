 <div id="equipe">
    	<div class="tituloBranco">Equipe </div>
        	<div class="textoBranco">
            	<strong>Coordenação Geral:<br /></strong>
				<span class="itemDestaque">Cristiano Maciel - cmaciel@ufmt.br - UFMT</span><br />
				<span class="itemDestaque">Patrícia Cristiane de Souza - patriciacs@ufmt.br - UFMT</span><br /><br />
                
                <strong>Comitê de Programa:</strong><br />
                <span class="itemDestaque">Junia Coutinho Anacleto - junia@dc.ufscar.br - UFSCAR</span><br />
                <span class="itemDestaque">Vânia Neris - vania@dc.ufscar.br - UFSCAR</span><br /><br />
				
				
				<div class="sanfona">
				 <div class="sanfonaTitulo"  id="SArtComp">
				 	Responsáveis pelas categorias de submissão de trabalhos
				 </div>
				<div class="sanfonaCorpo">
					<h1>Artigos Completos:</h1>
					<p>Júnia Anacleto - junia@dc.ufscar.br - UFSCar</p>
					<p>Vânia Neris - vania@dc.ufscar.br - UFSCar</p>
					
					<h1>Artigos Resumidos:</h1>
					<p>Raquel Prates - rprates@dcc.ufmg.br – UFMG</p>
					<p>Luciana Salgado - lsalgado2006@gmail.com - PUC-Rio</p>
								
					<h1>Artigos Industriais:</h1>
					<p>Marco Winckler - winckler@irit.fr - ICS-IRIT </p>
					<p>Bruno Santana -  brunosantana@inf.puc-rio.br - Petrobrás</p>
					
					<h1>Workshops:</h1>
					<p>Milene Silveira - milene@inf.pucrs.br - PUCRS</p>
					<p>Leonardo Cunha de Miranda - leonardo@dimap.ufrn.br - UFRN</p>
					
					<h1>Tutoriais:</h1>
					<p>Simone Barbosa - simone@inf.puc-rio.br - PUC-Rio </p>
					<p>Plínio Aquino Jr – plinio.aquino@fei.edu.br - FEI</p>
					
					<h1>Pôsteres e Demonstrações:</h1>
					<p>Amanda Meincke Melo - amandamelo@unipampa.edu.br – UNIPAMPA</p>
					<p>Marcelo Pimenta – mpimenta@inf.ufrgs.br - UFRGS</p>
					
					<h1>Competição de Avaliação:</h1>
					<p>Silvia Amélia Bim - sabim@unicentro.br - UNICENTRO</p>
					<p>Carla Faria Leitão – faria@inf.puc-rio.br - PUC-Rio</p>
					
				</div>
				<div class="sanfonaTitulo"  id="SArtComp">
				 	 Comitê de Programa
				 </div>
				<div class="sanfonaCorpo">
					<p>Alessandro Souza Ferreira Rubim de Assis (Nuance Communications, Brasil)</p><p>
Alfredo Sanchez (Universidad de las Américas Puebla, México)</p><p>
Amanda Melo (UNIPAMPA, Brasil)</p><p>
Bruno Santana da Silva (Petrobrás, Brasil)</p><p>
Carla Faria Leitão (PUC-Rio, Brasil)</p><p>
Carla Maria Dal Sasso Freitas (UFRGS, Brasil)</p><p>
Celmar Guimarães da Silva (FT-UNICAMP, Brasil)</p><p>
Clarisse Sieckenius de Souza (PUC-Rio, Brasil)</p><p>
Claudio Pinhanez (IBM, Brasil)</p><p>
Cleidson de Souza, (IBM, Brasil)</p><p>
Cristiano Maciel (UFMT, Brasil)</p><p>
Ednaldo Pizzolato (UFSCar, Brasil)</p><p>
Eduardo Hideki Tanaka (UNICAMP, Brasil)</p><p>
Elizabeth Furtado (UNIFOR, Brasil)</p><p>
Henry Lieberman (Massachusetts Institute of Technology, EUA)</p><p>
Flavio Soares Correa da Silva (USP, Brasil)</p><p>
Jair Cavalcanti Leite (UFRN, Brasil)</p><p>
Janne Oeiras (UFGD, Brasil)</p><p>
Joice Lee Otsuka (UFSCar, Brasil)</p><p>
Jonathan Grudin (Microsoft Research, EUA)</p><p>
Juliano Schimiguel (CETEC UNICSUL, Brasil)</p><p>
Junia Coutinho Anacleto (UFSCar, Brasil)</p><p>
Keiichi Nakata (University of Reading, Inglaterra)</p><p>
Kênia Sousa (Intrasoft International, Bélgica)</p><p>
Laura Sanchez Garcia (UFPR, Brasil)</p><p>
Leonardo Cunha de Miranda (UFRN, Brasil)</p><p>
Liza Potts (MichiganState University, EUA)</p><p>
Leonelo Dell Anhol Almeida (UNICAMP, Brasil)</p><p>
Lúcia Filgueiras (USP, Brasil)</p><p>
Luciana Martha Silveira (UFTPR, Brasil)</p><p>
Luciana Salgado (PUC-Rio, Brasil)</p><p>
Marcelo Pimenta (UFRGS, Brasil)</p><p>
Maria Cecília Baranauskas (Unicamp, Brasil)</p><p>
Marco Antônio Alba Winckler (Université Paul Sabatier, França)</p><p>
Milene Silveira (PUC-RS, Brasil)</p><p>
Patricia Cristiane de Souza (UFMT, Brasil)</p><p>
Plinio Thomaz Aquino Jr (FEI, Brasil)</p><p>
Priscila Farias (USP, Brasil)</p><p>
Raquel Oliveira Prates  (UFMG, Brasil)</p><p>
Renata Pontin (ICMC/USP, Brasil)</p><p>
Rodrigo Bonacin (CTI, Brasil)</p><p>
Sérgio Roberto Pereira da Silva (UEM, Brasil)</p><p>
Sidney Fels (University of British Columbia, Canadá)</p><p>
Silvia Amélia Bim (Unicentro, Brasil)</p><p>
Simone Diniz Junqueira Barbosa (PUC-Rio, Brasil)</p><p>
Stephen Gulliver (University of Reading, Inglaterra)</p><p> 
Tiago Silva da Silva (ICMC/USP, Brasil)</p><p>
Vânia Almeida Neris (UFSCAR, Brasil)</p><p>
Walter de Abreu Cybis (École Polytechnique de Montréal, Canadá)</p>
				</div>
            
  </div>
           </div>
           </div>
  <div class="clear"></div>
	
	
	
	