<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hotéis e Hospedagens IHC</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="bgThickBox">
	<div class="tituloThickBox">Hotéis e Hospedagens</div>
</div>
<div class="blocoThickBox"> 
<div style="float: left; width: 595px">
<p>
O hotel oficial do evento é o Holiday Inn Express Cuiabá, considerado uma das melhores
opções de hospedagem possui uma excelente infraestrutura para eventos e também:
</p>
</div>
<div style="float: right;">
<a href="http://www.holidaycuiaba.com.br" target="_blank">
<img src="img/holidayinnlogo.jpg" alt="logo holidayinn" border="0" />
</a>
</div>
<div style="float:left">
<ul>
<li>Localização privilegiada próximo a shopping center.</li>
<li>A 20 minutos do aeroporto</li>
<li>Todos os apartamentos com duas "camas Queens" ou uma <br />Cama King Size</li>
<li>Sistema anti-ruídos em portas e janelas</li>
<li> Ligações locais para números fixos incluso nas diárias</li>
<li>Internet FREE nos apartamentos</li>
<li>café incluso na diária</li>
<li>Fitness, sauna e piscina</li>
<li> Internet Wi-Fi gratuito em todo o hotel</li>
<li>Loja de Conveniência aberta 24 horas</li>
<li>Estacionamento gratuito e monitorado 24 horas</li>
</ul>

</div>
<div style="float: left; width: 100%; font-weight: bold">
<p>
Email exclusivo para reservas dos participantes do IHC 2012: <a href="mailto:ihc12@holidaycuiaba.com.br">ihc12@holidaycuiaba.com.br</a>

</p>
<p>
Tarifas exclusivas para participantes do IHC 2012
<br />
Apartamentos SGL R$ 180,00 + 13% taxas
<br />
Apartamentos DBL R$ 220,00 + 13% taxas
<br />
Terceira pessoa no apartamento acrescentar R$ 50,00 + 13%
</p>
<p>
Site: 
<a href="http://www.holidaycuiaba.com.br" target="_blank">
www.holidaycuiaba.com.br
</a>
<br />
</p>
</div>
<br />
<div style="float: left; width: 595px">
<p>
    Como uma segunda opção de hospedagem a comissão organizadora do IHC´12 sugere o ‘Serras Hotel’, localizado ao lado do hotel oficial do evento ‘Holiday Inn
    Express Cuiabá’.
</p>
</div>
<div style="float: right;">
<a href="http://www.serrashotel.com.br/" target="_blank">
<img src="img/serrashotel.jpg" alt="logo Serras Hotel" border="0" />
</a>
</div>
<div style="float:left;">
<ul>
	<li>
		Possui 96 apartamentos diferenciados em 14 suítes com banheira e <br />sacada, apartamentos luxo duplos com sacada e individuais
	</li>
	<li> 
		Possui TV a cabo, acesso em banda larga e gratuito à Internet, <br />cofres eletrônicos, ar condicionado e frigobar
	</li>
	<li> 
		Restaurante com serviço de room service 24 horas
	</li>
	<li>
		Academia completa
	</li>
	<li>
		Estacionamento com monitoramento 24 horas
	</li>
	<li>
		Tarifas com café da manhã incluso
	</li>
</ul>

</div>
<div style="float:left;" class="bold">
<p>
Reservas para participantes do IHC 12 somente via email: 
<a href="mailto:reservas@serrashotel.com.br">
reservas@serrashotel.com.br
</a>
</p><p>
Tarifas exclusivas para participantes do IHC 2012
</p>
<ul>
	<li>
Apto Luxo Single, 01 pax R$ 119,20	
	</li>
	<li>
Apto Luxo Duplo, 02 pax R$ 183,20
</li>
	<li>

Apto Luxo Triplo, 03 pax R$ 207,20
</li>
	<li>
Suíte, 01 pax R$ 151,20
</li>
	<li>
Suíte, 02 pax R$ 223,20
</li>
</ul>
<p>
Site: <a href="http://www.serrashotel.com.br/" target="_blank">http://www.serrashotel.com.br/</a>
<br /><br /><br />
</p>

</div>




</div>





</body>
</html>