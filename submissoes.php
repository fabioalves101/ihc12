    <div id="submissoes"> <!-- SUBMISSÕES -->
            <div class="tituloBranco">Submissões </div>
                
                  
                <div class="sanfona">
				 <div class="sanfonaTitulo"  id="SArtComp">
				 	Informações Importantes
				 </div>   
                    <div class="sanfonaCorpo">
                                
<u><strong>Datas Importantes:</strong></u><br />


Registro de equipes para a competição de avaliação até <strike>10/07/2012</strike> <br />
Submissão de propostas para workshops até <strike>10/07/2012</strike> <br />
Submissão de propostas para tutoriais até <strike>12/07/2012</strike> <br />
Submissão de propostas para pôsteres e demos até <strike>16/07/2012</strike> <br />
Submissão de artigos resumidos até <strike>10/07/2012</strike> <br />
Envio do relatório para a competição de avaliação até <strike>20/07/2012</strike> <br />
Submissão de artigos industriais até <strike>22/07/2012</strike> <br />
Submissões de artigos para o WEIHC até 31/08/2012 <br />
Submissões de artigos para o WAIHCWS’12 até <strike>31/08/2012</strike> 25/09/2012 <br />

<!-- 
Data para submissão de título, autores, palavras-chave e abstract, para Artigos Completos: <strike>28/06/2012</strike> 01/07/2012 (estendido)<br />
Data de submissão do artigo, para Artigos Completos (somente para aqueles já registrados): 08/07/2012<br />
Data para submissão para todas as outras categorias: 05/07/2012<br /><br />
-->
Website para submissão dos trabalhos: 	<br />
<a href="http://www.easychair.org/conferences/?conf=ihc2012">		
http://www.easychair.org/conferences/?conf=ihc2012
</a><br />
Email do evento: ihc2012@easychair.org<br /><br />
Os autores devem submeter seus trabalhos em Inglês ou Português


</div><!-- FIM CAIXA DESTAQUE -->
    
    
    </div>
    
    

    
    
    
    
    
    <br />
    
    <div class="textoBranco">O IHC 2012 oferece oportunidade de submissão nas seguintes categorias:</div><br />
    
    		<div class="sanfona"><!--SANFONA - -->
  				 
                  <div class="sanfonaTitulo"  id="SArtComp">Artigos completos</div>
                  	<div class="sanfonaCorpo"><!--  SANFONA CORPO 1 -->
                  	  <p>A categoria de artigos completos tem por objetivo permitir a apresentação de 
                  	  resultados de pesquisa acadêmica de alta qualidade. As submissões devem conter uma 
                  	  contribuição original, e não podem já ter sido publicadas em outro fórum, nem estarem 
                  	  submetidas à revisão para outras conferências ou publicações. Contribuições 
                  	  devem incluir resultados inéditos de pesquisas, estudos de casos ou experiências 
                  	  que forneçam novas evidências sobre a pesquisa ou aplicação de IHC.
                  	  <!-- 
                  	   Os artigos aceitos 
                  	  nesta categoria serão publicados nos anais do evento (pretende-se publicá-los, também, 
                  	  na biblioteca digital da ACM, como feito nos eventos anteriores). --></p>
                      <h1>Submissões:</h1>
						As submissões devem ser anônimas e ter até <strong>dez páginas</strong> no formato <strong>ACM SIGCHI</strong>
						<a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>
						. Os autores devem efetuar suas submissões eletronicamente, através do sistema <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a> e em <strong>formato PDF</strong>.
                      <h1>Apresentação:</h1>
						Os artigos selecionados serão apresentados durante as sessões técnicas, com 20 minutos de apresentação expositiva seguidos de dez minutos para discussão.
                      <h1>Coordenadoras: </h1>
                      Júnia Anacleto e Vânia Neris (UFSCar)
                      <h1>Datas Importantes:</h1>
                        <strike>28/06/2012</strike> 01/07/2012 (estendido) –  prazo de submissão de título, autores, palavras-chave e abstract<br />
                        08/07/2012 - prazo para submissão do artigo (somente para aqueles já registrados)<br />
						15/08/2012 - notificação de resultados <br />
                      	29/08/2012 –  envio da versão final
</div><!-- FIM SANFONA CORPO 1 -->
                        
                        
                          <div class="sanfonaTitulo" id="SArtRes">Artigos resumidos</div>
                  	<div class="sanfonaCorpo"><!--  SANFONA CORPO 2 -->
                  	  <p>O objetivo da categoria de artigos resumidos é permitir que os pesquisadores apresentem 
                  	  um relato conciso de estudos em andamento, com resultados parciais, porém significativos, 
                  	  que auxiliem no desenvolvimento da comunidade de IHC. As submissões devem conter uma 
                  	  contribuição original, não publicada em outro fórum nem submetida à revisão para outras 
                  	  conferências ou publicações. 
                  	  <!-- 
                  	  Os artigos aceitos nesta categoria serão publicados nos 
                  	  anais do evento (pretende-se publicá-los também na biblioteca digital da ACM, como feito 
                  	  nos eventos anteriores). --></p>
                      <h1>Submissões:</h1>
                      As submissões devem ser anônimas e conter até <strong>quatro páginas</strong> no formato 
                      <strong>ACM SIGCHI</strong><a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>. 
                      Os autores devem efetuar suas submissões eletronicamente, através do sistema <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a>, e em <strong>formato PDF</strong>.
                   <h1>Apresentação:</h1>
                    Artigos aceitos nessa categoria serão apresentados em sessões técnicas, com dez minutos de exposição seguidos de cinco minutos para discussão.
                      <h1>Coordenadores: </h1>
                      Raquel Prates (UFMG) e Luciana Salgado  (PUC-Rio)
                      <h1>Datas Importantes:</h1>
                        <strike>05/07/2012</strike> 10/07/2012 (estendido) –  prazo de submissão<br />
                        <strike>04/08/2012</strike> 10/08/2012 (estendido) –  notificação de resultados<br />
                        <strike>28/08/2012</strike> 29/08/2012 (estendido) –  envio da versão final
</div><!-- FIM SANFONA CORPO 2 -->
                        
                        
                    
                         <div class="sanfonaTitulo" id="SArtInd">Artigos Industriais</div>
  <div class="sanfonaCorpo"><!--  SANFONA CORPO 3 -->

Esta categoria visa estudos de caso, ferramentas, técnicas, métodos, práticas e 
experiências na indústria que tratem do desenvolvimento comercial de interfaces 
para usuários de sistemas interativos. Essa é uma oportunidade da indústria expor 
para academia suas necessidades, práticas e culturas para estabelecer um diálogo e 
estimular parcerias. O objetivo é identificar os desafios encontrados por profissionais 
durante o desenvolvimento de sistemas interativos, estimular a pesquisa aplicada a 
tais desafios e discutir a transferência de tecnologia através de parcerias.<br />
 
As submissões devem relatar experiências significativas na área de IHC para a indústria, 
com uma descrição clara dos desafios encontrados durante o uso e o desenvolvimento de 
interfaces com usuário, acompanhada de uma discussão sobre conhecimentos e práticas de 
IHC empregados. Com base nas suas experiências, os autores podem comparar princípios, 
modelos, técnicas ou soluções de IHC que já existem, avaliar a aplicação de novos, ou 
ainda descrever características desejadas para outros que precisam ser elaborados. 
<!-- 
As 
submissões serão revisadas por membros do comitê de programa e as selecionadas serão 
publicados nos anais do evento e constarão da biblioteca digital da ACM. --><br />
 
Salienta-se que a submissão de um artigo industrial e sua aceitação implicará naturalmente na participação no Simpósio, onde pesquisadores e profissionais da indústria terão a oportunidade de discutir acerca dos desafios apresentados. Esse diálogo pode estimular a troca de conhecimentos e experiências entre empresas e academia, ampliando a compreensão dos problemas e a análise de possíveis soluções.


<h1>Submissões:</h1>
 As submissões devem ser anônimas e conter até <strong>4 páginas</strong> no formato <strong>ACM SIGCHI</strong>
 <a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>. Os autores devem efetuar suas submissões eletronicamente, através do sistema <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a>, e em <strong>formato PDF</strong>.<br />
<br />


Espera-se que os Artigos Industriais contenham:<br />

    <h2>Caracterização do problema: área, contexto, fontes, características (gerenciais, sociais, técnicas), importância e consequências do problema abordado;</h2> 

    <h2>Relato de experiência na indústria: descrever os desafios para solucionar o problema descrito e soluções exploradas; e</h2>

	<h2>Análise crítica da experiência relatada: O que funcionou? O que não funcionou? O que precisa ser melhorado? Quais desafios foram encontrados? Além de considerações (comparação, avaliação, etc.) sobre conhecimentos e práticas de IHC existentes ou que deveriam existir.</h2>
    
<br />


<h1>Apresentação:</h1>
 Os trabalhos serão apresentados em sessões técnicas, com 10 minutos de apresentação seguidos de 5 minutos para discussão.



<h1>Critérios Gerais:</h1>


Contribuições devem ser baseadas em casos reais. Recomenda-se fortemente que os profissionais escrevam sobre suas experiências, relatando seu contexto socio-técnico, os desafios encontrados, seus questionamentos, dúvidas, sucessos, insucessos e lições aprendidas. As experiências relatadas devem ser compatíveis com os tópicos definidos para o Simpósio, e demonstrar um possível interesse para as comunidades empresariais e acadêmicas participantes do evento.</strong>
<br />

Poderão ser escolhidos trabalhos de profissionais (técnicos ou acadêmicos) com aplicações em indústrias, universidades, centros de pesquisas, empresas, etc. Não necessariamente restritos à área de Tecnologia de Informação.
<br />

Os trabalhos não podem ter conotação comercial nem fazer marketing explícito de um produto ou instituição. Qualquer trabalho que, no julgamento do Comitê Técnico, tenha a intenção principal de divulgar produtos, marcas, fabricantes ou instituições será recusado.
<br />


<h1>Coordenadores: </h1>
Marco Winckler (ICS-IRIT) e Bruno Santana (Petrobrás) 

<h1>Datas Importantes:</h1>


    <strike>05/07/2012</strike> 22/07/2012 (estendido) – prazo de submissão<br />
    <strike>04/08/2012</strike> 19/08/2012 (estendido) – notificação de resultados<br />
    <strike>28/08/2012</strike> 29/08/2012 (estendido) – envio da versão final<br />


                        </div><!-- FIM SANFONA CORPO 3 -->
                  
                                             
                        
                        
                          <div class="sanfonaTitulo"  id="SArtW">Workshops</div>
                  			<div class="sanfonaCorpo"><!--  SANFONA CORPO 4 -->
                        	
                            Os workshops oferecem uma valiosa oportunidade para pequenas comunidades ou grupos com interesses comuns se encontrarem para uma discussão sobre tópicos de interesse específicos, tendências e temas emergentes de pesquisa ou aspectos práticos de IHC. Geralmente um workshop reúne um grupo razoável de participantes e pode durar um dia inteiro ou um turno (manhã ou tarde). É interessante que os organizadores de cada workshop planejem uma publicação resultante das discussões do workshop, como um livro ou edição especial de algum periódico.


<h1>Submissões:</h1>


<strong>Cada submissão deve conter obrigatoriamente os seguintes itens:</strong><br />



    - Capa contendo título do workshop, informações para contato, duração (um dia inteiro ou um turno, de preferência, em específico: manhã ou tarde), público-alvo e breve biografia dos organizadores;<br />

    - Resumo estendido de <strong>duas páginas</strong> no formato de publicações da <strong>ACM SIGCHI</strong> 
    <a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>
    para ser incluído nos anais da conferência. Este resumo deve apresentar o foco do workshop, justificando a relevância do tema e enunciando algumas questões de pesquisa ou prática a serem discutidas durante o evento com base no estado-da-arte do tema proposto.; e<br />
	
	- Chamada para participação de 250 palavras, a ser publicado no web site do IHC 2012 e nas listas de e-mail utilizadas para divulgação do evento.

    
Esse material deve ser submetido eletronicamente, em <strong>formato PDF</strong>, pelo sistema <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a>.<br />


Processo de Seleção: Cada proposta de workshop será avaliada por um comitê de especialistas na área.<br />


<h1>Coordenadores: </h1> Milene Silveira (PUC-RS) e Leonardo Cunha de Miranda (UFRN)


<h1>Datas Importantes:</h1>
                  			  
                  			  
                  			 <h2>Para os organizadores de Workshops:</h2>
                  			  
                  			  
                  			<p class="pEspacoSub">  
                               <strike>05/07/2012</strike> 10/07/2012 (estendido) – prazo de submissão para propostas de organização de workshop<br />
                  			   <strike>04/08/2012</strike> 20/07/2012 (estendido) – notificação de aprovação de workshop
                              </p>
                  			  
                  			
                  			  <h2>Para os participantes de Workshops:</h2>
                  			  
                  			  
                  			<p class="pEspacoSub">  
                              15/08/2012 – prazo de submissões para participantes de workshop<br />
                  			  25/08/2012 – notificação dos resultados para os participantes de workshops<br />
                  			  29/08/2012 – envio da versão final dos resumos para os anais
                            </p>
  <br />
  
  <h1>Workshops selecionados IHC'12:</h1>
  
  <p class="pEspacoSub"> 
  	<strong>III WEIHC – Workshop sobre o Ensino de Interação Humano-Computador </strong> <br />
	Silvia Bim, Clodis Boscarioli <br />
	<a href="http://www.inf.unioeste.br/WEIHC/" target="_blank">
	http://www.inf.unioeste.br/WEIHC/</a> <br /><br />

	<strong>
		IV WAIHCWS - Workshop sobre Aspectos da Interação Humano-Computador para a Web Social
	</strong><br />	
	Adriana Santarosa Vivacqua, José Viterbo Filho, Sérgio Roberto P. da Silva <br />
	<a href="http://www.ufmt.br/ihc12/waihcws/" target="_blank">
	http://www.ufmt.br/ihc12/waihcws/</a> 
  </p>
   			  </div>
               			<!-- FIM SANFONA CORPO 4 -->
                        
                        
                        
<div class="sanfonaTitulo" id="SArtT">Tutoriais</div>
                  			<div class="sanfonaCorpo"><!--  SANFONA CORPO 5 -->
                        	Os tutoriais são eventos de curta duração que visam apresentar uma visão geral de um tópico de pesquisa ou tecnologia que seja de interesse da comunidade de IHC de forma prática e didática. Com isso, o ouvinte tem a oportunidade de aprender sobre novo assunto vinculado à sua área de atuação e pode também extrair elementos que pode aplicar em sua pesquisa ou prática.<br />

							Os tutoriais devem ter como público alvo os estudantes de graduação, de pós-graduação ou profissionais da área. Os tutoriais poderão ser introdutórios ou avançados e deverão estar relacionados aos tópicos de interesse do simpósio. Propostas interdisciplinares são muito bem-vindas. Os tutoriais devem ter a duração de três ou seis horas e ser apresentados em português.<br />
<h1>Tutoriais selecionados</h1>
<h2>A) Projetando Sistemas Web com o uso de Técnicas de Interação Humano-Computador (6 horas)</h2>
<h2>B) Algoritmos de Clusterização e Python Científico apoiando Modelagem de Usuário (6 horas)</h2>
<h2>C) Designing speech based interfaces (3 horas)</h2>
<h2>D) Sistematização de revisões bibliográficas em pesquisas da área de IHC (3 horas)</h2>
							
							
<h1>Submissões:</h1>
 As propostas para tutoriais devem se limitar a <strong>cinco páginas</strong>, no formato do modelo para publicação de capítulos de livros da SBC  <a href="http://www.sbc.org.br/index.php?subject=60&amp;content=downloads" target="_blank">http://www.sbc.org.br/index.php?subject=60&content=downloads</a>, incluindo o seguinte:
<h2>  Um resumo;<br /></h2>
    <h2>Duração do tutorial (três ou seis horas);<br /></h2>
   <h2> Uma indicação de público-alvo, com especificação e justificativa de pré-requisitos;<br /></h2>
  <h2>  Sumário estendido, explicando o que será coberto em cada tópico;<br /></h2>

  <h2>  Uma indicação de por que o curso é interessante e deve atrair a atenção do público alvo;<br /></h2>

   <h2> Breve biografia dos autores;<br /></h2>

   <h2> Indicação de qual autor apresentará o tutorial;<br /></h2>
 <h2>Os recursos computacionais e audiovisuais necessários.<br /></h2>

<br />

Cada tutorial selecionado deverá ter como versão final um texto que será publicado como capítulo de um livro: de 20 a 30 páginas, para tutoriais com duração de três horas; ou de 30 a 40 páginas, para tutoriais com duração de seis horas. Os autores deverão autorizar a publicação do resumo e da apresentação dos tutoriais na Internet, no website do IHC 2012.<br />

Um apresentador de cada tutorial ganhará inscrição no evento. Outros tipos de auxílio estão sendo avaliados com a Comissão Organizadora do evento.<br />

Para submeter seu trabalho para o tutorial, acesse o sistema <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a>.<br />
<br />

<h1>Coordenadores: </h1>
Simone Barbosa (PUC-Rio) e Plínio Aquino Jr (FEI)<br /><br />


<h1>Datas Importantes:</h1>

    <strike>05/07/2012</strike> 12/07/2012 (estendido) – submissão de propostas <br />
    <strike>28/07/2012</strike> 26/07/2012 (estendido) – notificação de resultados <br />
    <strike>28/08/2012</strike> 29/08/2012 (estendido) – envio da versão final<br />

                        </div><!-- FIM SANFONA CORPO 5 -->
                    
                        
               <div class="sanfonaTitulo" id="SArtPD">Pôsteres e Demonstrações </div>
                  			<div class="sanfonaCorpo"><!--  SANFONA CORPO 6 -->
                        	Nessa trilha, a submissão como Pôster ou Demonstração destina-se a promover um ambiente de visibilidade e discussões no qual empresas, universidades e desenvolvedores possam apresentar trabalhos em fases iniciais ou intermediárias, ainda sem resultados consolidados, bem como sistemas interativos computacionais dos mais diversos tipos, cujos autores têm interessem em receber contribuições construtivas dos participantes do IHC 2012. <br />


Espera-se criar um ambiente no qual autores possam apresentar e receber feedback de membros experientes da comunidade atuante na área. Esse espaço dentro do IHC2012 também tem por objetivo promover visibilidade às pesquisas em andamento e protótipos de produtos para a comunidade acadêmica e industrial, potencializando a integração e cooperação entre esses setores.<br />


Apresentação: Os autores terão um momento de apresentação na forma de exposição coletiva e interativa, durante a qual pelo menos um dos autores deverá estar presente. As instruções e as informações sobre os recursos disponíveis no evento serão encaminhados aos autores dos trabalhos selecionados.<br />

<br />

<h1>Submissão:</h1>
O texto de apresentação deve ter até duas páginas e seguir o formato <strong>ACM SIGCHI</strong>
<a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>
 e deve incluir:<br />
	
    - Caracterização do problema<br />

	- Fundamentação teórica<br />

    - Metodologia e estado atual do trabalho<br />

    - Trabalhos relacionados<br />

    - Resultados esperados<br />

<br />


Os textos devem ser submetidos eletronicamente através do sistema <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a>, e em <strong>formato PDF</strong>.<br />


Apresentação: Os autores dos trabalhos terão um momento de apresentação na forma de exposição coletiva e interativa, durante os horários de coffee break do evento.<br />


<h1>Coordenadores:</h1>

 Amanda Melo (UNIPAMPA) e Marcelo  Pimenta (UFRGS)<br />



<h1>Datas Importantes:</h1>


    <strike>05/07/2012</strike> <strike>10/07/2012</strike> 16/07/2012 (estendido) – prazo de submissão<br />

    <strike>04/08/2012</strike> 10/08/2012 (estendido) – notificação dos resultados<br />

    <strike>28/08/2012</strike> 29/08/2012 (estendido) – envio da versão final<br />


                        </div><!-- FIM SANFONA CORPO 6 -->    
                        
                        
                        
                         <div class="sanfonaTitulo" id="SArtCA">Competição de Avaliação</div>
                  			<div class="sanfonaCorpo"><!--  SANFONA CORPO 7 -->
                        	Esta categoria busca motivar a participação no IHC 2012 de alunos e professores de IHC, contribuindo para a formação destes alunos, a geração de material didático e a experiência da comunidade. A Competição de Avaliação tem um caráter essencialmente prático – os participantes fazem a avaliação de um sistema computacional e, assim, aplicam seus conhecimentos teóricos relacionados a metodologias de avaliação de IHC.<br />
							A primeira edição da Competição de Avaliação foi no IHC 2006, envolvendo equipes de alunos de graduação e de pós-graduação de diversas instituições de ensino do país. Os resultados positivos trouxeram-nos à quinta edição da Competição, no IHC 2012.<br />
							<a target="_blank" href="http://www.ufmt.br/ihc12/Chamada-Competicao-Avaliacao-IHC2012.pdf">Detalhes sobre a temática para a Competição de Avaliação do IHC 2012 estão disponíveis aqui.</a>
							
<h1>Resultado da Competiçao do IHC</h1> 
<h2>Graduação</h2>

Análise da Percepção e Interação de Usuários sobre Privacidade e Segurança no Facebook</br>
UNIOESTE - Universidade Estadual do Oeste do Paraná (Cascavel - PR)</br>
Luiz Gustavo de Souza, Tiago Alexandre Schulz Sippert, </br>
André Specian Cardoso e Clodis Boscarioli (professor-orientador)
</br>
</br>
Exposição de imagem no Facebook - Um estudo sobre a privacidade de fotos pessoas na rede social</br>
UFC - Universidade Federal do Ceará (Fortaleza - CE)</br>
Mateus Pinheiro, Rodrigo Almeida, Deivith Oliveira</br>
Atila Oliveira e Arthur Tavares

<h2>Pós-Graduação</h2>

Avaliando aspectos de privacidade no Facebook pelas lentes de usabilidade, acessibilidade e fatores emocionais</br>
UFSCar - Universidade Federal de São Carlos (São Carlos - SP)</br>
Tatiana Alencar, Maira Canal, Kamila Rodrigues</br>
Rogério Xavier e Vânia Neris ( professora-orientadora )
</br>
</br>
Imagem e privacidade - contradições no Facebook</br>
UFMG - Universidade Federal de Minas Gerais (Belo Horizonte - MG)</br>
Ana Terto, Cláudio Alves, Janicy Rocha</br>
e Raquel Prates (professora-orientadora)</br>
</br>
Inspeção Semiótica e Avaliação de Comunicabilidade: identificando falhas de comunicabilidade sobre as configurações de privacidade do Facebook</br>
PUC-RS - Pontifícia Universidade Católica do Rio Grande do Sul (Porto Alegre - RS)</br>
Juliano Varella de Carvalho, Felipe Lammel</br>
Janaína Dias da Silva, Lucélia Cynthia Chipeaux</br>
e Milene Silveira ( professora-orientadora)
							
<h1>Submissões</h1> 
As submissões devem ser anônimas e ter até doze páginas no modelo para publicação de artigos da 
<strong>ACM SIGCHI</strong>
<a href="http://www.sigchi.org/publications/chipubform" target="_blank">(http://www.sigchi.org/publications/chipubform) </a>
. 

Os autores devem efetuar suas submissões eletronicamente, através do sistema <a href="http://www.easychair.org/conferences/?conf=ihc2012" target="_blank">EasyChair</a>, e em <strong>formato PDF</strong>.


<h1>Coordenadores</h1>
Silvia Amélia Bim (UNICENTRO) e Carla Faria Leitão (PUC-Rio)

<h1>Datas Importantes:</h1>

    <strike>05/07/2012</strike> 10/07/2012 (estendido) – prazo de inscrição das equipes<br />

    <strike>19/07/2012</strike> 20/07/2012 (estendido) – prazo de submissão dos relatórios<br />

    <strike>10/08/2012</strike> 15/08/2012 (estendido) – notificação dos finalistas para apresentação no IHC 2012<br />

    28/08/2012 – entrega do relatório final

                        </div><!-- FIM SANFONA CORPO 7 -->      

                        <div class="sanfonaTitulo" id="SArtCMD">Desafios IHC12</div>
                  			<div class="sanfonaCorpo"> 
<h1>
	GranDIHC-BR: Prospecção de Grandes Desafios de Pesquisa em 
	Interação Humano-Computador no Brasil (2012-2022)
</h1>
<p>

	Em 2006, a Sociedade Brasileira de Computação (SBC) apresentou os 5 Grandes Desafios de Pesquisa em Computação no Brasil para a década seguinte. Dentre os
	desafios, o de número 4, <strong>Acesso participativo e universal do cidadão brasileiro ao conhecimento</strong>, reconheceu o papel e a importância da
	área de Interação-Humano Computador (IHC) para o desenvolvimento social, científico, tecnológico e econômico do país.
</p>
<p>
	Dentre as 9 áreas centrais da Ciência da Computação, enumeradas desde 1988 pela ACM, IHC é a área que precisa lidar com questões de caráter universal e
	transversal às demais áreas e, ao mesmo tempo, considerar aspectos específicos (sociais, culturais, econômicos, políticos, geográficos) do ambiente em que
	sua aplicação ocorre. O contexto brasileiro é complexo: o país é o quinto maior em termos de território e população, possui uma cultura diversificada, e é
	tradicionalmente marcado pela desigualdade social. Embora nos últimos anos transformações rápidas e profundas tenham propiciado uma evolução nos
	indicadores de desenvolvimento econômico, social e humano, limitações em áreas críticas para manter essa evolução estão se tornando mais evidentes (e.g.,
	educação, ciência e tecnologia, saúde, infraestrutura, segurança, etc.). O desenvolvimento científico e tecnológico é um fator crucial para a superação
	dessas limitações, porém seus benefícios somente serão alcançados e sentidos nas dimensões necessárias se ele ocorrer de forma socialmente responsável.
</p>
<p>
	O GranDIHC-BR será realizado no XI Simpósio Brasileiro sobre Fatores Humanos em Sistemas Computacionais (IHC 2012) para prospectar questões de pesquisa na
	área de IHC que serão importantes para a ciência e o país num período de 10 anos, estendendo o alcance do Desafio 4 da SBC.
</p>
<p>
	Pesquisadores e profissionais da indústria são convidados a preencher o formulário online (<a href="http://goo.gl/4yo5Y" target="_blank">acesse</a>)
	apresentando suas propostas de desafios. As propostas serão analisadas por uma comissão de especialistas, e servirão como base para a discussão e
	proposição dos Grandes Desafios de Pesquisa em IHC no Brasil. Algumas das propostas serão convidadas para serem apresentadas durante o evento.
</p>
<p>
	Características de um Grande Desafio de Pesquisa, conforme proposto pela SBC:
</p>
<div>

	<p>
		1. Um Grande Desafio deve ser dirigido a avanços significativos na área e, consequentemente, à ciência, em vez de se basear em resultados incrementais de
		progressos existentes.
	</p>
	<p>
		2. A pesquisa para abordar um desafio deve ir muito além dos trabalhos e resultados que podem ser desenvolvidos e alcançados em um projeto de pesquisa
		individual convencional.
	</p>
	<p>
		3. Seu progresso deve ser passível de ser realizado e avaliado de forma incremental, de modo que seja possível analisar a sua evolução e executar mudanças
		de curso eventualmente necessárias.
	</p>
	<p>
		4. O sucesso de um Grande Desafio deve poder ser avaliado de forma clara e objetiva.
	</p>
	<p>
		5. Possivelmente multidisciplinar na natureza e nas possibilidades de solução.
	</p>
	<p>
		6. O Desafio deve ser realista e discutível em um prazo viável (e.g., 10 anos), ao mesmo tempo em que devem desafiar paradigmas, questionar e provocar uma
		evolução no panorama tradicional da área.
	</p>
	<p>
		7. Um Grande Desafio emerge de um consenso da comunidade científica para servir como um cenário de longo prazo para os pesquisadores, independentemente de
		políticas de financiamento ou de questões conjunturais.
	</p>

<h1>
	Formulário para submissão das propostas: <a href="http://goo.gl/4yo5Y" target="_blank">Acesse o formulário</a>
</h1>
<p>Informações adicionais: 
<a href="mailto:rpereira@ic.unicamp.br">
rpereira@ic.unicamp.br
</a>
</p>
<h1>
	Datas Importantes
</h1>
<p>
	Prazo para submissão das propostas: <strike>30/09/2012</strike>  05/10/2012
</p>
<p>
	Feedback aos autores: 
	15/10/2012
</p>
<p>
	Apresentação no evento:
	 06/11/2012, à tarde.
</p>

	<h1>Coordenação:</h1>
	
<p>
	Cecília Baranauskas (UNICAMP)
</p>
<p>
	Clarisse de Souza (PUC-Rio)
</p>
<h1>Comitê de Avaliação:</h1>
<p>
	Alessandro Rubim de Assis (Nuance)
</p>
<p>
	Elizabeth Furtado (UNIFOR)
</p>
<p>
	Jair Leite (UFRN)
</p>
<p>
	Juliana Sales (Microsoft Research)
</p>
<p>
	Junia C. Anacleto (UFSCar)
</p>
<p>
	Laura Sanchez Garcia (UFPR)
</p>
<p>
	Lúcia Filgueiras (USP)
</p>
<p>
	Marcelo Pimenta (UFRGS)
</p>
<p>
	Rodrigo Bonacin (CTI)
</p>
<p>
	Sergio Roberto da Silva (UEM)
</p>
<h1>Apoio:</h1>

<p>
	Roberto Pereira (UNICAMP)
</p>

</div> 
                        
                        
                        
                           
                        
	  </div><!-- FIM SANFONA -->
	  
	  
    
    </div><!-- FIM SUBMISSÕES -->
</div>
