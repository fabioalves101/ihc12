<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Informações Turísticas IHC</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<script src="js/lightbox/js/jquery-1.7.2.min.js"></script>
<script src="js/lightbox/js/lightbox.js"></script>
<link href="js/lightbox/css/lightbox.css" rel="stylesheet" />
</head>

<body class="turismobg">
<div class="bgThickBox">
	<div class="tituloThickBox">Informações Turísticas</div>
</div>
<div class="blocoThickBox"> 
<div style="text-align: center">
<object width="600" height="450">

<param name="movie" value="swf/ConhecaMT.swf" />

<embed src="swf/ConhecaMT.swf" width="600" height="450">

</embed>

</object>



</div>

<p>
	Cuiabá, capital matogrossense, de clima tropical, possui 530.308 habitantes (IBGE, 2010) 
	e está situada na região sul do Estado de Mato Grosso. É conhecida
	nacionalmente pelas belezas naturais, deliciosas comidas típicas e população receptiva 
	e hospitaleira.
</p>
<p>
	Na maior parte do ano os termômetros registram altas temperaturas chegando a ultrapassar 
	os 40º C. Para refrescar nada melhor do que um <em>happy hour</em>
	com sorvete de frutas regionais ou um choppinho bem gelado em qualquer uma das diversas 
	opções de bares e restaurantes da cidade.
</p>
<p>
	Um point para todos os gostos é a Praça Popular. Cercada de bares, café, sorveteria, 
	pizzarias e restaurantes oferece diversas opções de alimentação, reúne
	gente bonita, boa música e é diversão na certa do <em>happy hour</em> ao início da madrugada.
</p>
<p>
	O povo cuiabano sabe realçar e guardar sua história e tradição. Vale a pena separar um 
	tempo para conhecer alguns dos museus, galerias de arte e demais
	pontos turísticos.
</p>
<p>
	Dia e noite há muito o que ver e fazer em Cuiabá!
</p>
<p>
	<u>Dicas valiosas para o turista</u>
	:
</p>
<p>
	1. O fuso horário de Cuiabá registra uma hora a menos em relação à Brasília (DF).
</p>
<p>
	2. Roupas leves e protetor solar na mala!
</p>
<p>
	3. Para Chapada, Jaciara, Nobres e/ou Pantanal: incluir boné, roupa de banho, repelente.
</p>
<p>
	4. Centro de Atendimento ao Turista no Aeroporto: (65) 3692-6204
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/cuiaba1-aquario.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/cuiaba1-aquario.jpg" alt="Aquário Municipal" />
</a>
</div>



<div class="imggal">
<a href="img/fotos/cuiaba2-avcpa.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/cuiaba2-avcpa.jpg" alt="Avenida do CPA" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/cuiaba3-bonifacia.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/cuiaba3-bonifacia.jpg" alt="Parque Mãe Bonifácia" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/cuiaba4-viola.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/cuiaba4-viola.jpg" alt="Viola de Cocho" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/cuiaba5-rio.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/cuiaba5-rio.jpg" alt="Rio Cuiabá" />
</a>
</div>
</div>
<br clear="left" />

<p>
	<strong><u>Onde você não pode deixar de ir...</u></strong>
</p>
<ul>
	<li>
		<strong>SESC ARSENAL</strong>
	</li>
</ul>
<p>
	<br/>
	Foi criado em 1818, por ordem de Dom João IV. Foi construído para ser "um estabelecimento 
	militar para conserto e fabricação de armas", conforme definiu a
	carta-régia. Hoje se encontra restaurado como um centro cultural. Possui bar/restaurante, 
	sala de cinema, auditório, espaço para eventos artísticos e
	musicais, além de uma biblioteca. Dentre a programação oferecida pelo Sesc, destaca-se o 
	Bulixo, realizado todas as quintas-feiras a partir das 18h até às
	22h no jardim do Sesc. Artes plásticas, gravuras, artesanatos são comercializado a um preço 
	bem popular. O Bulixo ainda conta com uma praça de alimentação
	onde se vende comidas típicas como Maria Izabel, arroz com pequi e várias outras guloseimas.
</p>
<p>
	Com toda certeza, vale a pena conferir essa e outras programações oferecidads pelo Sesc Arsenal.
</p>
<p>
	Aberto de terça a domingo das 09h às 22h.
</p>
<p>
	A choperia está aberta a partir das 19h.
</p>
<p>
	Telefone: (65) 3611-0550
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/sesc2.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/sesc2.jpg" alt="Fachada Sesc Arsenal" />
</a>
</div>
<div class="imggal">
<a href="img/fotos/sesc1.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/sesc1.jpg" alt="Interior Sesc Arsenal" />
</a>
</div>
</div>
<br clear="left" />

<ul>
	<li>
		<strong>CASA DO ARTESÃO</strong>
	</li>
</ul>
<p>
	<br/>
	Situada na Rua 13 de Junho, no bairro do Porto, a Casa do Artesão tem uma bela amostra da 
	cultura mato-grossense, há um Museu do Artesanato com exposição
	permanente de peças caboclas e indígenas, e de peças criadas pelos mais diversos artesãos 
	locais, onde o turista pode viajar pela memória e cultura de Mato
	Grosso, conhecendo e reconhecendo a história do artesanato local, além de poder adquirir 
	diversos souvenirs. A casa do Artesão oferece ainda, um delicioso
	“Tchá cô bolo”, aonde diversos produtos regionais, entre eles o famoso Bolo de Arroz e Bolo 
	de Queijo Cuiabano, podem ser degustados.
</p>
<p>
	Aberto de segunda a sexta-feira - 8h30 às 17h / Sábado - 8h30 às 13h.
</p>
<p>
	“Tchá com bolo” todas às sextas-feiras das 14h às 17h. Fone: (65) 3611-0500.
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/casaartesao.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/casaartesao.jpg" width="200px" border="2" alt="Fachada Casa do Artesão" />
</a>
	</div>
	<div class="imggal">
<a href="img/fotos/art1.jpg" rel="lightbox"><img src="img/fotos/thumbs/art1.jpg" width="200px" border="2" alt="Artesanato" />
</a>

</div>

	<div class="imggal">
<a href="img/fotos/art3.jpg" rel="lightbox"><img src="img/fotos/thumbs/art3.jpg" width="200px" border="2" alt="Artesanato" />
</a>
	</div>
</div>

<br clear="left" />
<ul>
	<li>
		<strong>CATEDRAL METROPOLITANA</strong>
	</li>
</ul>
<p>
	<br/>
	Inaugurada em 1973, a atual catedral foi construída sobre os escombros da antiga, uma jóia 
	do período colonial que foi demolida num episódio até hoje não
	esclarecido, sua arquitetura moderna tem admiradores, é a sede da tradicional Festa do Senhor 
	Divino, que acontece de Maio a Junho.
</p>
<ul>
	<li>
		<strong>IGREJA DE NOSSA SENHORA DO BOM DESPACHO</strong>
	</li>
</ul>
<p>
	<br/>
	Localizada no Morro do Seminário, teve sua construção iniciada em 1720 pelas mãos do Frei 
	Ambrósio Daylé, que deu ao projeto o estilo arquitetônico francês
da Notre Dame de Paris e, por isso, é conhecida como a "Notre Dame Cuiabana", inaugurada em 1919, 
a igreja ainda está inacabada, ao lado da Igreja, está o	<strong>Museu de Arte Sacra</strong>, 
antigo Seminário da Conceição, expõe peças da antiga catedral de Cuiabá e alguns pertences de Dom Aquino Corrêa.
</p>
<p>
	Horário de funcionamento: 2ª a 6ª feira das 14h às 18h. Fone: (65) 3028-6286
</p>
<p>
	<strong></strong>
</p>
<p>
	<strong></strong>
</p>
<ul>
	<li>
		<strong>MUSEU RONDON</strong>
	</li>
</ul>
<p>
	<br/>
	Criado em 1972 pela UFMT, com a função de pesquisar grupos indígenas de MT. Mantém exposição de 
	artesanato, armas e ornamentos indígenas.
</p>
<p>
	Aberto de seg à sex 7h30 às 11h30 e 13h30 às 17h30, sáb, 7h30 às 11h30, entrada franca, Campus 
	da UFMT. Fone: (65) 3615-8489
	<br/>
	<br/>
</p>
<ul>
	<li>
		<strong>MUSEU DO MORRO DA CAIXA D’ÁGUA VELHA</strong>
	</li>
</ul>
<p>
	<strong></strong>
</p>
<p>
	Rua Comandante Costa s/No, Centro. Fone: (65) 3617-1274
</p>
<ul>
	<li>
		<strong>MARCO DO CENTRO GEODÉSICO DA AMÉRICA DO SUL</strong>
	</li>
</ul>
<p>
	<strong></strong>
</p>
<p>
	Rua Barão de Melgaço. Praça Moreira Cabral.
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/geo.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/geo.jpg" alt="Fachada Sesc Arsenal" />
</a>
</div>
</div>
<br clear="left" />

<ul>
	<li>
		<strong>COMUNIDADE SÃO GONÇALO BEIRA RIO</strong>
	</li>
</ul>
<p>
	<strong></strong>
</p>
<p>
	Comunidade tradicional de ribeirinhos que oferece peixarias simples com pescado local e 
	artesanato feito de cerâmica.
</p>
<ul>
	<li>
		<strong>ÁREA VERDE</strong>
	</li>
</ul>
<p>
	<strong></strong>
</p>
<p>
	Cuiabá possui um zoológico situado na UFMT além de alguns parques, excelente opção para 
	caminhadas e outras atividades físicas ao ar livre, são eles:
	Parque Mãe Bonifácia, Parque Zé Bolo Flô e Parque Massairo Okamura.
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/zoo.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/zoo.jpg" alt="Fachada Sesc Arsenal" />
</a>
</div>
</div>
<br clear="left" />
<p>
	<strong>
		Prolongue a estadia em Mato Grosso, e aproveite para desfrutar de seus três ecossistemas: 
		Pantanal, Cerrado e Amazônia. São imperdíveis os passeios
		para Chapada dos Guimarães, Nobres, Jaciara a Pantanal!
	</strong>
</p>
<p>
	<strong></strong>
</p>
<p>
	<strong>Parque Nacional de Chapada dos Guimarães</strong>
	, símbolo do turismo de contemplação, possui lindos chapadões, trilhas ecológicas, cavernas, 
	penhascos e atrativas cachoeiras! Além do Parque há também a
	cidade de Chapada dos Guimarães, com um centrinho cheio de opções de artesanato local e bons 
	restaurantes. Distância de Cuiabá: 60 km.
</p>
<div class="galeria">
	<div class="imggal">
<a href="img/fotos/chapada1-veu.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/chapada1-veu.jpg" alt="Véu de Noiva" />
</a>
</div>



<div class="imggal">
<a href="img/fotos/chapada3.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/chapada3.jpg" alt="Chapada dos Guimarães" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/chapada4-mirante.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/chapada4-mirante.jpg" alt="Mirante" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/chapada6-mirante.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/chapada6-mirante.jpg" alt="Mirante" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/chapada2-cachoeirinha.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/chapada2-cachoeirinha.jpg" alt="Cachoeirinha" />
</a>
</div>
</div>
<br clear="left" />
<p>
	<strong>Nobres</strong>
	, linda por natureza, tem como principais atrativos: o Aquário Natural, a Lagoa das Araras, o 
	Rio Salobra e a Cachoeira da Serra Azul. Passeios de bote,
	tirolesa e flutuação são as principais opções que vão te colocar em contato com a vida aquática. 
	Distância de Cuiabá: 140km.
</p>
<p>
	<strong>Jaciara</strong>
	espera os aventureiros para a prática de esportes radicais: Rafting e Rapel! Oferece também Águas 
	Termais, cachoeiras, cavernas e trilhas ecológicas.
	Distância de Cuiabá: 125km.
</p>
<p>
	O <strong>Pantanal</strong> dispensa apresentações. Conhecido mundialmente pela exuberante fauna e 
	flora, o Pantanal Mato-Grossense é considerado a maior
	planície de inundação do planeta, englobando o sudoeste do Mato Grosso, o oeste do Mato Grosso do 
	Sul, e parte do Paraguai e Bolívia. Lá você encontra
	diversas opções de hospedagem e passeios que te coloca em contato direto com o meio ambiente. 
	Distância de Cuiabá: 140 km
</p>

<div class="galeria">
	<div class="imggal">
<a href="img/fotos/pantanal1-jacare.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal1-jacare.jpg" alt="Jacaré" />
</a>
</div>



<div class="imggal">
<a href="img/fotos/pantanal2.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal2.jpg" alt="Pantanal" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal3-pantaneiro.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal3-pantaneiro.jpg" alt="Pantaneiro" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal4-aves.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal4-aves.jpg" alt="Aves" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal5-tapete.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal5-tapete.jpg" alt="Tapete Natural" />
</a>
</div>
<div class="imggal">
<a href="img/fotos/pantanal6-paisagem.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal6-paisagem.jpg" alt="Paisagem" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal6-tuiuiu.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal6-tuiuiu.jpg" alt="Tuiuiu" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal6-vegetacao.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal6-vegetacao.jpg" alt="Vegetação" />
</a>
</div>

<div class="imggal">
<a href="img/fotos/pantanal7-tuiuiu.jpg" rel="lightbox">
	<img src="img/fotos/thumbs/pantanal7-tuiuiu.jpg" alt="Tuiuiu" />
</a>
</div>
</div>
<br clear="left" />

</div>
</body>
</html>