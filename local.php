﻿<div id="local">
    <div class="tituloBranco">Local do Evento</div>
    <div class="sanfona">
				 <div class="sanfonaTitulo"  id="SArtComp">
				 Informações sobre o local do evento
				 </div>
				 <div class="sanfonaCorpo">
	
        <p>
        	O Simpósio Brasileiro sobre Fatores Humanos em Sistemas Computacionais, 
        	IHC, será realizado em Cuiabá capital do Mato Grosso, nas dependências 
        	do Holiday Inn Express. O  Holiday Inn Express faz parte da rede IHG, 
        	referência mundial em hospedagem. Localizado a 10km do Aeroporto 
        	Internacional de Cuiabá/Marechal Rondonl, é considerado uma das melhores 
        	opções de hospedagem e conta com uma excelente infraestrutura para eventos.
        	<br /><br /> 
        </p>
        <p><strong>Serviço:</strong></p>
        <p>                
        	<img src="img/holiday_logo_grande.gif" width="250" />        	
    	</p>
    	<p><br />
		    <strong>Endereço:</strong>
		    Avenida Miguel Sutil, 2050 - Jardim Leblon, Cuiabá - MT, 78060-000.
		    <br/>
		    <strong>Telefone:</strong>
		    (65) 3055-8500.
		</p>
		<p>
		    <strong>E-mail exclusivo para reservas dos participantes do IHC'12:</strong>
		    <br />
		    <a href="mailto:ihc12@holidaycuiaba.com.br">ihc12@holidaycuiaba.com.br</a>
		    
		</p>
		<p>
		    <strong>Site:</strong> <br />
		    <a href="http://www.holidaycuiaba.com.br/">
		    	<strong>http://www.holidaycuiaba.com.br/</strong>
		    </a><br /><br />
		</p>
		
		
		<ul>
			<li>
			- Localização privilegiada próximo a shopping center.
			</li>
			<li>
			- A 20 minutos do aeroporto
			</li>
			<li>
			- Todos os apartamentos com duas "camas Queens" ou uma 
			Cama King Size
			</li>
			<li>
			- Sistema anti-ruídos em portas e janelas
			</li>
			<li>
			- Ligações locais para números fixos incluso nas diárias
			</li>
			<li>
			- Internet FREE nos apartamentos café incluso na diária
			</li>
			<li>
			- Fitness, sauna e piscina
			</li>
			<li>
			- Internet Wi-Fi gratuito em todo o hotel
			</li>
			<li>
			- Loja de Conveniência aberta 24 horas
			</li>
			<li>
			- Estacionamento gratuito e monitorado 24 horas
			</li>
		</ul>
		<p><br />
			<strong>
				Tarifas exclusivas para participantes do IHC´12:
			</strong>
		</p>
		<p>
			Apartamentos SGL R$ 180,00 + 13% taxas<br />
			Apartamentos DBL R$ 220,00 + 13% taxas <br />
			Terceira pessoa no apartamento acrescentar R$ 50,00 + 13%<br /><br /><br />
		</p>
		</div>
		
	</div><!-- FIM CAIXA DESTAQUE -->
</div>